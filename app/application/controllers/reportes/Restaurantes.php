<?php
class Restaurantes extends MY_Controller{

    public function __construct(){
        parent::__construct();
        $this->table = 'administracion_eventos';
        $this->fields = ['nombre',];
        $this->module = $module = $this->getModule(11);
        $this->id_module = $module['id'];
        $this->load->model('ReportesRestaurantesModel','model');
    }
    public function index(){
        $this->load->view('main',
            [
                'title' =>  $this->module['nombre'],
                'usuario' => $this->session()['usuario'],
                'id_module' => $this->module['id'],
                'id_section' => $this->module['id_seccion']
            ]);
        $this->load->view('reportes/restaurantes');
        $this->load->view('footer');
        $this->log($this->module['id']);
    }


    public function eventos(){
        $data = $this->input->get();
        $inicio = '';
        $fin = '';
        if (isset($data['fecha_inicio']) && isset($data['fecha_fin'])){
            $inicio = new DateTime($data['fecha_inicio']);
            $inicio = $inicio->format('Y-m-d');
            $fin = new DateTime($data['fecha_fin']);
            $fin = $fin->format('Y-m-d');
        }
        $query = [
            'table' => 'administracion_eventos ae',
            'fields' => 'md5(ae.id) AS id, ae.nombre   ',
            'conditions' => [
                'ae.estatus' => 1,
                'DATE(ae.fecha_inicio) <=' => $inicio,
                'DATE(ae.fecha_fin) >=' => $fin,
                'MD5(administracion_distribucion.id_restaurante)' => $data['id_restaurante'],
            ],
            'joins' => [
                'administracion_distribucion  ' => ['administracion_distribucion.id_evento' => 'ae.id']
            ]
        ];
        $data = $this->query->select($query);
        $this->json($data);
    }

    public function datatable(){
        $get = $this->input->get();

        $data = $this->model->datatable($get);

        $this->json([
            'data' => $data,
            'count' => count($data)
        ]);
    }

    public function save(){
        $request_body = file_get_contents('php://input');
        $data = json_decode($request_body,true);
        $response = $this->valid($data);
        if($response == 1){
            $id = $this->query->save($this->table,$data);
            $this->save_action($data,$id);
        }
        else {
            $this->json($response);
        }
    }

    public function tipos(){
        $query = [
            'table' => 'administracion_tipo_eventos  ',
            'fields' => 'id, nombre',
            'conditions' => [
                'estatus' => 1
            ]
        ];
        $this->json([
            'data' => $this->query->select($query)
        ]);

    }



}