<?php
class Usuarios extends MY_Controller{

    public function __construct(){
        parent::__construct();
        $this->table = 'sistema_usuarios';
        $this->fields = ['usuario','correo'];
        $this->module = $module = $this->getModule(1);
        $this->id_module = $module['id'];


    }
    public function index(){
        $this->load->view('main',
            [
                'title' => 'Usuarios',
                'usuario' => $this->session()['usuario'],
                'id_module' => $this->module['id'],
                'id_section' => $this->module['id_seccion']
            ]);
        $this->load->view('sistema/usuarios');
        $this->load->view('footer');
        $this->log($this->module['id']);
    }

    public function datatable(){
        $query = [
            'table' => 'sistema_usuarios su',
            'fields' => 'md5(su.id) as id, su.nombre, su.usuario, su.correo, sp.nombre as perfil, su.id_perfil',
            'conditions' => [
                'su.estatus' => 1
            ],
            'filter' => [/*'su.usuario','su.correo','sp.nombre',*/'su.nombre'],
            'joins' => [
                'sistema_perfiles sp' => ['su.id_perfil' => 'sp.id']
            ]
        ];
        unset($this->input->get()['name']);
        $query = $this->filter($this->input->get(),$query);
        $data = $this->query->select($query);
        unset($query['limit']);
        $count = $this->query->select($query,false,true);
        $this->json([
            'data' => $data,
            'count' => $count
        ]);
    }

    public function save(){
        $request_body = file_get_contents('php://input');
        $data = json_decode($request_body,true);
        if (isset($data['contrasena'])){
             $data['contrasena'] = md5($data['contrasena']);
        }
        $response = $this->valid($data);
        if($response == 1){
             $id = $this->query->save($this->table,$data);
            $this->save_action($data,$id);
        }
        else {
            $this->json($response);
        }
    }
    public function perfiles(){
        $query = [
            'table' => 'sistema_perfiles',
            'fields' => 'id, nombre',
            'conditions' => [
                'estatus' => 1
            ]
        ];
        $this->json([
            'data' => $this->query->select($query)
        ]);

    }



}
