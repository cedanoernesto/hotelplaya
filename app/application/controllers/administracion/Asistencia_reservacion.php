<?php

class Asistencia_reservacion extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->table = 'administracion_reservacion';
        $this->fields = [];
        $this->module = $this->getModule(13);
        $this->id_module = $this->module['id'];
        $this->load->model('ReservacionModel', 'reservacion');
    }

    function index() {
        $this->load->view('main',
            [
                'title' => 'Asistencia',
                'usuario' => $this->session()['usuario'],
                'id_module' => $this->module['id'],
                'id_section' => $this->module['id_seccion']
            ]);

        $this->load->view('administracion/asistencia_reservacion');
        $this->load->view('footer');
        $this->log($this->module['id']);
    }

    function reservacion() {
        $get = $this->input->get();

        if (isset($get['id_reservacion'])) {
            $time_to_pay = 50;
            $data = $this->reservacion->getReservacion($get['id_reservacion'], $time_to_pay);
            if (count($data) == 1) {
                $data = $data[0];
                $data['distribucion'] = $this->reservacion->getItems(md5($data['id_distribucion']));
                $reservacionItems = $this->reservacion->getReservacionItems($get['id_reservacion']);

                foreach ($data['distribucion'] as $key => $value) {
                    $data['distribucion'][$key]['selectable'] = '0';
                    if (in_array($data['distribucion'][$key]['id'], $reservacionItems)) {
                        $data['distribucion'][$key]['current_filter'] = 'selected';
                    }
                }

                $floors = [];
                foreach ($data['distribucion'] as $item) {
                    if (!isset($floors[$item['piso']])) {
                        $floors[$item['piso']] = [];
                    }

                    $floors[$item['piso']][] = $item;
                }

                $data['distribucion'] = array_values($floors);

                $this->json($data);
            } else {
                $this->json([ 'status' => 0 ]);
            }
        }
    }
}
