<?php

class Reservaciones extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->table = 'administracion_reservacion';
        $this->fields = [];
        $this->module = $this->getModule(14);
        $this->id_module = $this->module['id'];
    }

    function index() {
        $this->load->view('main',
            [
                'title' => 'Reservaciones',
                'usuario' => $this->session()['usuario'],
                'id_module' => $this->module['id'],
                'id_section' => $this->module['id_seccion']
            ]);

        $this->load->view('administracion/reservaciones');
        $this->load->view('footer');
        $this->log($this->module['id']);
    }

    public function datatable(){
        $get = $this->input->get();

        $conditions = [];

        if (isset($get['restaurant']) && $get['restaurant'] != '0') {
            $conditions['md5(arr.id)'] = $get['restaurant'];
        }

        if (isset($get['fechas_filtro']) && $get['fechas_filtro'] == 0) {
            if (isset($get['fecha_inicio']) && isset($get['fecha_fin'])) {
                $condicion = "(
                    DATE(ar.fecha) >= DATE('".$get['fecha_inicio']."')
                    AND DATE(ar.fecha) <= DATE('".$get['fecha_fin']."')
                ) OR (
                    DATE(ar.fecha_reservacion) >= DATE('".$get['fecha_inicio']."')
                    AND DATE(ar.fecha_reservacion) <= DATE('".$get['fecha_fin']."')
                )";

                $conditions[$condicion.' AND 1 = '] = '1';
            } else if (isset($get['fecha_inicio'])) {
                $condicion = "(
                    DATE(ar.fecha) >= DATE('".$get['fecha_inicio']."')
                ) OR (
                    DATE(ar.fecha_reservacion) >= DATE('".$get['fecha_inicio']."')
                )";

                $conditions[$condicion.' AND 1 = '] = '1';
            } else if (isset($get['fecha_fin'])) {
                $condicion = "(
                    DATE(ar.fecha) <= DATE('".$get['fecha_fin']."')
                ) OR (
                    DATE(ar.fecha_reservacion) <= DATE('".$get['fecha_fin']."')
                )";

                $conditions[$condicion.' AND 1 = '] = '1';
            }


        } else if (isset($get['fechas_filtro']) && $get['fechas_filtro'] == 1) {
            if (isset($get['fecha_inicio'])) {
                $conditions['DATE(ar.fecha_reservacion) >='] = $get['fecha_inicio'];
            }

            if (isset($get['fecha_fin'])) {
                $conditions['DATE(ar.fecha_reservacion) <='] = $get['fecha_fin'];
            }
        } else if (isset($get['fechas_filtro']) && $get['fechas_filtro'] == 2) {
            if (isset($get['fecha_inicio'])) {
                $conditions['DATE(ar.fecha) >='] = $get['fecha_inicio'];
            }

            if (isset($get['fecha_fin'])) {
                $conditions['DATE(ar.fecha) <='] = $get['fecha_fin'];
            }
        }

        /*if (isset($get['fecha_inicio'])) {
            $conditions['DATE(ar.fecha) >='] = $get['fecha_inicio'];
        }

        if (isset($get['fecha_fin'])) {
            $conditions['DATE(ar.fecha) <='] = $get['fecha_fin'];
        }*/
        $query = [
            'table' => 'administracion_reservacion ar',
            'fields' => 'md5(ar.id) as id, ar.fecha, ar.fecha_reservacion, ar.nombre_persona, ar.estatus_pago, ar.cantidad_adultos, ar.cantidad_menores, ar.asistencia, ar.total_pago, atp.nombre as tipo_pago, arr.nombre, su.nombre as usuario_registro, suu.nombre as usuario_pago, ar.fecha_pago, GROUP_CONCAT(adr.etiqueta SEPARATOR \', \') as asientos, ard.habitacion as habitacion',
            'conditions' => $conditions,
            'filter' => ['ar.nombre_persona'],
            'joins' => [
                'administracion_restaurantes arr' => [
                    'ar.id_restaurante' => 'arr.id'
                ],
                'administracion_tipo_pago atp' => [
                    'ar.id_tipo_pago' => 'atp.id',
                    'type' => 'left'
                ],
                'sistema_usuarios su' => [
                    'ar.id_usuario_registro' => 'su.id'
                ],
                'sistema_usuarios suu' => [
                    'ar.id_usuario_pago' => 'suu.id',
                    'type' => 'left'
                ],
                'administracion_reservacion_mobiliario arm' => [
                    'arm.id_reservacion_detalle' => 'ar.id',
                    'type' => 'left'
                ],
                'administracion_distribucion_restaurante adr' => [
                    'adr.id' => 'arm.id_distribucion_mobiliario',
                    'type' => 'left'
                ],
                'administracion_reservacion_detalles ard' => [
                    'ard.id_reservacion' => 'ar.id',
                    'ard.nombre_persona' => 'ar.nombre_persona',
                    'type' => 'left'
                ],
            ],
            'order' => 'ar.fecha DESC',
            'group' => 'ar.id'
        ];

        $query = $this->filter($get, $query);
        $data = $this->query->select($query);
        unset($query['limit']);
        $count = $this->query->select($query,false,true);
        $this->json([
            'data' => $data,
            'count' => $count
        ]);
    }
}
