<?php

class Pago_reservacion extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->table = 'administracion_reservacion';
        $this->fields = [];
        $this->module = $this->getModule(12);
        $this->id_module = $this->module['id'];
        $this->load->model('ReservacionModel', 'reservacion');
        $this->load->helper('date');
    }

    function index() {
        $this->load->view('main',
            [
                'title' => 'Pago de reservación',
                'usuario' => $this->session()['usuario'],
                'id_module' => $this->module['id'],
                'id_section' => $this->module['id_seccion']
            ]);

        $this->load->view('administracion/pago_reservacion');
        $this->load->view('footer');
        $this->log($this->module['id']);
    }

    public function save(){
        $request_body = file_get_contents('php://input');
        $data = json_decode($request_body,true);
        $response = $this->valid($data);
        if($response == 1){
            $data['id_usuario_pago'] = $this->session()['usuario']['id'];
            $data['fecha_pago'] = mdate("%Y-%m-%d %H:%i:%s");
            $id = $this->query->save($this->table,$data);
            $this->save_action($data,$id);
        }
        else {
            $this->json($response);
        }

    }

    function reservacion() {
        $get = $this->input->get();

        if (isset($get['id_reservacion'])) {
            $time_to_pay = 50;
            $data = $this->reservacion->getReservacion($get['id_reservacion'], $time_to_pay);

            if (count($data) == 1) {
                $this->json($data[0]);
            } else {
                $this->json([ 'status' => 0 ]);
            }
        }
    }

    function metodos() {
        $query = [
            'table' => 'administracion_tipo_pago ap',
            'fields' => 'ap.id, ap.nombre',
            'conditions' => [
                'ap.estatus' => 1
            ],
            'filter' => [],
        ];

        $data = $this->query->select($query);
        $this->json([
            'data' => $data
        ]);
    }

}
