<?php

class Estatus_eventos extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->module = $this->getModule(15);
        $this->id_module = $this->module['id'];
    }

    function index() {
        $this->load->view('main',
            [
                'title' => 'Estatus eventos',
                'usuario' => $this->session()['usuario'],
                'id_module' => $this->module['id'],
                'id_section' => $this->module['id_seccion']
            ]);

        $this->load->view('administracion/estatus_eventos');
        $this->load->view('footer');
        $this->log($this->module['id']);
    }
}
