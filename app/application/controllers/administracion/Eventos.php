<?php
class Eventos extends MY_Controller{

    public function __construct(){
        parent::__construct();
        $this->table = 'administracion_eventos';
        $this->fields = ['nombre',];
        $this->module = $module = $this->getModule(7);
        $this->id_module = $module['id'];
    }
    public function index(){
        $this->load->view('main',
            [
                'title' =>  $this->module['nombre'],
                'usuario' => $this->session()['usuario'],
                'id_module' => $this->module['id'],
                'id_section' => $this->module['id_seccion']
            ]);
        $this->load->view('administracion/eventos');
        $this->load->view('footer');
        $this->log($this->module['id']);
    }

    public function datatable(){
        $query = [
            'table' => 'administracion_eventos ae',
            'fields' => 'md5(ae.id) AS id, ae.privado, ae.costo, ae.costo_menores, ae.nombre, ae.fecha_inicio, ae.fecha_fin, administracion_tipo_eventos.nombre as evento, ae.id_tipo',
            'conditions' => [
                'ae.estatus' => 1
            ],
            'filter' => ['ae.nombre'],
            'joins' => [
                'administracion_tipo_eventos  ' => ['ae.id_tipo' => 'administracion_tipo_eventos.id']
            ]
        ];
        $query = $this->filter($this->input->get(),$query);
        $data = $this->query->select($query);
        unset($query['limit']);
        $count = $this->query->select($query,false,true);
        $this->json([
            'data' => $data,
            'count' => $count
        ]);
    }

    public function dias(){
        $this->json($this->db->get('sistema_dias')->result_array());
    }
    public function detalles($id){
        $this->json($this->db->get_where('administracion_evento_detalle',['md5(id_evento)' => $id,'estatus' => 1])->result_array());
    }
    public function detalle(){
        $request_body = file_get_contents('php://input');
        $data = json_decode($request_body,true);
       $this->db->where('id',$data['id']);
       $this->db->update('administracion_evento_detalle',['estatus' => 0]);
    }


    public function save(){
        $request_body = file_get_contents('php://input');
        $data = json_decode($request_body,true);
        $response = $this->valid($data['record']);

        //$id_restaurante = $data['record']['id_restaurante'];
        if($response == 1){
            $id = $this->query->save($this->table,$data['record']);
            $this->save_action($data['record'],$id);
          if (isset($data['details'])) {
              foreach ($data['details'] as $index => $item) {
                  $data['details'][$index]['id_evento'] = $id;
                  $data['details'][$index]['id'] = md5($data['details'][$index]['id']);
                  $this->query->save('administracion_evento_detalle', $data['details'][$index]);
              }
          }
        }
        else {
            $this->json($response);
        }
    }

    public function saveDistribucion(){
        $request_body = file_get_contents('php://input');
        $data = json_decode($request_body,true);

        $evento = $this->query->select([
            'table' => 'administracion_eventos ae',
            'fields' => 'ae.id',
            'conditions' => [
                'md5(ae.id)' => $data['id_evento']
            ],
            'filter' => [],
        ]);

        $this->query->save('administracion_distribucion', [
            'nombre' => $data['nombre'],
            'id_restaurante' => $data['id_restaurante'],
            'id_evento' => $evento[0]['id']
        ]);
    }

    public function tipos(){
        $query = [
            'table' => 'administracion_tipo_eventos  ',
            'fields' => 'id, nombre',
            'conditions' => [
                'estatus' => 1
            ]
        ];
        $this->json([
            'data' => $this->query->select($query)
        ]);

    }
}
