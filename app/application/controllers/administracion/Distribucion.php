<?php

class Distribucion extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->table = 'administracion_distribucion';
        $this->sub_table = 'administracion_distribucion_restaurante';
        $this->fields = ['nombre', 'id_evento'];
        $this->module = $this->getModule(10);
        $this->id_module = $this->module['id'];

    }

    function index() {
        $this->load->view('main',
            [
                'title' => 'Distribucion',
                'usuario' => $this->session()['usuario'],
                'id_module' => $this->module['id'],
                'id_section' => $this->module['id_seccion']
            ]);

        $this->load->view('administracion/distribucion');
        $this->load->view('footer');
        $this->log($this->module['id']);
    }

    public function datatable(){
        $get = $this->input->get();

        if (isset($get['restaurant'])) {
            $query = [
                'table' => 'administracion_distribucion ad',
                'fields' => 'md5(ad.id) as id, ad.nombre, ad.capacidad, md5(ad.id_evento) as id_evento',
                'conditions' => [
                    'ad.estatus' => 1,
                    'md5(ad.id_restaurante)' => $get['restaurant']
                ],
                'filter' => [/*'su.usuario','su.correo','sp.nombre',*/'ad.nombre'],
            ];
            $query = $this->filter($get,$query);
            $data = $this->query->select($query);
            unset($query['limit']);
            $count = $this->query->select($query,false,true);
            $this->json([
                'data' => $data,
                'count' => $count
            ]);
        } else {
            $this->json([
                'data' => [],
                'count' => 0
            ]);
        }
    }

    public function items() {
        $get = $this->input->get();

        if (isset($get['id_distribucion'])) {

            $query = [
                'table' => 'administracion_distribucion_restaurante ad',
                'fields' => 'md5(id) AS id, relacion_aspecto AS aspect_ratio, id_mobiliario AS imageId, posx_radio As posX_ratio, posy_radio AS posY_ratio, rotacion AS rotation, capacidad AS quantity, seleccionable AS selectable, tamano_radio AS size_ratio, etiqueta AS tag, mesa AS tables, piso',
                'conditions' => [
                    'ad.estatus' => 1,
                    'md5(ad.id_distribucion)' => $get['id_distribucion']
                ]
            ];

            $results = $this->query->select($query);

            $floors = [];
            foreach ($results as $item) {
                if (!isset($floors[$item['piso']])) {
                    $floors[$item['piso']] = [];
                }

                $floors[$item['piso']][] = $item;
            }

            $this->json(['data' => array_values($floors)]);
        } else {
            $this->json(['data' => []]);
        }
    }

    public function save(){
        $this->load->model('RestauranteModel', 'restaurante');
        $request_body = file_get_contents('php://input');
        $data = json_decode($request_body, true);

        $restaurante = $this->query->select([
            'table' => 'administracion_restaurantes ar',
            'fields' => 'ar.id',
            'conditions' => [
                'md5(ar.id)' => $data['id_restaurante']
            ],
            'filter' => [],
        ]);

        $data['id_restaurante'] = $restaurante[0]['id'];

        $evento = $this->query->select([
            'table' => 'administracion_eventos ae',
            'fields' => 'ae.id',
            'conditions' => [
                'md5(ae.id)' => $data['id_evento']
            ],
            'filter' => [],
        ]);

        $data['id_evento'] = $evento[0]['id'];

        $map = [];
        if (isset($data['map'])) {
            $map = $data['map'];
            unset($data['map']);
            $data['capacidad'] = array_reduce($map, function ($total, $piso) {
                return $total + array_reduce($piso, function($acc, $moviliario) {
                    return $acc + $moviliario['quantity'];
                }, 0);
            }, 0);
        }

        $response = $this->valid($data);

        if($response == 1){

            foreach ($data['removed'] as $id) {
                $this->query->save($this->sub_table, [
                    'id' => $id,
                    'estatus' => 0
                ]);
            }

            $this->restaurante->removeFloor($data['id'], $data['removedFloors']);

            unset($data['removed']);
            unset($data['removedFloors']);
            $id = $this->query->save($this->table, $data);

            foreach ($map as $index => $piso) {
                foreach ($piso as $moviliario) {
                    $this->query->save($this->sub_table, [
                        'id' => $moviliario['id'],
                        'id_distribucion' => $id,
                        'relacion_aspecto' => $moviliario['aspect_ratio'],
                        'id_mobiliario' => $moviliario['imageId'],
                        'posx_radio' => $moviliario['posX_ratio'],
                        'posy_radio' => $moviliario['posY_ratio'],
                        'rotacion' => $moviliario['rotation'],
                        'capacidad' => $moviliario['quantity'],
                        'seleccionable' => $moviliario['selectable'],
                        'tamano_radio' => $moviliario['size_ratio'],
                        'etiqueta' => $moviliario['tag'],
                        'mesa' => $moviliario['tables'],
                        'piso' => ($index + 1)
                    ]);
                }
            }
        }
        else {
            $this->json($response);
        }
    }
}
