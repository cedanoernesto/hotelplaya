<?php
class Tipos_eventos extends MY_Controller{

    public function __construct(){
        parent::__construct();
        $this->table = 'administracion_tipo_eventos  ';
        $this->fields = ['nombre',];
        $this->module = $module = $this->getModule(6);
        $this->id_module = $module['id'];
    }
    public function index(){
        $this->load->view('main',
            [
                'title' => $this->module['nombre'],
                'usuario' => $this->session()['usuario'],
                'id_module' => $this->module['id'],
                'id_section' => $this->module['id_seccion']
            ]);
        $this->load->view('administracion/tipos_eventos');
        $this->load->view('footer');
        $this->log($this->module['id']);
    }

    public function datatable(){
        $query = [
            'table' => 'administracion_tipo_eventos  ',
            'fields' => 'md5(id) as id, nombre, importancia',
            'conditions' => [
                'estatus' => 1
            ],
            'filter' => ['nombre'],
        ];
        $query = $this->filter($this->input->get(),$query);
        $data = $this->query->select($query);
        unset($query['limit']);
        $count = $this->query->select($query,false,true);
        $this->json([
            'data' => $data,
            'count' => $count
        ]);
    }

    public function save(){
        $request_body = file_get_contents('php://input');
        $data = json_decode($request_body,true);
        $response = $this->valid($data);
        if($response == 1){
            $id = $this->query->save($this->table,$data);
            $this->save_action($data,$id);
        }
        else {
            $this->json($response);
        }
    }
    public function perfiles(){
        $query = [
            'table' => 'sistema_perfiles',
            'fields' => 'id, nombre',
            'conditions' => [
                'estatus' => 1
            ]
        ];
        $this->json([
            'data' => $this->query->select($query)
        ]);

    }



}