<?php
class Restaurantes extends MY_Controller{

    public function __construct(){
        parent::__construct();
        $this->table = 'administracion_restaurantes  ';
        $this->fields = ['nombre',];
        $this->module = $module = $this->getModule(5);
        $this->id_module = $module['id'];
    }
    public function index(){
        $this->load->view('main',
            [
                'title' => 'Restaurantes',
                'usuario' => $this->session()['usuario'],
                'id_module' => $this->module['id'],
                'id_section' => $this->module['id_seccion']
            ]);
        $this->load->view('administracion/restaurantes');
        $this->load->view('footer');
        $this->log($this->module['id']);
    }

    public function datatable(){
        $query = [
            'table' => 'administracion_restaurantes ar',
            'fields' => 'md5(ar.id) as id, ar.id as _id,ar.nombre, ar.capacidad, ar.descripcion, ar.hora_apertura, ar.hora_cerrado',
            'conditions' => [
                'ar.estatus' => 1
            ],
            'filter' => [/*'su.usuario','su.correo','sp.nombre',*/'ar.nombre'],
        ];
        $query = $this->filter($this->input->get(),$query);
        $data = $this->query->select($query);
        unset($query['limit']);
        $count = $this->query->select($query,false,true);
        $this->json([
            'data' => $data,
            'count' => $count
        ]);
    }

    public function all() {
        $this->load->model('RestauranteModel', 'restaurante');

        $query = [
            'table' => 'administracion_restaurantes ar',
            'fields' => 'md5(ar.id) as id, ar.nombre, ar.capacidad, ar.descripcion, ar.hora_apertura, ar.hora_cerrado, imagen',
            'conditions' => [
                'ar.estatus' => 1
            ],
            'filter' => [],
        ];

        $data = $this->query->select($query);

        foreach ($data as $key => $value) {
            $data[$key]['horas'] = $this->restaurante->getHoursByRest($value['id']);
        }

        $this->json([
            'data' => $data,
        ]);
    }

    public function save(){
        $request_body = file_get_contents('php://input');
        $data = json_decode($request_body,true);
        unset($data['_id']);
        $response = $this->valid($data);
        if($response == 1){
            $id = $this->query->save($this->table,$data);
            $this->save_action($data,$id);
        }
        else {
            $this->json($response);
        }
    }
    public function perfiles(){
        $query = [
            'table' => 'sistema_perfiles',
            'fields' => 'id, nombre',
            'conditions' => [
                'estatus' => 1
            ]
        ];
        $this->json([
            'data' => $this->query->select($query)
        ]);

    }

    public function horario() {
        $data = $this->input->get();
        $this->load->model('RestauranteModel', 'restaurante');
        $horarios = $this->restaurante->getHoursByRest($data['id_restaurante'], $data['fecha'], $data['dia']);

        $this->json([
            'data' => $horarios
        ]);
    }

}
