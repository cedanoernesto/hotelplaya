<?php

class Reservacion extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->table = 'administracion_reservacion';
        $this->table_detalle = 'administracion_reservacion_detalles';
        $this->table_mobiliario = 'administracion_reservacion_mobiliario';
        $this->sub_table = 'administracion_distribucion_restaurante';
        $this->fields = [];
        $this->module = $this->getModule(9);
        $this->id_module = $this->module['id'];

        $this->load->model('ReservacionModel', 'reservacion');

        $this->reservation_config = $this->reservacion->getConfig();
    }

    function index() {
        $this->load->view('main',
            [
                'title' => 'Reservacion',
                'usuario' => $this->session()['usuario'],
                'id_module' => $this->module['id'],
                'id_section' => $this->module['id_seccion']
            ]);

        $this->load->view('kiosko/reservacion');
        $this->load->view('footer');
        $this->log($this->module['id']);
    }

    function save() {
        $request_body = file_get_contents('php://input');
        $data = json_decode($request_body, true);

        $restaurante = $this->query->select([
            'table' => 'administracion_restaurantes ar',
            'fields' => 'ar.id',
            'conditions' => [
                'md5(ar.id)' => $data['id_restaurante']
            ],
            'filter' => [],
        ]);

        $data['id_restaurante'] = $restaurante[0]['id'];

        if (!isset($data['huespedes'])) {
            $data['huespedes'] = [];
        }

        $evento = $this->query->select([
            'table' => 'administracion_eventos ae',
            'fields' => 'ae.costo, ae.costo_menores, ae.nombre',
            'conditions' => [
                'md5(ae.id)' => $data['id_evento']
            ],
            'filter' => [],
        ]);

        if (count($data['huespedes']) == 0 || !$data['huespedes'][0]['todo_incluido']) {
            $data['total_pago'] = ($evento[0]['costo'] * $data['adultos']) + ( $evento[0]['costo_menores'] * $data['menores']);
        } else {
            $data['total_pago'] = 0;
        }

        $response = $this->validate_reservation($data);
        $user = isset($this->session()['usuario']) ? $this->session()['usuario'] : null;

        if ($response == 1) {
            $reservacion = $this->query->save($this->table, [
                'id_restaurante' => $data['id_restaurante'],
                'fecha_reservacion' => $data['fecha'].' '.$data['hora'],
                'nombre_persona' => $data['nombre_persona'],
                'cantidad_adultos' => $data['adultos'],
                'cantidad_menores' => $data['menores'],
                'total_pago' => $data['total_pago'],
                'estatus_pago' => ($data['total_pago'] == 0) ? 1 : 0,
                'id_usuario_registro' => $this->session()['usuario']['id']
            ]);

            foreach ($data['huespedes'] as $huesped) {
                $this->query->save($this->table_detalle, [
                    'nombre_persona' => $huesped['nombre'],
                    'habitacion' => $huesped['habitacion'],
                    'id_reservacion' => $reservacion
                ]);
            }

            $asientos = [];
            foreach ($data['asientos'] as $asiento) {
                $asientos[] = $asiento['tables'].'-'.$asiento['tag'];
                $moviliario = $this->query->select($query = [
                    'table' => 'administracion_distribucion_restaurante ad',
                    'fields' => 'ad.id',
                    'conditions' => [
                        'md5(ad.id)' => $asiento['id']
                    ],
                    'filter' => [],
                ]);
                $id = $this->query->save($this->table_mobiliario, [
                    'id_reservacion_detalle' => $reservacion,
                    'id_distribucion_mobiliario' => $moviliario[0]['id']
                ]);
            }

            $codigo = $this->reservacion->getCode(md5($reservacion), $data);

            $this->json([
                'status' => 1,
                'data' => [
                    'nombre' => $data['nombre_persona'],
                    'codigo' => $codigo,
                    'evento' => $evento[0]['nombre'],
                    'adultos' => $data['adultos'],
                    'menores' => $data['menores'],
                    'fecha' => $data['fecha'],
                    'hora' => $data['hora'],
                    'total' => $data['total_pago'],
                    'usuario_reservacion' => $user,
                    'asientos' => implode($asientos, ', '),
                    'habitacion' => count($data['huespedes']) > 0 ? $data['huespedes'][0]['habitacion'] : null
                ]
            ]);
        } else {
            $this->json($response);
        }


    }

    function barcode($id) {
        $generator = new Picqer\Barcode\BarcodeGeneratorPNG();
        echo $generator->getBarcode($id, $generator::TYPE_CODE_128);
    }

    function config() {
        $this->json($this->reservation_config);
    }

    function items() {
        $data = $this->input->get();

        if (isset($data['asiento_seleccionable']) && $data['asiento_seleccionable'] == '1') {
            $results = $this->reservacion->getItems($data['id_distribucion']);
            $selectedItems = $this->reservacion->getSelectedItems(
                $data['id_distribucion'],
                $data['fecha'],
                $data['hora'],
                $this->reservation_config['reservation_time'],
                $this->reservation_config['time_to_pay']
            );

            $tempSelectedItems = $this->reservacion->getReservTempItems(
                $data['fecha'],
                $data['hora'],
                $this->reservation_config['reservation_time'],
                $this->reservation_config['timeout']
            );

            foreach ($results as $key => $value) {
                if (in_array($value['id'], $selectedItems) || in_array($value['id'], $tempSelectedItems)) {
                    $results[$key]['selectable'] = '0';
                    $results[$key]['current_filter'] = 'reserved';
                } else {
                    $results[$key]['current_filter'] = 'available';
                }
            }

            $available = array_reduce($results, function($acc, $item) {
                return $item['selectable'] !== '0' ? $acc + 1 : $acc;
            }, 0);

            $data = [];
            foreach ($results as $item) {
                if (!isset($data[$item['piso']])) {
                    $data[$item['piso']] = [];
                }

                $data[$item['piso']][] = $item;
            }

            $this->json(['data' => array_values($data), 'available' => $available]);
        } else {
            $available = $this->reservacion->checkAvailability(
                $data['id_distribucion'],
                $data['fecha'],
                $data['hora'],
                $data['dia'],
                $this->reservation_config['reservation_time'],
                $this->reservation_config['time_to_pay']
            );

            $this->json(['data' => [], 'available' => $available]);
        }
    }

    function fakeWebservice() {
        $data = $this->input->get();

        $habitaciones = [];
        $huespedes = $this->reservacion->getGuest(
            $data['habitacion'],
            $data['nombre'],
            $data['fecha'],
            $data['hora']
        );

        foreach ($huespedes as $huesped) {
            $result = $this->reservacion->checkPeopleReservation(
                $huesped['habitacion'],
                $data['fecha'],
                $data['hora'],
                $this->reservation_config['reservation_time'],
                $this->reservation_config['time_to_pay']
            );

            if ($result) {
                $habitaciones[] = $huesped['habitacion'];
            }
        }

        if (count($habitaciones) > 0) {
            $this->json([
                'status' => 0,
                'message' => 'Usted ya tiene una reservacion en esta fecha y hora',
                'message_code' => 'ALREADY_RESERV_GUEST'
            ]);
            return;
        }

        $this->json([
            'data' => $huespedes
        ]);
    }

    function restaurantInfo() {
        $request_body = file_get_contents('php://input');
        $data = json_decode($request_body, true);

        $evento = $this->reservacion->getEvento(
            $data['id_restaurante'],
            $data['fecha'],
            $data['hora'],
            $data['dia'],
            $this->reservation_config['reservation_time']
        );

        if ($evento) {
            $this->json([
                'status' => 1,
                'data' => $evento
            ]);
        } else {
            $this->json([
                'status' => 0,
                'message' => 'No existe evento o distribucion asignada'
            ]);
        }

    }

    public function seat() {
        $request_body = file_get_contents('php://input');
        $data = json_decode($request_body, true);

        foreach ($data['id'] as $id) {
            $available = $this->reservacion->reserSeatTemp(
                $id,
                $data['estatus'],
                $data['fecha'],
                $data['hora'],
                $this->reservation_config['reservation_time'],
                $this->reservation_config['timeout']
            );
        }

        $this->json(['status' => $available ? 1 : 0]);
    }

    private function validate_reservation($data) {
        $habitaciones = [];
        foreach ($data['huespedes'] as $huesped) {
            $result = $this->reservacion->checkPeopleReservation(
                $huesped['habitacion'],
                $data['fecha'],
                $data['hora'],
                $this->reservation_config['reservation_time'],
                $this->reservation_config['time_to_pay']
            );

            if ($result) {
                $habitaciones[] = $huesped['habitacion'];
            }
        }

        $selectedItems = $this->reservacion->getSelectedItems(
            $data['id_distribucion'],
            $data['fecha'],
            $data['hora'],
            $this->reservation_config['reservation_time'],
            $this->reservation_config['time_to_pay']
        );

        return (count($habitaciones) > 0)
            ? [
                'status' => 2,
                'message' =>
                'Las siguientes habitaciones tinen registrada una reservacion al mismo tiempo que la seleccionada: '.implode(', ', $habitaciones),
                'message_code' => 'ALREADY_RESERV',
                'rooms' => $habitaciones
            ]
            : 1;
    }

    function x() {
        $this->reservacion->guestX();
    }
}
