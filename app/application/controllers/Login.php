<?php
class Login extends MY_Controller{

    public function __construct(){
        parent::__construct();
        $this->id_module = 0;
        $this->session_data = $this->session->userdata();
    }

    public function index(){
        if (!isset($this->session()['log'])){
            $this->load->view('main',['title' => 'Inicio de sesión']);
            $this->load->view('login');
            $this->load->view('footer');
        }
        else {
            redirect('dashboard');
        }
    }

    public function login(){
        $get = $this->input->get();
        $query = [
            'table' => 'sistema_usuarios su',
            'fields' => 'su.id as id, su.nombre, su.usuario, su.correo, sp.nombre as perfil, su.id_perfil',
            'conditions' => [
                'usuario' => $get['usuario'],
                'contrasena' => md5($get['contrasena'])

            ],
            'filter' => ['su.usuario','su.correo','sp.nombre','su.nombre'],
            'joins' => [
                'sistema_perfiles sp' => ['su.id_perfil' => 'sp.id']
            ]
        ];
        $user = $this->query->select($query);
        $user = (count($user)>0)?$user[0]:$user;
        if (count($user)>0){
            $this->session->set_userdata([
                'log' => true,
                'usuario' => $user
            ]);
        }
        $session = $this->session->userdata();
        if (isset($session['log'])){
            $message = [
                'status' => 1,
                'message' => 'Bienvenido: '.$user['usuario']
            ];
        }
        else{
            $message = [
                'status' => 0,
                'message' => 'Usuario o contraseña incorrectos.'
            ];
        }
        $this->json($message);
    }
    public function modules($id_section){
        return  $this->db->select('md5(sm.id) as id, sm.id as _id, sm.nombre, sm.icono, sm.url')
            ->join('sistema_perfiles_modulos spm','sm.id = spm.id_modulo')
            ->where([
                'sm.id_seccion' => $id_section,
                'spm.id_perfil' => $this->session_data['usuario']['id_perfil'],
                'sm.estatus' => 1
            ])
            ->get('sistema_modulos sm')->result_array();
    }

    public function sections(){
        $sections = $this->db->select('md5(id) as id,id as _id ,nombre, icono')
            ->where(['estatus' => 1])
            ->get('sistema_secciones')
            ->result_array();
        foreach ($sections as $index => $value){
            $sections[$index]['modules'] = $this->modules($value['_id']);
            if (!count($sections[$index]['modules'])) {
                unset($sections[$index]);
            }
        }
        $this->json($sections);

    }

    function logout(){
        $this->session->sess_destroy();
        redirect('login');
    }

}