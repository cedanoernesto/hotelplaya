<?php
class MY_model extends CI_Model {
    public function __construct(){
        parent::__construct();
    }
    public function save($tabla,$datos,$tipo=1){
        $id=0;
        $datos['id'] =(!isset($datos['id']))?0:$datos['id'];
        if ($datos['id']==md5("0") || $datos['id']=="0"){
            unset($datos['id']);
            $this->db->insert($tabla,$datos);
            $id=$this->db->insert_id();
        }
        else{
            if ($datos['id']!="*")
                $this->db->where("md5(id)",$datos['id']);
            $id=$datos['id'];
            unset($datos['id']);
            $this->db->update($tabla,$datos);
        }
        if ($tipo)
            $this->json(array('id' =>$id,'id_md5' => md5($id),'message'=>'Datos guardados correctamente','estatus' =>1));
        return $id;
    }
    public function json($data){
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

}