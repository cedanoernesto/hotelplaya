<?php
class MY_Controller extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->library('query');
        $this->load->library('client');
    }
    protected $table;
    protected $id_module;

    protected function filter($filter,$query){
        $page = (isset($filter['page']))?$filter['page']:1;
        foreach ($filter as $index => $value){
            switch ($index){
                case 'limit':
                    $offset = $value *($page-1);
                    $query['limit'] = $value;
                    $query['start'] = $offset;
                    break;
                case 'order':
                    if ($filter[$index] != ''){
                        $order = (substr($value,0,1)=='-')?'desc':'asc';
                        $value = ($order=='desc')?substr($value,1):$value;
                        $query['order'] = $value.' '.$order;
                        break;
                    }
                case 'filter':
                    if ($value!=''){
                        $type = 'like';
                        foreach ($query['filter'] as $item) {
                            if (isset($query['conditions'][$type])){
                                $type = 'or_like';
                            }
                            $query['conditions'][$type][$item] = $value;
                        }
                    }
                    break;
            }
        }
        return $query;
    }
    public function valid($data){
        $where = [];
        $fields = [];
        foreach ($data as $index => $value) {
            if (in_array($index,$this->fields)){
                $where[$index] = $value;
            }
        }
        foreach ($where as $index => $value) {
            $condition = ($data['id'] == '0')?[$index => $value]:[$index => $value, 'md5(id) <>' => $data['id']];
            $condition['estatus'] = 1;
            if($this->db->get_where($this->table,$condition)->num_rows()>0){
                $fields[] = $index;
            }
        }
        $response = [
            'status' => 0,
            'message' => 'Ya existe un registro con estos campos: '.implode(', ',$fields)
        ];
        return (count($fields)>0)?$response:true;
    }

    protected function save_action($data,$id_record){
        $session = $this->session();
        $id_action = ($data['id']=='0')?2:3;
        $id_action = (isset($data['estatus']))?4:$id_action;
        $this->query->save(
            'sistema_usuario_accion_modulo  ',
            [
                'id_usuario' => $session['usuario']['id'],
                'id_modulo' => $this->id_module,
                'id_accion' => $id_action,
                'id_afectado' => $id_record,
                'ip' => $this->client->getIp()
            ], 0
        );

    }
    public function save(){
        $request_body = file_get_contents('php://input');
        $data = json_decode($request_body,true);
        $response = $this->valid($data);
        if($response == 1){
            $id = $this->query->save($this->table,$data);
            $this->save_action($data,$id);
        }
        else {
            $this->json($response);
        }
    }

    public function datable($query){
       // $query = $this->filter($this->input->get(),$query);
        $data = $this->query->select($query);
        unset($query['limit']);
        $count = $this->query->select($query,false,true);
        $this->json([
            'data' => $data,
            'count' => $count
        ]);

    }
    protected function log($id_module = 0){
        $session = $this->session();
        $row = $this->db->get_where('sistema_perfiles_modulos  ',[
            'id_modulo' => $id_module,
            'id_perfil' => $session['usuario']['id_perfil'],
            'estatus' => 1
        ])->num_rows();
        if(!$row) {
            redirect('login');
        }
        else {
            $this->query->save(
                'sistema_usuario_accion_modulo  ',
                [
                    'id_usuario' => $session['usuario']['id'],
                    'id_modulo' => $id_module,
                    'id_accion' => 1,
                    'ip' => $this->client->getIp()
                ]
                ,false);
        }
    }
    protected function session(){
        return $this->session->userdata();
    }
    protected function getModule($id){
        return $this->db->get_where('sistema_modulos',['id' => $id])->result_array()[0];
    }

    protected function json($data){
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

}
