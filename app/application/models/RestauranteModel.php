<?php

class RestauranteModel extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getHoursByRest($id_restaurante, $fecha = false, $dia = 0) {
        $this->db->select('DISTINCT aed.hora_inicio', false);

        $this->db->join('administracion_eventos ae', 'ae.id = ad.id_evento');
        $this->db->join('administracion_evento_detalle aed', 'aed.id_evento = ae.id');
        $this->db->where('md5(ad.id_restaurante)', $id_restaurante);
        $this->db->where('aed.estatus', 1);

        if ($fecha) {
            $this->db->where('aed.fecha_inicio <=', $fecha);
            $this->db->where('aed.fecha_fin >=', $fecha);
        }

        if ($dia) {
            $this->db->where('aed.dia <=', $dia + 1);
        }

        $this->db->order_by('aed.hora_inicio', 'asc');

        $results = $this->db->get('administracion_distribucion ad')->result_array();

        return array_map(function($result) {
            return $result['hora_inicio'];
        }, $results);
    }

    function removeFloor($id_dsitribucion, $floors) {
        if (count($floors) > 0) {
            $this->db->where_in('md5(id_distribucion)', $id_dsitribucion);
            $this->db->where_in('piso', $floors);
            $this->db->update('administracion_distribucion_restaurante', [
                'estatus' => 0
            ]);
        }
    }
}
