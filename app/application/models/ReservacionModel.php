<?php

class ReservacionModel extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getConfig() {
        return $this->db->get_where('sistema_reservacion_config', ['id' => 1])->row_array();

    }

    function getEvento($id_restaurante, $date, $time, $day, $intervalo = 0) {
        $intervalo = 0;
        $this->db
            ->select('
                md5(ae.id) as id,
                ae.nombre,
                md5(aed.id) as id_distribucion,
                ae.asiento_seleccionable,
                ae.privado
            ')
            ->join('administracion_tipo_eventos aet', 'aet.id = ae.id_tipo')
            ->join('administracion_distribucion aed', 'aed.id_evento = ae.id','left')
            ->join('administracion_evento_detalle aeed', 'aeed.id_evento = ae.id')
            ->where('aeed.dia', $day + 1)
            ->where('ae.estatus', 1)
            ->where('aeed.estatus', 1)
            ->where('md5(aed.id_restaurante)', $id_restaurante)
            //->where('ae.fecha_inicio <=', $date)
            //->where('ae.fecha_fin >=', $date)
            ->where('aeed.fecha_inicio <=', $date)
            ->where('aeed.fecha_fin >=', $date)
            ->where("IF(aeed.hora_inicio > aeed.hora_fin and aeed.hora_fin > CONVERT('$time', TIME), CONVERT('00:00:00', TIME), aeed.hora_inicio) <= CONVERT('$time', TIME)")
            ->where("IF(aeed.hora_inicio > aeed.hora_fin and aeed.hora_fin < CONVERT('$time', TIME), CONVERT('23:59:59', TIME), aeed.hora_fin) >= IF((CONVERT('$time', TIME) + INTERVAL $intervalo MINUTE) >= CONVERT('23:59:59', TIME) and (CONVERT('00:00:00', TIME) + INTERVAL $intervalo MINUTE) <= aeed.hora_fin, CONVERT('23:59:59', TIME) , (CONVERT('$time', TIME) + INTERVAL $intervalo MINUTE))")
            ->order_by('aet.importancia', 'desc')
            ->order_by('aed.capacidad', 'desc')
            ->limit(1);

        $result = $this->db->get('administracion_eventos ae')->row_array();
        return ($result && isset($result['privado']) && $result['privado'] == '0') ? $result : false;
    }

    function getReservacion($id_reservacion, $time_to_pay, $fecha = '', $tolerancia = 0) {
        $this->db->select("
            distinct md5(ar.id) as id,
            ar.estatus_pago,
            IF(ar.fecha + INTERVAL $time_to_pay MINUTE < NOW() AND ar.estatus_pago = 0, 0, 1) as disponible,
            ar.nombre_persona as nombre,
            DATE_FORMAT(ar.fecha_reservacion, '%Y-%m-%d') as fecha,
            DATE_FORMAT(ar.fecha_reservacion, '%h:%i %p') as hora,
            cantidad_adultos as adultos,
            cantidad_menores as menores,
            ar.total_pago as total,
            ae.nombre as evento,
            adr.id_distribucion", false);
        $this->db->where('get_code(ar.id, ar.fecha_reservacion) =', $id_reservacion);
        $this->db->join('administracion_reservacion_mobiliario arm', 'arm.id_reservacion_detalle = ar.id', 'left');
        $this->db->join('administracion_distribucion_restaurante adr', 'adr.id = arm.id_distribucion_mobiliario', 'left');
        $this->db->join('administracion_distribucion ad', 'ad.id = adr.id_distribucion', 'left');
        $this->db->join('administracion_eventos ae', 'ae.id = ad.id_evento', 'left');

        return $this->db->get('administracion_reservacion ar')->result_array();
    }

    function getReservacionItems($id_reservacion) {
        $this->db->select("md5(adr.id) as id", false);
        $this->db->where('get_code(ar.id, ar.fecha_reservacion) = ', $id_reservacion);
        $this->db->join('administracion_reservacion_mobiliario arm', 'arm.id_reservacion_detalle = ar.id');
        $this->db->join('administracion_distribucion_restaurante adr', 'adr.id = arm.id_distribucion_mobiliario');

        $results = $this->db->get('administracion_reservacion ar')->result_array();

        return array_map(function($item) {
            return $item['id'];
        }, $results);
    }

    function getItems($id_distribucion) {
        $this->db->select('md5(id) AS id, relacion_aspecto AS aspect_ratio, id_mobiliario AS imageId, posx_radio As posX_ratio, posy_radio AS posY_ratio, rotacion AS rotation, capacidad AS quantity, seleccionable AS selectable, tamano_radio AS size_ratio, etiqueta AS tag, mesa AS tables, piso', false);
        $this->db->where('ad.estatus', 1);
        $this->db->where('md5(ad.id_distribucion)', $id_distribucion);

        $results = $this->db->get('administracion_distribucion_restaurante ad')->result_array();

        return $results;
    }

    function checkAvailability($id_distribucion, $fecha, $hora, $dia, $intervalo, $time_to_pay) {
        $intervalo = 0;
        $this->db->select('aed.*', false);
        $this->db->join('administracion_eventos ae', 'ae.id = ad.id_evento');
        $this->db->join('administracion_evento_detalle aed', 'aed.id_evento = ae.id');
        $this->db->where('ad.estatus', 1);
        $this->db->where('aed.estatus', 1);
        $this->db->where('aed.dia', $dia);
        $this->db->where('md5(ad.id)', $id_distribucion);
        $evento = $this->db->get('administracion_distribucion ad')->row_array();

        $this->db->select("ar.cantidad_adultos + ar.cantidad_menores as cantidad, IF(ar.fecha + INTERVAL $time_to_pay MINUTE < NOW() AND ar.estatus_pago = 0, 0, 1) as disponible", false);
        $this->db->join('administracion_restaurantes arr', 'arr.id = ad.id_restaurante');
        $this->db->join('administracion_reservacion ar', 'ar.id_restaurante = arr.id');

        $this->db->where('md5(ad.id)', $id_distribucion);
        $this->db->where('ar.fecha_reservacion <=', "$fecha $hora");
        $this->db->where("(ar.fecha_reservacion + INTERVAL $intervalo MINUTE) >=", "$fecha $hora");
        $this->db->or_where("(ar.fecha_reservacion - INTERVAL $intervalo MINUTE) <=", "$fecha $hora");
        $this->db->where("ar.fecha_reservacion >=", "$fecha $hora");
        $this->db->having('disponible', 1);

        $results = $this->db->get('administracion_distribucion ad')->result_array();

        $cantidad = array_reduce($results, function($acc, $result) {
            return $acc + $result['cantidad'];
        }, 0);

        return $evento['capacidad'] - $cantidad;
    }

    function getSelectedItems($id_distribucion, $fecha, $hora, $intervalo, $time_to_pay) {
        $this->db->select("distinct md5(adr.id) as id, IF(ar.fecha + INTERVAL $time_to_pay MINUTE < NOW() AND ar.estatus_pago = 0, 0, 1) as disponible", false);
        $this->db->join('administracion_reservacion_mobiliario arm', 'arm.id_distribucion_mobiliario = adr.id');
        $this->db->join('administracion_reservacion ar', 'ar.id = arm.id_reservacion_detalle');

        $this->db->where('md5(adr.id_distribucion)', $id_distribucion);
        $this->db->where('ar.fecha_reservacion <=', "$fecha $hora");
        $this->db->where("(ar.fecha_reservacion + INTERVAL $intervalo MINUTE) >=", "$fecha $hora");
        $this->db->or_where("(ar.fecha_reservacion - INTERVAL $intervalo MINUTE) <=", "$fecha $hora");
        $this->db->where("ar.fecha_reservacion >=", "$fecha $hora");
        $this->db->having('disponible', 1);

        $results = $this->db->get('administracion_distribucion_restaurante adr')->result_array();

        return array_map(function($item) {
            return $item['id'];
        }, $results);
    }

    function checkPeopleReservation($habitacion, $fecha, $hora, $intervalo, $time_to_pay) {
        $intervalo = 0;
        $this->db->select("ar.id, IF(ar.fecha + INTERVAL $time_to_pay MINUTE < NOW() AND ar.estatus_pago = 0, 0, 1) as disponible, ard.habitacion", false);
        $this->db->join('administracion_reservacion_detalles ard', 'ard.id_reservacion = ar.id');

        $this->db->where('ar.fecha_reservacion <=', "$fecha $hora");
        $this->db->where("(ar.fecha_reservacion + INTERVAL $intervalo MINUTE) >=", "$fecha $hora");
        $this->db->or_where("(ar.fecha_reservacion - INTERVAL $intervalo MINUTE) <=", "$fecha $hora");
        $this->db->where("ar.fecha_reservacion >=", "$fecha $hora");
        $this->db->having('disponible', 1);
        $this->db->having('habitacion', $habitacion);

        $results = $this->db->get('administracion_reservacion ar')->result_array();
        return count($results) > 0;
    }

    function getCode($id, $data) {
        $this->db->select('get_code(id, fecha_reservacion) as code', false);
        $this->db->where('md5(id)', $id);
        $result = $this->db->get('administracion_reservacion')->row_array();
        return $result['code'];
    }

    function getGuest($habitacion, $nombre, $fecha, $hora) {
        $db_hotel = $this->load->database('hotel', true);
        $db_hotel->select('
            CONCAT(TRIM(h31.guest_name_key_x), TRIM(h31.guest_name_x)) as nombre,
            h31.room_no_x as habitacion,
            h31.no_of_persons_x as adultos,
            h31.no_of_children_x + h31.no_of_juniors_x as menores,
            h31.arrival_date_x, arrival_time_x,
            h31.departure_date_x, departure_time_x,
            h31.all_inclusive_cd_x,
        ', false);
        //$db_hotel->select('*');
        $db_hotel->join('hmrv1b31 h31', 'h31.rsv_no_x = h33.rsv_no_x AND h31.rsv_sub_x = h33.rsv_sub_x');
        $db_hotel->where('h33.room_no_x', $habitacion);
        $db_hotel->like('CONCAT(TRIM(h31.guest_name_key_x), TRIM(h31.guest_name_x))', strtoupper($nombre));

        $results = $db_hotel->get('hmrv1b33 h33')->result_array();
        $valid_results = [];
        foreach ($results as $key => $value) {
            $results[$key]['nombre'] = trim(trim($value['nombre']), '/');
            $results[$key]['total'] = $value['adultos'] + $value['menores'];
            unset($results[$key]['total']);

            $results[$key]['habitacion'] = trim($results[$key]['habitacion']);
            $results[$key]['todo_incluido'] = trim($value['all_inclusive_cd_x']) !== '';
            $arrival = implode('-', str_split($value['arrival_date_x'], 2)).' '.implode(':', str_split($value['arrival_time_x'], 2)).':00';
            $salida = implode('-', str_split($value['departure_date_x'], 2)).' '.implode(':', str_split($value['departure_time_x'], 2)).':00';
            $results[$key]['llegada'] = date('Y-m-d H:i:s', strtotime($arrival));
            $results[$key]['salida'] = date('Y-m-d H:i:s', strtotime($salida));

            unset($results[$key]['arrival_date_x']);
            unset($results[$key]['arrival_time_x']);
            unset($results[$key]['departure_date_x']);
            unset($results[$key]['departure_time_x']);
            unset($results[$key]['all_inclusive_cd_x']);

            //print_r($results);
            if (strtotime("$fecha $hora") >= strtotime($results[$key]['llegada']) && strtotime("$fecha $hora") <= strtotime($results[$key]['salida'])) {
                $valid_results[] = $results[$key];
            }
            //$valid_results[] = $results[$key];
        }

        return $valid_results;
    }

    function getReservTempItems($fecha, $hora, $intervalo, $timeout) {
        $this->db->select('id, id_mobiliario, estatus, timestamp');
        $this->db->where('fecha <=', "$fecha $hora");
        $this->db->where("(fecha + INTERVAL $intervalo MINUTE) >=", "$fecha $hora");
        $this->db->or_where("(fecha - INTERVAL $intervalo MINUTE) <=", "$fecha $hora");
        $this->db->where("fecha >=", "$fecha $hora");
        $this->db->having('estatus', 1);
        $this->db->having('timestamp >= timestamp(DATE_SUB(NOW(), INTERVAL '.($timeout+60).' SECOND))');
        $results = $this->db->get('administracion_reservacion_mobiliario_temp')->result_array();

        return array_map(function($item) {
            return $item['id_mobiliario'];
        }, $results);
    }

    function reserSeatTemp($id, $estatus, $fecha, $hora, $intervalo, $timeout) {
        if ($estatus === 1) {
            $this->db->select('id, id_mobiliario, estatus, timestamp');
            $this->db->where('fecha <=', "$fecha $hora");
            $this->db->where("(fecha + INTERVAL $intervalo MINUTE) >=", "$fecha $hora");
            $this->db->or_where("(fecha - INTERVAL $intervalo MINUTE) <=", "$fecha $hora");
            $this->db->where("fecha >=", "$fecha $hora");
            $this->db->having('id_mobiliario', $id);
            $this->db->having('estatus', 1);
            $this->db->having('timestamp >= timestamp(DATE_SUB(NOW(), INTERVAL '.($timeout+60).' SECOND))');
            $result = $this->db->get('administracion_reservacion_mobiliario_temp')->row_array();

            if (!$result) {
                $this->db->insert('administracion_reservacion_mobiliario_temp', [
                    'id_mobiliario' => $id,
                    'fecha' => "$fecha $hora"
                ]);
                return true;
            }

            return false;
        }

        $this->db->where('id_mobiliario', $id);
        $this->db->where('fecha', "$fecha $hora");
        $this->db->update('administracion_reservacion_mobiliario_temp', [
            'estatus' => 0
        ]);

        return true;
    }

    function guestX(){
        $db_hotel = $this->load->database('hotel', true);
        $db_hotel->select('
            h31.guest_name_x as nombre,
            h33.room_no_x as habitacion,
            h31.no_of_persons_x as total,
            h31.no_of_children_x as menores,
            h31.arrival_date_x, arrival_time_x,
            h31.departure_date_x, departure_time_x,
            h31.all_inclusive_cd_x,
        ', false);
        //$db_hotel->select('*');
        $db_hotel->join('hmrv1b31 h31', 'h31.rsv_no_x = h33.rsv_no_x AND h31.rsv_sub_x = h33.rsv_sub_x');
        //$this->db->where("TRIM(h31.all_inclusive_cd_x) <> ''");
        $results = $db_hotel->get('hmrv1b33 h33')->result_array();
        print_r($results);
    }
}
