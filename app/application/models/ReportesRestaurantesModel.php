<?php
class ReportesRestaurantesModel extends CI_Model{

    public function datatable($get, $type=1){
        /*$offset = $value *($page-1);
        $get['limit'] = $value;
        $get['start'] = $offset;*/
        $group = 'DAYOFMONTH';
        $field = 'dia';
        switch ($get['group']){
            case 1:
                $group = 'HOUR';
                $field = 'hora';
                break;
            case 3:
                $group = 'WEEK';
                $field = 'semana';
                break;
            case 4:
                $group = 'MONTH';
                $field = 'mes';
                break;
        }
        $this->db->select("concat('Semana ', (FLOOR((DAYOFMONTH(ar.fecha_reservacion))/7)+1),' de ', sistema_meses.nombre) AS semana,
                CONCAT(sistema_meses.nombre, ' del ', YEAR(ar.fecha_reservacion)) AS mes,  
                CONCAT( sistema_dias.nombre , ' ' , DAY(ar.fecha_reservacion), ' de ', sistema_meses.nombre ) AS dia, 
                ar.fecha_reservacion AS fecha,  
                ar.fecha_reservacion AS hora, 
                SUM(ar.cantidad_adultos) AS cant_adultos, 
                SUM(ar.cantidad_menores) AS cant_menores, 
                 (SUM(ar.cantidad_adultos) +SUM(ar.cantidad_menores) ) AS cant_total, 
                 SUM(ar.total_pago) AS total_pago",false)
            ->join('sistema_dias','sistema_dias.id=DAYOFWEEK(ar.fecha_reservacion)')
            ->join('sistema_meses','sistema_meses.id=MONTH(ar.fecha_reservacion)')
            ->join('administracion_distribucion  ',"administracion_distribucion.id_restaurante=ar.id_restaurante")
            ->where('md5(administracion_distribucion.id_evento)',$get['id_evento'])
            ->where('ar.estatus_pago',1)
            ->group_by($group.'(ar.fecha_reservacion)');
        if (!$type){
           // $this->db->limit('')
        }

        $data =  $this->db->get('administracion_reservacion ar  ')->result_array();
        foreach ($data as $index => $value) {
            $data[$index]['fecha'] = $value[$field];
        }
        return $data;
    }
}