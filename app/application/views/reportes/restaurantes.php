<script src="<?= base_url('public/js/md-data-table.min.js') ?>"></script>
<link rel="stylesheet" href="<?= base_url('public/css/material-datetimepicker.css') ?>">
<script type="text/javascript" src="<?= base_url('public/js/moment-with-locales.min.js') ?>"></script>
<script src="<?= base_url('public/js/highcharts.js') ?>" ></script>
<script src="<?= base_url('public/js/modules/reportes/restaurantes.js') ?>"></script>
<md-content layout="column" layout-wrap flex="100" ng-controller="module as r" layout-padding>
    <md-card>
        <div layout="column" flex="100">
            <div layout="column" layout-align="center center">
                <h2 layout-align="center center" ><?= $title ?></h2>
            </div>
            <div layout="row" flex="100">
                <md-input-container flex="100">
                    <label for="">Seleccionar restaurante</label>
                    <md-select ng-model="search.id_restaurante" placeholder="Seleccionar restaurante" class="md-no-underline" required >
                        <md-option value="{{record.id}}" ng-repeat="record in restaurants">{{record.nombre}}</md-option>
                    </md-select>
                </md-input-container>
                <md-input-container flex="100">
                    <label>Fecha de inicio</label>
                    <input mdc-datetime-picker date="true" time="false" type="text" short-time="true"
                           show-todays-date
                           format="YYYY-MM-DD"
                           ng-model="search.fecha_inicio"
                           max-date="search.fecha_fin"
                           required>
                </md-input-container>
                <md-input-container flex="100" >
                    <label>Fecha de fin</label>
                    <input mdc-datetime-picker date="true" time="false" type="text" short-time="true"
                           show-todays-date
                           min-date="search.fecha_inicio"
                           format="YYYY-MM-DD"
                           ng-model="search.fecha_fin"
                           required>
                </md-input-container>
                <md-input-container flex="100">
                    <label for="">Seleccionar Evento</label>
                    <md-select ng-model="search.id_evento" placeholder="Seleccionar evento" class="md-no-underline" required >
                        <md-option value="{{event.id}}" ng-repeat="event in events">{{event.nombre}}</md-option>
                    </md-select>
                </md-input-container>
                <md-radio-group ng-model="search.group" class="md-primary" layout="column" >
                    <label for="">Agrupar por</label>
                    <div layout="row"  layout-padding>
                        <md-radio-button ng-repeat="d in radios"
                                         ng-value="d.value"
                                         ng-disabled=" d.isDisabled">
                            {{ d.label }}
                        </md-radio-button>
                    </div>
                </md-radio-group>
            </div>
        </div>
        <md-divider></md-divider>
        <md-table-container>
            <table md-table  ng-model="selected" md-progress="promise">
                <thead md-head md-order="query.order" md-on-reorder="getRecords">
                <tr md-row>
                    <th md-column md-order-by="ar.fecha_reservacion" >
                        <span>Fecha</span>
                    </th>
                    <th md-column md-order-by="ar.cantidad_adultos">
                        <span>Cantidad de adultos</span>
                    </th>
                    <th md-column md-order-by="ar.cantidad_menores">
                        <span>Cantidad de menores</span>
                    </th>
                    <th md-column md-order-by="ar.total_pago">
                        <span>Total de pago</span>
                    </th>
                </tr>
                </thead>
                <tbody md-body>
                <tr md-row md-select="record" md-select-id="_record" md-auto-select ng-repeat="record in records.data" ng-click="showDialog(event,record)" >
                    <td md-cell>{{record.fecha}}</td>
                    <td md-cell>{{record.cant_adultos}}</td>
                    <td md-cell>{{record.cant_menores}}</td>
                    <td md-cell>{{record.total_pago}}</td>
                </tr>
                </tbody>
            </table>
        </md-table-container>
        <md-table-pagination md-limit="query.limit" md-limit-options="[5, 10, 15]" md-page="query.page" md-total="{{records.count}}" md-on-paginate="getRecords" md-page-select></md-table-pagination>
    </md-card>
    <div layout="row" layout-align="end center">
        <md-button class="md-fab" ng-click="showDialog()" ">
        <md-icon>show_chart</md-icon>
        </md-button>
    </div>
</md-content>
