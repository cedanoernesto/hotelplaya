<script src="<?= base_url('public/js/md-data-table.min.js') ?>"></script>
<script src="<?= base_url('public/js/modules/sistema/usuarios.js') ?>"></script>
<md-content layout="column" layout-wrap flex="100" layout-margin ng-controller="usuarios as u" layout-padding>
    <md-card>
        <div layout="column" flex="100">
            <div layout="column" layout-align="center center">
                <h2 layout-align="center center" >Usuarios</h2>
            </div>
            <md-input-container layout="row">
                <md-icon>search</md-icon>
                <input type="text" ng-model="query.filter" placeholder="Busqueda" ng-click="propagation($event)" >
            </md-input-container>
        </div>
        <md-divider></md-divider>
        <md-table-container>
            <table md-table  ng-model="selected" md-progress="promise">
                <thead md-head md-order="query.order" md-on-reorder="getRecords">
                <tr md-row>
                    <th md-column md-order-by="su.nombre" >
                        <span>Nombre</span>
                    </th>
                    <th md-column md-order-by="su.usuario">
                        <span>Usuario</span>
                    </th>
                    <th md-column md-order-by="su.correo">
                        <span>Correo</span>
                    </th>
                    <th md-column md-order-by="sp.nombre">
                        <span>Perfil</span>
                    </th>
                    <th md-column>
                        <span>Eliminar</span>
                    </th>
                </tr>
                </thead>
                <tbody md-body>
                <tr md-row md-select="record" md-select-id="usuario" md-auto-select ng-repeat="record in records.data" ng-click="showDialog(event,record)" >
                    <td md-cell>{{record.nombre}}</td>
                    <td md-cell>{{record.usuario}}</td>
                    <td md-cell>{{record.correo}}</td>
                    <td md-cell>{{record.perfil}}</td>
                    <td md-cell ng-click="remove($event,record.id)"><md-icon>remove_circle_outline</md-icon></td>
                </tr>
                </tbody>
            </table>
        </md-table-container>
        <md-table-pagination md-limit="query.limit" md-limit-options="[5, 10, 15]" md-page="query.page" md-total="{{records.count}}" md-on-paginate="getRecords" md-page-select></md-table-pagination>
    </md-card>
    <div layout="row" layout-align="end center">
        <md-button class="md-fab" ng-click="showDialog(event,{id:'0'})" ">
        <md-icon>add</md-icon>
        </md-button>
    </div>
</md-content>
