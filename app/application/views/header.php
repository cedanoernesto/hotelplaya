<!doctype html>
<html lang="es" ng-app="App">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= (isset($title))?$title:'Sin titulo' ?></title>
    <!-- CSS -->
    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/angular_material/1.1.0/angular-material.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url('public/css/main.css') ?>">
    <link rel="stylesheet" href="<?= base_url('public/css/md-data-table.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('public/css/md-expansion-panel.min.css') ?>">

    <!-- JS -->
    <script src="<?= base_url('public/js/angular/1.6.2/angular.min.js') ?>"></script>
    <script src="<?= base_url('public/js/angular/1.6.2/angular-animate.min.js') ?>"></script>
    <script src="<?= base_url('public/js/angular/1.6.2/angular-aria.min.js') ?>"></script>
    <script src="<?= base_url('public/js/angular/1.6.2/angular-messages.min.js') ?>"></script>
    <script src="<?= base_url('public/js/angular/1.6.2/angular-material.min.js') ?>"></script>
    <script src="<?= base_url('public/js/angular/1.6.2/angular-resource.js') ?>"></script>
    <!--Plugins angular -->
    <script src="<?= base_url('public/js/md-expansion-panel.min.js') ?>"></script>
    <script src="<?= base_url('public/js/angular-material-datetimepicker.js') ?>"></script>
    <script src="<?= base_url('public/js/modules/main.js') ?>"></script>

</head>
<body layout="column" flex ng-controller="appController">
