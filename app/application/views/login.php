<script src="<?= base_url('public/js/md-data-table.min.js') ?>"></script>
<script src="<?= base_url('public/js/modules/login.js') ?>"></script>
<div layout-align="center center" layout="row"  ng-cloak layout-margin ng-controller="login">
   <md-card style="width: 500px;">
       <md-content layout-padding>
           <form name="formLogin">
               <div layout="column" layout-align="center center">
                   <md-progress-linear md-mode="indeterminate" ng-show="showLoader"></md-progress-linear>
                   <p class="md-headline">Iniciar Sesión</p>
               </div>
               <md-divider></md-divider>
               <md-input-container class="md-block">
                  <md-icon>person_outline</md-icon>
                   <input type="text" ng-model="record.usuario" placeholder="Usuario" required md-autofocus>
               </md-input-container>
               <md-input-container class="md-block">
                   <md-icon>lock_outline</md-icon>
                   <input type="password" ng-model="record.contrasena" placeholder="Contraseña" required>
               </md-input-container>
            <div layout-align="end" layout="row">
                <md-button ng-click="login()">Aceptar</md-button>
            </div>
           </form>
       </md-content>
   </md-card>
</div>