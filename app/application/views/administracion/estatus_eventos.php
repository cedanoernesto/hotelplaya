<script src="<?= base_url('public/js/konva.min.js') ?>"></script>
<script src="<?= base_url('public/js/map.js') ?>"></script>
<script src="<?= base_url('public/js/modules/administracion/estatus_eventos.js') ?>"></script>
<md-content layout="column" flex ng-controller="estatus">
    <md-card flex>
        <md-card-content layout="row">
            <div flex=40>
                <h2>Estatus de evento</h2>
                <form name="payForm" layout="column">
                    <md-input-container layout="row" flex>
                        <md-icon>home</md-icon>
                        <label>Restaurante</label>
                        <md-select ng-model="restaurant_select" flex>
                            <md-option
                                ng-repeat="restaurant in restaurants"
                                ng-value="restaurant.id">
                                {{ restaurant.nombre }}
                            </md-option>
                        </md-select>
                    </md-input-container>
                    <md-input-container class="md-input-has-value input-no-margin input-no-padding">
                        <label>{{ translation.LABELS.SELECT_RESERV_DATE }}</label>
                    </md-input-container>
                    <md-datepicker
                        class="datepicker"
                        ng-model="data.reservationdate"
                        md-placeholder="dd/mm/aaaa"
                        md-open-on-focus
                        md-date-locale="es_MX"
                        md-min-date="reservation.mindate"
                        md-max-date="reservation.maxdate"
                        required></md-datepicker>

                    <div layout="row">
                        <md-input-container flex>
                            <label>{{ translation.LABELS.SELECT_RESERV_TIME }}</label>
                            <md-select ng-model="data.reservationtime" required>
                                <md-option ng-value="{{ option.id }}" ng-repeat="option in options">{{ option.value }}</md-option>
                            </md-select>
                            <div class="errors" ng-messages="dateForm.favoriteColor.$error">
                                <div ng-message="required">{{ translation.LABELS.FIELD_REQUIRED }}</div>
                            </div>
                        </md-input-container>
                        <md-button ng-click="getInfo()" class="md-raised md-primary">Verificar</md-button>
                    </div>
                </form>
                <div ng-show="event" layout="column" layout-padding ng-show="event.id">
                    <h2 class="center-text">Informacion</h2>
                    <div layout="row">
                        <span class="bold-label">Evento:</span> {{ event.nombre }}
                    </div>
                    <div layout="row">
                        <span class="bold-label">Asientos disponibles:</span> {{ event.available }}
                    </div>
                </div>
            </div>
            <div flex=60 ng-controller="map" layout="column" layout-align="center center">
                <div id="map-container" style="width:100%;"></div>
            </div>
        </md-card-content>
    </md-card>
</md-content>
