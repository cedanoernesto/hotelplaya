<script src="<?= base_url('public/js/md-data-table.min.js') ?>"></script>
<script src="<?= base_url('public/js/modules/administracion/distribucion.js') ?>"></script>
<script src="<?= base_url('public/js/konva.min.js') ?>"></script>
<script src="<?= base_url('public/js/map.js') ?>"></script>
<style media="screen">
    #container {
        max-width: 100%;
        background-color: #fff;
    }

    .no-input-content {
        margin-bottom: 0;
        padding: 0;
    }

    .no-margin-bottom {
        margin-bottom: 0;
    }
</style>
<md-content layout="column" layout-wrap flex="90" flex-offset="5" flex-offset-sm="0" ng-controller="distribucion as d" layout-padding ng-hide="map.isMapShown">
    <md-card>
        <div layout="column" flex="100">
            <div layout="column" layout-align="center center">
                <h2 layout-align="center center" ><?= $title ?></h2>
            </div>
            <md-input-container layout="row">
                <md-icon>home</md-icon>
                <label>Restaurante</label>
                <md-select ng-model="restaurant_select" flex>
                    <md-option
                        ng-repeat="restaurant in restaurants"
                        ng-value="restaurant.id">
                        {{ restaurant.nombre }}
                    </md-option>
                </md-select>
            </md-input-container>
            <md-input-container layout="row">
                <md-icon>search</md-icon>
                <input type="text" ng-model="query.filter" placeholder="Busqueda" ng-click="propagation($event)" >
            </md-input-container>
        </div>
        <md-divider></md-divider>
        <md-table-container>
            <table md-table  ng-model="selected" md-progress="promise">
                <thead md-head md-order="query.order" md-on-reorder="getRecords">
                <tr md-row>
                    <th md-column md-order-by="ad.nombre" >
                        <span>Nombre</span>
                    </th>
                    <th md-column md-order-by="ad.capacidad">
                        <span>Capacidad</span>
                    </th>
                    <th md-column>
                        <span>Eliminar</span>
                    </th>
                </tr>
                </thead>
                <tbody md-body>
                <tr md-row md-select="record" md-select-id="_record" md-auto-select ng-repeat="record in records.data" ng-click="showDialog(event,record)" >
                    <td md-cell>{{record.nombre}}</td>
                    <td md-cell>{{record.capacidad}}</td>
                    <td md-cell ng-click="remove($event,record.id)"><md-icon>remove_circle_outline</md-icon></td>
                </tr>
                </tbody>
            </table>
        </md-table-container>
        <md-table-pagination md-limit="query.limit" md-limit-options="[5, 10, 15]" md-page="query.page" md-total="{{records.count}}" md-on-paginate="getRecords" md-page-select></md-table-pagination>
    </md-card>
    <div layout="row" layout-align="end center">
        <md-button class="md-fab" ng-click="showMap()">
        <md-icon>add</md-icon>
        </md-button>
    </div>
</md-content>
<md-content ng-controller="map" ng-show="map.isMapShown" layout="column">
    <md-content layout="row">
        <md-content flex layout="column">
            <div layout="row">
                <div flex>
                    <md-button
                        ng-repeat="floor in map.floors"
                        ng-class="{'md-accent' : $index == currentIndex}"
                        ng-click="showFloor($index)">
                            Piso #{{ $index + 1 }}
                        </md-button>
                </div>
                <md-button ng-click="addFloor()">Agregar</md-button>
                <md-button ng-click="removeFloor()">Eliminar</md-button>
            </div>
            <div id='container' flex></div>
        </md-content>
        <md-sidenav
            class="md-sidenav-right"
            md-component-id="right"
            md-is-locked-open="true"
            md-whiteframe="4">

            <md-toolbar class="md-theme-indigo" layout="row" layout-align="center center">
                <h1 class="md-toolbar-tools">Configuración</h1>
                <md-button class="md-icon-button" ng-click="save()"><md-icon>check</md-icon></md-button>
                <md-button class="md-icon-button" ng-click="cancel()"><md-icon>clear</md-icon></md-button>
            </md-toolbar>

            <md-content layout="column" flex=100>
                <section>
                    <md-subheader class="md-primary">Distribución</md-subheader>
                    <section layout-padding layout="column">
                        <form name="distributionForm" layout="column">
                            <md-input-container class="no-margin-bottom" flex>
                                <label>Nombre</label>
                                <input type="text" ng-model="record.nombre" required>
                                <div class="errors" ng-messages="distributionForm.favoriteColor.$error">
                                    <div ng-message="required">Campo requerido</div>
                                </div>
                            </md-input-container>
                            <md-input-container class="no-margin-bottom" flex>
                                <label>Evento</label>
                                <md-select ng-model="record.id_evento" required>
                                    <md-option
                                        ng-repeat="event in events"
                                        ng-value="event.id">
                                        {{ event.nombre }}
                                    </md-option>
                                </md-select>
                                <div class="errors" ng-messages="distributionForm.favoriteColor.$error">
                                    <div ng-message="required">Campo requerido</div>
                                </div>
                            </md-input-container>
                        </form>
                    </section>
                </section>
                <section ng-show="showSettings">
                    <section layout="column">
                        <md-button class="md-raised md-warn" flex="100" ng-click="removeItem()">Eliminar elemento</md-button>
                    </section>
                </section>
                <section>
                    <md-subheader class="md-primary">Simbolo</md-subheader>
                    <section layout-padding>
                        <md-input-container layout="row" class="md-block">
                            <label>Imagen</label>
                            <md-select ng-model="selectedOption" ng-change="updateImage()">
                                <md-option
                                    ng-repeat="option in options"
                                    ng-value="option.id">
                                    {{ option.nombre }}
                                </md-option>
                            </md-select>
                        </md-input-container>

                        <md-input-container class="md-block md-input-has-value no-input-content">
                            <label>Tamaño</label>
                        </md-input-container>
                        <md-slider-container>
                            <md-slider flex min="0.1" max="3" step="0.1" ng-model="scale" ng-change="updateObjScale()"></md-slider>
                            <md-input-container>
                                <input type="number" ng-model="scale">
                            </md-input-container>
                        </md-slider-container>

                        <md-input-container class="md-block no-margin-bottom">
                            <label>Rotar</label>
                            <input type="number" ng-model="degrees">
                        </md-input-container>

                        <section layout="row" flex>
                            <md-button
                                class="md-raised"
                                ng-click="rotate(-10)"
                                flex="50"
                                ng-model="degrees">
                                Izquierda
                            </md-button>
                            <md-button
                                class="md-raised"
                                ng-click="rotate(10)"
                                flex="50"
                                ng-model="degrees">
                                Derecha
                            </md-button>
                        </section>
                    </section>
                </section>

                <section>
                    <md-subheader class="md-primary">Interaccion</md-subheader>
                    <section layout-padding>
                        <md-input-container class="md-block md-input-has-value no-input-content">
                            <label>Seleccionable</label>
                        </md-input-container>
                        <md-switch ng-model="selectable" ng-change="updateSelectable()">
                            {{ (selectable)
                                ? 'Seleccionable por el usuario'
                                : 'NO Seleccionable por el usuario'
                            }}
                        </md-switch>
                    </section>
                </section>

                <section>
                    <md-subheader class="md-primary">Informacion</md-subheader>
                    <section layout-padding flex layout="column">
                        <div layout="row">
                            <md-input-container flex>
                                <label>Etiqueta</label>
                                <input type="text" ng-model="tag" ng-change="updateTag()">
                            </md-input-container>
                            <md-input-container flex>
                                <label>Etiqueta</label>
                                <input type="text" ng-model="table" ng-change="updateTable()">
                            </md-input-container>
                        </div>
                        <md-input-container flex>
                            <label>Capacidad</label>
                            <input type="number" min="0" ng-model="quantity" ng-change="updateQuantity()">
                        </md-input-container>
                    </section>
                </section>

                <!-- <button ng-click="getInfo()">Info</button> -->
            </md-content>
        </md-sidenav>
    </md-content>
</md-content>
