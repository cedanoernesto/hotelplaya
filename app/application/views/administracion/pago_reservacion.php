<script src="<?= base_url('public/js/modules/administracion/pago_reservacion.js') ?>"></script>

<md-content layout="row" layout-align="center top" flex ng-controller="pay">
    <md-card flex=50>
        <md-card-content>
            <h2>Buscar reservación</h2>
            <form name="payForm" layout="row">
                <md-input-container flex=70>
                    <label>Código de reservación</label>
                    <input type="text" ng-model="reservation_code">
                </md-input-container>
                <div flex=30 layout-align="center center" layout="row">
                    <md-button class="md-raised md-primary" ng-click="getInfo()">Verificar</md-button>
                </div>
            </form>
            <div ng-show="reservation" layout="column" layout-padding ng-show="reservation.id">
                <h2 class="center-text">Informacion de reservacion</h2>
                <div layout="row">
                    <span class="bold-label">Nombre:</span> {{ reservation.nombre }}
                </div>
                <div layout="row">
                    <div flex=50>
                        <span class="bold-label">Evento:</span> {{reservation.evento}}
                    </div>
                    <div flex=50>
                        <span class="bold-label">Asistentes:</span> {{reservation.adultos}} adultos y {{reservation.menores}} menores
                    </div>
                </div>
                <div layout="row">
                    <div flex=50>
                        <span class="bold-label">Fecha:</span> {{reservation.fecha}}
                    </div>
                    <div flex=50>
                        <span class="bold-label">Hora:</span> {{reservation.hora}}
                    </div>
                </div>
                <div layout="column" class="center-text">
                    <h2>Total a pagar</h2>
                    <span style="font-size: 3em;">{{reservation.total | currency }}</span>
                </div>
                <div layout="column">
                    <form name="payMethodForm" layout="column">
                        <md-input-container>
                            <label>Metodo de pago</label>
                            <md-select ng-model="id_metodo_pago" required>
                                <md-option ng-repeat="method in methods" ng-value="method.id">
                                    {{method.nombre}}
                                </md-option>
                            </md-select>
                        </md-input-container>
                    </form>
                </div>
                <div layout="column" layout-align="center center" flex>
                    <md-button class="md-raised md-primary" flex=30 ng-click="pay()">Pagar</md-button>
                </div>
            </div>
        </md-card-content>
    </md-card>
</md-content>
