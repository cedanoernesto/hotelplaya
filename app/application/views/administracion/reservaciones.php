<script src="<?= base_url('public/js/md-data-table.min.js') ?>"></script>
<script src="<?= base_url('public/js/modules/administracion/reservaciones.js') ?>"></script>
<style media="screen">
</style>
<md-content layout="column" layout-wrap flex="100" ng-controller="reservaciones as r" layout-padding>
    <md-card>
        <div layout="column" flex="100">
            <div layout="column" layout-align="center center">
                <h2 layout-align="center center" ><?= $title ?></h2>
            </div>
            <div layout="row">
                <md-input-container layout="row" flex>
                    <md-icon>search</md-icon>
                    <input type="text" ng-model="query.filter" placeholder="Busqueda por nombre" ng-click="propagation($event)" >
                </md-input-container>
            </div>
            <div layout="row" layout-align="start center">
                <md-input-container layout="row" flex=50>
                    <md-icon>home</md-icon>
                    <label>Restaurante</label>
                    <md-select ng-model="restaurant_select" flex>
                        <md-option
                            ng-value="0">
                            Todos
                        </md-option>
                        <md-option
                            ng-repeat="restaurant in restaurants"
                            ng-value="restaurant.id">
                            {{ restaurant.nombre }}
                        </md-option>
                    </md-select>
                </md-input-container>
                <div layout="column" flex>
                    <label flex=10 style="margin-left: 2em;">Rango de fechas</label>
                    <md-datepicker ng-model="fecha_inicio" md-placeholder="Fecha inicio" md-open-on-focus></md-datepicker>
                    <md-datepicker ng-model="fecha_fin" md-placeholder="Fecha fin" md-open-on-focus></md-datepicker>
                </div>
                <div flex>
                    <md-radio-group ng-model="fechas_filtro" flex>
                        <md-radio-button value="0" class="md-primary">Ambos</md-radio-button>
                        <md-radio-button value="1">Fecha del evento</md-radio-button>
                        <md-radio-button value="2">Fecha de reservación</md-radio-button>
                    </md-radio-group>
                </div>
            </div>
        </div>
        <md-divider></md-divider>
        <md-table-container>
            <table md-table  ng-model="selected" md-progress="promise">
                <thead md-head md-order="query.order" md-on-reorder="getRecords">
                <tr md-row>
                    <th md-column md-order-by="ar.nombre_persona" >
                        <span>Nombre</span>
                    </th>
                    <th md-column md-order-by="arr.nombre">
                        <span>Restaurante</span>
                    </th>
                    <th md-column md-order-by="ar.fecha_reservacion">
                        <span>Fecha del evento</span>
                    </th>
                    <th md-column md-order-by="ar.fecha">
                        <span>Fecha de reservación</span>
                    </th>
                </tr>
                </thead>
                <tbody md-body>
                <tr md-row md-select="record" md-select-id="_record" md-auto-select ng-repeat="record in records.data" ng-click="showDialog(event,record)" >
                    <td md-cell>{{record.nombre_persona}}</td>
                    <td md-cell>{{record.nombre}}</td>
                    <td md-cell>{{record.fecha_reservacion}}</td>
                    <td md-cell>{{record.fecha}}</td>
                </tr>
                </tbody>
            </table>
        </md-table-container>
        <md-table-pagination md-limit="query.limit" md-limit-options="[5, 10, 15]" md-page="query.page" md-total="{{records.count}}" md-on-paginate="getRecords" md-page-select></md-table-pagination>
    </md-card>
</md-content>
