<script src="<?= base_url('public/js/konva.min.js') ?>"></script>
<script src="<?= base_url('public/js/map.js') ?>"></script>
<script src="<?= base_url('public/js/modules/administracion/asistencia_reservacion.js') ?>"></script>
<md-content layout="column" flex ng-controller="check">
    <md-card flex>
        <md-card-content layout="row">
            <div flex=40>
                <h2>Buscar reservación</h2>
                <form name="payForm" layout="row">
                    <md-input-container flex=70>
                        <label>Código de reservación</label>
                        <input type="text" ng-model="reservation_code">
                    </md-input-container>
                    <div flex=30 layout-align="center center" layout="row">
                        <md-button class="md-raised md-primary" ng-click="getInfo()">Verificar</md-button>
                    </div>
                </form>
                <div ng-show="reservation" layout="column" layout-padding ng-show="reservation.id">
                    <h2 class="center-text">Informacion de reservacion</h2>
                    <div layout="row">
                        <span class="bold-label">Nombre:</span> {{ reservation.nombre }}
                    </div>
                    <div layout="row">
                        <div flex=50>
                            <span class="bold-label">Evento:</span> {{reservation.evento}}
                        </div>
                        <div flex=50>
                            <span class="bold-label">Asistentes:</span> {{reservation.adultos}} adultos y {{reservation.menores}} menores
                        </div>
                    </div>
                    <div layout="row">
                        <div flex=50>
                            <span class="bold-label">Fecha:</span> {{reservation.fecha}}
                        </div>
                        <div flex=50>
                            <span class="bold-label">Hora:</span> {{reservation.hora}}
                        </div>
                    </div>
                </div>
            </div>
            <div flex=60 ng-controller="map" layout="column" layout-align="center center">
                <div layout="row">
                    <div flex>
                        <md-button
                            ng-repeat="floor in map.floors"
                            ng-class="{'md-accent' : $index == map.index}"
                            ng-click="showFloor($index)">
                                Piso #{{ $index + 1 }}
                            </md-button>
                    </div>
                </div>
                <div id="map-container" style="width:100%;"></div>
            </div>
        </md-card-content>
    </md-card>
</md-content>
