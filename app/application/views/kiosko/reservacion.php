<link href="<?= base_url('public/css/md-steppers.min.css') ?>" rel="stylesheet">
<script src="<?= base_url('public/js/md-steppers.min.js') ?>"></script>
<script src="<?= base_url('public/js/modules/kiosko/reservacion.js') ?>"></script>
<script src="<?= base_url('public/js/konva.min.js') ?>"></script>
<script src="<?= base_url('public/js/map.js') ?>"></script>
<script src="<?= base_url('public/js/moment.js') ?>"></script>
<script src="<?= base_url('public/js/humanize-duration.js') ?>"></script>
<script src="<?= base_url('public/js/angular-timer.min.js') ?>"></script>
<style media="screen">
    md-content {
        background-color: #fff !important;
    }

    .center {
        text-align: center;
    }

    .input-no-margin {
        margin: 0;
    }

    .input-no-padding {
        padding: 0;
    }

    .picture-description img {
        width: 100%;
    }

    .description {
        display: flex;
        align-items: center;
    }
    .card-container {
        display: flex;
        flex-flow: row wrap;
        justify-content: space-around;
    }

    .card-container > md-card {
        width: 30%;
        min-width: 300px;
        cursor: pointer;
    }

    @media all and (max-width: 650px) {
        .card-container > md-card {
            width: 90%;
        }
    }

    .fade.ng-hide {
        opacity: 0;
    }

    .fade.ng-hide-remove,
    .fade.ng-hide-add {
        display: block !important;
    }

    .fade.ng-hide-remove {
        transition: all linear 1200ms;
    }

    .fade.ng-hide-add {
        transition: all linear 100ms;
    }

    .capitilized {
        text-transform: capitalize;
    }

    .first-letter-upper::first-letter {
        text-transform: uppercase;
    }

    .select_quantity {
        font-size: 20px;
        text-align: center;
    }

    .select_quantity .description {
        display: inline-block;
        width: 100px;
    }

    .select_quantity .quantity {
        display: inline-block;
        font-size: 48px;
        vertical-align: middle;
        width: 50px;
    }

    .datepicker .md-datepicker-input-container {
        width: 80%;
    }

    .datepicker .md-datepicker-input {
        width: 100%;
    }

    .selected-seats > *{
        font-size: 24px;
        text-align: center;
        margin: 1em 0;
    }

    .selected-seats .seats {
        font-weight: bold;
    }

    .label-bold {
        font-weight: bold;
        display: inline-block;
        margin-right: 10px;
    }

    .info > * {
        margin-top: 5px;
    }
</style>
<md-content ng-controller="KioskoController" layout="column" ng-cloak flex>

    <md-content class="md-padding" layout="column" ng-show="selectedIndex == 0" layout-align="center center" flex-gt-xs=100>
        <div class="card-container" layout-xs="column" layout="row">
            <md-card ng-click="selectDate(restaurant)" ng-repeat="restaurant in restaurants">
                <img ng-src="<?=base_url('/public/assets/images/restaurantes/')?>{{ restaurant.imagen }}" alt="{{ restaurant.nombre }}">
                <md-card-title>
                    <md-card-title-text>
                        <span class="md-headline">{{ restaurant.nombre }}</span>
                    </md-card-title-text>
                </md-card-title>
                <md-card-content style="overflow-y: auto;">
                    <p>
                        {{ restaurant.descripcion }}
                    </p>
                </md-card-content>
            </md-card>
        </div>
    </md-content>

    <md-content class="md-padding" ng-show="selectedIndex == 1 && !map.isMapShown" flex=100 layout="column">
        <div layout="row" flex=100>
            <md-card flex-gt-xs=30>
                <img ng-src="<?=base_url('/public/assets/images/restaurantes/')?>{{ data.selectedRestaurant.imagen }}" alt="{{ data.selectedRestaurant.nombre }}">
                <md-card-title>
                    <md-card-title-text>
                        <span class="md-headline">{{ data.selectedRestaurant.nombre }}</span>
                    </md-card-title-text>
                </md-card-title>
                <md-card-content style="overflow-y: auto;">
                    <p>{{ data.selectedRestaurant.descripcion }}</p>
                </md-card-contetnt>
            </md-card>
            <md-card flex-gt-xs=70>
                <md-stepper-card-content flex layout="column">
                    <div flex=25 layout-padding class="info">
                        <div layout="row" flex>
                            <div flex-gt-xs=50 flex-xs=100>
                                <span class="label-bold">{{ translation.LABELS.RESERV_DATE }}:</span>
                                <span class="first-letter-upper"> {{ data.reservationdate.toLocaleDateString(translation.LOCALE, {
                                    weekday: 'long',
                                    day: 'numeric',
                                    month: 'long',
                                    year: 'numeric'
                                }) }} </span>
                            </div>
                            <div flex-gt-xs=40 flex-xs=100>
                                <div ng-show="state.selectedDate()">
                                    <span class="label-bold">{{ translation.LABELS.RESERV_TIME }}: </span>
                                    <span>{{ state.selectedDate().value }}</span>
                                </div>
                            </div>
                            <div flex=10 layout="row" layout-align="end top">
                                <md-button class="md-icon-button" ng-click="cancel()">
                                    <md-icon>home</md-icon>
                                </md-button>
                            </div>
                        </div>
                        <div ng-show="data.customers[0] || data.no_huesped_nombre">
                            <span class="label-bold">{{ translation.LABELS.RESERV_CUSTOMER }}: </span>
                            <span ng-show="data.customers[0]">{{ data.customers[0].nombre }}</span>
                            <span ng-show="data.no_huesped_nombre">{{ data.no_huesped_nombre }}</span>
                        </div>
                        <div layout="row" ng-show="data.quantity.adults + data.quantity.children > 0">
                            <div flex-gt-xs=50 flex-xs=100>
                                <span class="label-bold">{{ translation.LABELS.RESERV_NUM_SEATS }}:</span>
                                <span>{{ data.quantity.adults + data.quantity.children }}</span>
                            </div>
                            <div flex-gt-xs=50 flex-xs=100 ng-show="data.selectedSeats.length > 0">
                                <span class="label-bold">{{ translation.LABELS.RESREV_SELECTED_SEATS }}: </span>
                                <span>{{ state.getSelectedSeats() }}</span>
                            </div>
                        </div>
                    </div>
                    <md-steppers md-dynamic-height md-stretch-steppers="always" flex=65 layout="column" md-selected="step">
                        <md-step
                            label="{{ translation.LABELS.STEP_SELECT_DATE }}"
                            md-complete="false"
                            ng-disabled="step < 0">
                            <md-content layout="column" layout-padding ng-controller="DateController">
                                <h2 class="md-headline center">{{ translation.LABELS.SELECT_H_RESERV_DATE }}</h2>
                                <div layout="row"  layout-align="center center">
                                    <form name="dateForm" flex-gt-xs=50 layout-padding layout="column">
                                        <md-input-container class="md-input-has-value input-no-margin input-no-padding">
                                            <label>{{ translation.LABELS.SELECT_RESERV_DATE }}</label>
                                        </md-input-container>
                                        <md-datepicker
                                            class="datepicker"
                                            ng-model="data.reservationdate"
                                            md-placeholder="dd/mm/aaaa"
                                            md-open-on-focus
                                            md-date-locale="es_MX"
                                            md-min-date="reservation.mindate"
                                            md-max-date="reservation.maxdate"
                                            required></md-datepicker>

                                        <md-input-container>
                                            <label>{{ translation.LABELS.SELECT_RESERV_TIME }}</label>
                                            <md-select ng-model="data.reservationtime" required>
                                                <md-option ng-value="{{ option.id }}" ng-repeat="option in options">{{ option.value }}</md-option>
                                            </md-select>
                                            <div class="errors" ng-messages="dateForm.favoriteColor.$error">
                                                <div ng-message="required">{{ translation.LABELS.FIELD_REQUIRED }}</div>
                                            </div>
                                        </md-input-container>
                                    </form>
                                </div>
                            </md-content>
                        </md-step>
                        <md-step
                            label="{{ translation.LABELS.STEP_SELECT_INFO }}"
                            md-complete="false"
                            ng-disabled="step < 1">
                            <md-content layout="column" layout-align="center center" ng-controller="RoomController">
                                <h2 class="md-headline">{{ translation.LABELS.SELECT_H_RESERV_ROOM }}</h2>
                                <form name="roomForm" layout="column" flex=30>
                                    <md-input-container>
                                        <label>{{ translation.LABELS.SELECT_RESERV_ROOM }}</label>
                                        <input ng-model="data.room.number" required/>
                                        <div class="errors" ng-messages="dateForm.favoriteColor.$error">
                                            <div ng-message="required">{{ translation.LABELS.FIELD_REQUIRED }}</div>
                                        </div>
                                    </md-input-container>
                                    <md-input-container>
                                        <label>{{ translation.LABELS.SELECT_RESERV_NAME }}</label>
                                        <input ng-model="data.room.name" required/>
                                        <div class="errors" ng-messages="dateForm.favoriteColor.$error">
                                            <div ng-message="required">{{ translation.LABELS.FIELD_REQUIRED }}</div>
                                        </div>
                                    </md-input-container>
                                </form>
                            </md-content>
                        </md-step>
                        <md-step
                            label="{{ translation.LABELS.STEP_SELECT_NUM }}"
                            md-complete="false"
                            ng-disabled="step < 2">
                            <md-content layout="column" layout-align="center center" ng-controller="QuantityController">
                                <h2 class="md-headline">{{ translation.LABELS.SELECT_H_RESERV_QUANTITY }}</h2>
                                <div flex=30 layout="column" class="select_quantity">
                                    <div layout-align="center center">
                                        <span class="description">{{ translation.LABELS.SELECT_RESERV_ADULTS }}</span>
                                        <md-button
                                            class="md-icon-button"
                                            ng-style="{'font-size': '64px', 'height': '64px', 'width': '64px'}"
                                            ng-click="addQuantityAdults(-1)">
                                            <md-icon
                                                ng-style="{'font-size': '48px', 'height': '48px', 'width': '48px'}">
                                                remove_circle_outline
                                            </md-icon>
                                        </md-button>
                                        <span class="quantity">{{ data.quantity.adults }}</span>
                                        <md-button
                                            class="md-icon-button"
                                            ng-style="{'font-size': '64px', 'height': '64px', 'width': '64px'}"
                                            ng-click="addQuantityAdults(1)">
                                            <md-icon
                                                ng-style="{'font-size': '48px', 'height': '48px', 'width': '48px'}">
                                                add_circle_outline
                                            </md-icon>
                                        </md-button>
                                    </div>
                                    <div>
                                        <span class="description">{{ translation.LABELS.SELECT_RESERV_CHILDREN }}</span>
                                        <md-button
                                            class="md-icon-button"
                                            ng-style="{'font-size': '64px', 'height': '64px', 'width': '64px'}"
                                            ng-click="addQuantityChildren(-1)">
                                            <md-icon
                                                ng-style="{'font-size': '48px', 'height': '48px', 'width': '48px'}">
                                                remove_circle_outline
                                            </md-icon>
                                        </md-button>
                                        <span  class="quantity">{{ data.quantity.children }}</span>
                                        <md-button
                                            class="md-icon-button"
                                            ng-style="{'font-size': '64px', 'height': '64px', 'width': '64px'}"
                                            ng-click="addQuantityChildren(1)">
                                            <md-icon
                                                ng-style="{'font-size': '48px', 'height': '48px', 'width': '48px'}">
                                                add_circle_outline
                                            </md-icon>
                                        </md-button>
                                    </div>
                                    <div>
                                        <span class="description">{{ translation.LABELS.SELECT_RESERV_TOTAL }}</span>
                                        <span class="quantity">{{ data.quantity.adults + data.quantity.children }}</span>
                                    </div>
                                </div>
                            </md-content>
                        </md-step>
                        <md-step
                            label="{{ translation.LABELS.STEP_SELECT_SEATS }}"
                            md-complete="false"
                            ng-disabled="step < 3">
                            <md-content layout="column" ng-controller="SeatsController">
                                <h2 class="md-headline center">{{ translation.LABELS.SELECT_H_RESERV_SEATS }}</h2>
                                <div layout="row" layout-padding>
                                    <div flex=50 class="selected-seats">
                                        <div>{{ translation.LABELS.SELECT_RESERV_SELECTED_SEATS }}</div>
                                        <div class="seats">{{ state.getSelectedSeats() }}</div>
                                    </div>
                                    <div flex=50 layout="column" layout-align="center center">
                                        <md-button class="md-raised md-primary" ng-click="state.showMap()">{{ translation.LABELS.SELECT_RESERV_SHOW_MAP }}</md-button>
                                    </div>
                                </div>
                            </md-content>
                        </md-step>
                    </md-steppers>
                    <md-content layout="row" flex>
                        <div flex=50 layout-padding>
                            {{ translation.LABELS.TIME_LEFT }}:
                            <timer
                                countdown="timeout"
                                max-time-unit="'minute'"
                                interval="1000"
                                finish-callback="timeoutFinish()"
                                autostart="false">
                                {{mminutes}}:{{sseconds}}
                            </timer>
                        </div>
                        <div flex=50 layout="row" layout-align="end top">
                            <md-button class="md-raised" ng-click="state.skip()" ng-show="step == 1">{{ translation.LABELS.NOT_GUEST }}</md-button>
                            <md-button class="md-raised" ng-click="state.nextStep(-1)" ng-hide="step == 0">{{ translation.LABELS.BACK }}</md-button>
                            <md-button
                                class="md-primary md-raised"
                                ng-click="controllersData[step].isValid($event) && state.nextStep(1)"
                                ng-disabled="!controllersData[step].isCompleted()">
                                {{ translation.LABELS.NEXT }}
                            </md-button>
                        </div>
                    </md-content>
                </md-stepper-card-content>
            </md-card>
        </div>
    </md-content>

    <md-content class="" ng-show="map.isMapShown" ng-controller="MapController">
        <md-card id="map-card">
            <md-card-content class="no-layout-padding" style="position: relative;">
                <md-button
                    class="md-icon-button"
                    ng-click="cancel()"
                    style="position: absolute; top: 1em; right: 1em; z-index: 100;">
                    <md-icon>home</md-icon>
                </md-button>
                <div class="map-content" layout="column">
                    <div flex layout="row">
                        <md-button
                            ng-repeat="floor in map.floors"
                            ng-class="{'md-accent' : $index == currentIndex}"
                            ng-click="showFloor($index)">
                                Piso #{{ $index + 1 }}
                            </md-button>
                    </div>
                    <div id="map-container"></div>
                </div>
                <md-button
                    class="md-fab md-primary"
                    style="position: fixed; bottom: 1em; right: 1em;"
                    ng-click="state.hideMap()">
                    <md-icon>check</md-icon>
                </md-button>
            </md-card-content>
        </md-card>
    </md-content>
</md-content>
