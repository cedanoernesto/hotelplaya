<?php $this->load->view('header'); ?>
    <div ng-controller="toolbar" ng-cloak >
        <md-toolbar class="md-hue-2" layout="row" >
            <div class="md-toolbar-tools">
                <?php if (isset($usuario)): ?>
                <md-button class="md-icon-button" aria-label="Settings" ng-click="toggleLeft()">
                    <i class="material-icons">menu</i>
                </md-button>
                <?php  endif; ?>
                <img src="<?= base_url('public/assets/images/logo.png') ?>" class="logo" alt="">
                <h2 flex md-truncate>{{title}}</h2>
                <h2 flex md-truncate>{{module}}</h2>
                <md-button class="md-icon-button" aria-label="Favorite">
                    <md-icon style="color: greenyellow;"></md-icon>
                </md-button>
                <md-button class="md-icon-button" aria-label="More">
                    <md-icon ></md-icon>
                </md-button>
                <md-button ng-click="showLanguageSelector($event)" ng-show="multilanguage">
                    {{ translation.LABELS.CHANGE_LANG }}
                </md-button>
            </div>
        </md-toolbar>
        <section layout="row"  >
            <md-sidenav md-component-id="left"  class="md-sidenav-left md-whiteframe-4dp" >
                <md-toolbar class="md-theme-indigo" layout-padding ng-controller="menuController">
                    <div layout="column" >
                        <img class="user_image" ng-src="<?= base_url() ?>public/assets/images/user.png" alt="user" width="60px">
                        <p class="md-headline info"><?= (isset($usuario['nombre']))?$usuario['nombre']:'' ?></p>
                        <div layout="row" layout-align="space-between center">
                            <p class="md-body-1 info"><?= (isset($usuario['correo']))?$usuario['correo']:'' ?></p>
                            <p></p>
                            <md-menu>
                                <md-button aria-label="Open menu with custom trigger" ng-click="$mdOpenMenu($event)">
                                    <md-icon>expand_more</md-icon>
                                </md-button>
                                <md-menu-content width="3" ng-mouseleave="$mdCloseMenu($event)">
                                    <md-menu-item >
                                        <md-button ng-click="logout()">
                                            <md-icon>exit_to_app</md-icon>
                                            Salir
                                        </md-button>
                                    </md-menu-item>
                            </md-menu>
                        </div>
                    </div>
                </md-toolbar>
                <md-content ng-controller="sectionController" ng-init="expand(<?= (isset($id_section))?$id_section:'0' ?>, <?= (isset($id_module))?$id_module:'0' ?>);">
                    <div layout="column" layout-padding style="max-height: 64px;">
                        <md-input-container>
                            <input type="text" placeholder="Busqueda" ng-model="moduleFilter" flex="100" md-autofocus >
                            <md-icon>search</md-icon>
                        </md-input-container>
                    </div>

                    <md-expansion-panel md-component-id="{{section._id}}" ng-repeat="section in sections" style="margin: 0px;">
                    <md-expansion-panel-collapsed>
                           <div class="md-title">{{section.nombre}}</div>
                           <md-expansion-panel-icon ng-click="$panel.collapse()" ></md-expansion-panel-icon>
                    </md-expansion-panel-collapsed>

                    <md-expansion-panel-expanded>
                        <md-expansion-panel-header ng-click="$panel.collapse()" >
                            <div class="md-title">{{section.nombre}}</div>
                            <md-expansion-panel-icon> </md-expansion-panel-icon>
                        </md-expansion-panel-header>
                        <md-expansion-panel-content flex="100" layout-fill style="padding: 0px!important;">
                            <md-list ng-cloak flex layout-fill style="padding: 0px!important;">
                                <md-list-item ng-repeat="module in section.modules | filter: moduleFilter" style="padding:0px">
                                    <p flex="100" style="padding: 15px;" class="md-subhead module" ng-class="{'current_module': current_module == module._id}" ng-click="current_module != module._id && redirect(module.url)">
                                        <md-icon>{{module.icono}}</md-icon>
                                        <span>{{module.nombre}}</span>
                                    </p>
                                </md-list-item>
                            </md-list>
                        </md-expansion-panel-content>
                        <!-- <md-expansion-panel-footer>
                             <div flex></div>
                             <md-button class="md-warn" ng-click="$panel.collapse()">Collapse</md-button>
                        </md-expansion-panel-footer>-->
                    </md-expansion-panel-expanded>
                    </md-expansion-panel>
                </md-content>
            </md-sidenav>
        </section>
    </div>
