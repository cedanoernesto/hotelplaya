function Map(config) {
    var self = this
    this.isOnHover = false
    this.container = document.querySelector('#' + config.container)

    this.imageResources = config.imageResources
    this.defaultTextScale = 0.3

    this.defaultInfo = {
        size_ratio: config.scale || 0.1,
        rotation: 0,
    }

    if (this.imageResources.length > 0) {
        this.defaultInfo.imageId = this.imageResources[0].id,
        this.defaultInfo.imageUrl = this.imageResources[0].img
    }

    this.map = {}

    var width = this.container.offsetWidth
    this.map.aspect_ratio = 0.6
    var height = width * this.map.aspect_ratio;

    this.filters = {
        originalFilter: config.originalFilter || {},
        hoverFilter: config.hoverFilter || {},
        selectedFilter: config.selectedFilter || {},
        reservedFilter: config.reservedFilter || {},
        availableFilter: config.availableFilter || {}
    }

    this.map.stage = new Konva.Stage({
        container: config.container,
        width: width,
        height: height
    })

    this.map.layers = {}
    this.map.layers['main'] = new Konva.Layer()
    this.map.layers['drag'] = new Konva.Layer()

    this.map.object_map = {}
    this.map.object_map['main'] = []

    this.edit = config.edit || false
    this.select = config.select

    if (this.select === undefined) {
        this.select = true;
    }

     this.multiselect = config.multiselect || false

    if (this.edit) {
        this.map.stage.on('contentClick', function(e) {
            if (self.isOnHover) { return }

            var option = self.imageResources.find(function (resource) {
                return resource.id === self.defaultInfo.imageId
            }) || {}

            self.newObjectInstance({
                idImage: self.defaultInfo.imageId,
                urlImage: self.defaultInfo.imageUrl,
                posX: e.evt.layerX,
                posY: e.evt.layerY,
                layer: 'main',
                tag: option.texto_predeterminado || '',
                size_ratio: self.defaultInfo.size_ratio,
                rotation: self.defaultInfo.rotation,
                filters: self.filters,
                select: true
            })
        })
    }

    this.map.stage.add(this.map.layers['main'])
    this.map.stage.add(this.map.layers['drag'])

    this.selectedObject = undefined


    if (this.multiselect) {
        this.multiselectedObjects = []
        this.multiselectedObjects.applyFilter = function(filter) {
            this.forEach(function(object) {
                if (object.baseinfo[filter]) {
                    object.objMap.image.applyFilter(object.baseinfo[filter])
                }
            })
        }
    }

    this.listeners = {}

}

Map.prototype.newObjectInstance = function(config, callback) {
    var self = this
    var imageObj = new Image()
    imageObj.onload = function() {
        var imgWidth = this.width
        var imgHeight = this.height

        var baseinfo = {
            id: config.id || 0,
            imageId: parseInt(config.idImage || config.imageId),
            imageUrl: config.urlImage,
            layer: config.layer || 'main',
            size_ratio: parseFloat(config.size_ratio),
            aspect_ratio: imgWidth / imgHeight,
            originalFilter: config.filters.originalFilter || {},
            hoverFilter: config.filters.hoverFilter || {},
            selectedFilter: config.filters.selectedFilter || {},
            reservedFilter: config.filters.reservedFilter || {},
            availableFilter: config.filters.availableFilter || {},
            rotation: parseInt(config.rotation || 0),
            selectable: config.selectable !== '0' || false,
            quantity: parseInt(config.quantity || 0),
            tag: config.tag || '',
            piso: config.piso,
            tables: config.tables || '',
            current_filter: config.current_filter || '',
            selected: config.selected || false
        }

        var objHeight = self.container.offsetHeight * baseinfo.size_ratio;
        var objWidth = objHeight * baseinfo.aspect_ratio;

        var posX = config.posX;
        var posY = config.posY;

        if (posX) {
            baseinfo.posX_ratio = parseFloat(posX) / self.container.offsetWidth
        } else {
            baseinfo.posX_ratio = parseFloat(config.posX_ratio)
            posX = baseinfo.posX_ratio * self.container.offsetWidth
        }

        if (posY) {
            baseinfo.posY_ratio = parseFloat(posY) / self.container.offsetHeight
        } else {
            baseinfo.posY_ratio = parseFloat(config.posY_ratio)
            posY = baseinfo.posY_ratio * self.container.offsetHeight
        }

        var width = Math.ceil(objWidth)
        var height = Math.ceil(objHeight)
        var offset = {
            x: Math.ceil(objWidth / 2),
            y: Math.ceil(objHeight / 2)
        }

        var group = new Konva.Group({
            x: posX,
            y: posY,
            width: width,
            height: height,
            offset: offset,
            draggable: self.edit,
            fill: 'red'
       })

        var obj = new Konva.Image({
            image: imageObj,
            width: width,
            height: height,
            dragBoundFunc: function(pos) {
                var stageWidth = self.map.stage.width()
                var stageHeight = self.map.stage.height()

                var currentX = (pos.x < 0) ? 0 : pos.x
                var currentY = (pos.y < 0) ? 0 : pos.y

                //console.log(self.map.layers['main'].getIntersection(pos, 'Image'))

                return {
                    x: (currentX > stageWidth) ? stageWidth : currentX,
                    y: (currentY > stageHeight) ? stageHeight : currentY,
                }
            }
        })

        var simpleText = new Konva.Text({
            x: width / 2,
            y: height / 2,
            text: baseinfo.tag,
            fontSize: height * self.defaultTextScale,
            fontFamily: 'sans-serif',
            fill: 'black',
            align: 'center'
        })

        simpleText.offset({
            x: simpleText.getWidth() / 2,
            y: simpleText.getHeight() / 2
        })

        group.add(obj)
        group.add(simpleText)
        group.baseinfo = baseinfo

        group.objMap = { image: obj, text: simpleText }

        // Methods to update and move the object in the map
        obj.attrs.image.onload = function onImageLoaded() {
            var imgWidth = this.width
            var imgHeight = this.height

            group.baseinfo.aspect_ratio = imgWidth / imgHeight;

            var objHeight = self.container.offsetHeight * group.baseinfo.size_ratio;
            var objWidth = objHeight * group.baseinfo.aspect_ratio;

            var width = Math.ceil(objWidth)
            var height = Math.ceil(objHeight)
            var offset = {
                x: Math.ceil(objWidth / 2),
                y: Math.ceil(objHeight / 2)
            }

            group.width(width)
            group.height(height)

            group.offset(offset)

            group.objMap.image.width(width)
            group.objMap.image.height(height)

            if (group.baseinfo.tag.replace(/ /g,'') === '') {
                var option = self.imageResources.find(function (resource) {
                    return resource.id === group.baseinfo.imageId
                }) || {}

                group.setTag(option.texto_predeterminado || '')
            }

            group.objMap.text.fontSize(objHeight * self.defaultTextScale)
            group.objMap.text.text(group.baseinfo.tag)
            group.objMap.text.position({
                x: objWidth / 2,
                y: objHeight / 2
            })

            group.objMap.text.offset({
                x: group.objMap.text.getWidth() / 2,
                y: group.objMap.text.getHeight() / 2
            })

            group.objMap.image.cache()
            self.map.layers[group.baseinfo.layer].batchDraw()
        }

        group.setImage = function(id, url) {
            this.baseinfo.imageId = id
            this.baseinfo.imageUrl = url
        }

        group.setScale = function(scale){
            this.baseinfo.size_ratio = scale
        }

        group.setTag = function(tag) {
            this.baseinfo.tag = tag
        }

        group.setTable = function(tables) {
            this.baseinfo.tables = tables
        }

        group.setSelectable = function(selectable) {
            this.baseinfo.selectable = selectable
        }

        group.setQuantity = function(quantity) {
            this.baseinfo.quantity = quantity
        }

        group.update = function(){
            this.objMap.image.attrs.image.src = this.baseinfo.imageUrl
        }

        group.changeRotation = function(degrees) {
            this.rotate(degrees)
            group.baseinfo.rotation += degrees
            self.map.layers[this.baseinfo.layer].batchDraw()
        }

        obj.applyFilter = function(filter) {
            for (var key in filter) {
                this[key](filter[key])
            }
            this.cache()
            self.map.layers[group.baseinfo.layer].batchDraw()
        }

        obj.cache()
        obj.filters([Konva.Filters.RGBA]);
        obj.applyFilter(group.baseinfo.originalFilter)

        if (group.baseinfo.current_filter !== '') {
            obj.applyFilter(group.baseinfo[group.baseinfo.current_filter + 'Filter'])
        }

        if (group.baseinfo.selected) {
            obj.applyFilter(group.baseinfo['selectedFilter'])
        }

        // Events for the object
        if (self.select && baseinfo.selectable || self.edit) {
            group.on('mouseover', function(){
                self.isOnHover = true
                document.body.style.cursor = 'pointer'

                if (!group.baseinfo.selectable && !self.edit) return;
                if (group.baseinfo.selected) return;

                this.objMap.image.applyFilter(this.baseinfo.hoverFilter)

                if (self.multiselect) {
                    self.multiselectedObjects.applyFilter('selectedFilter')
                } else if (self.selectedObject) {
                    self.selectedObject.objMap.image.applyFilter(self.selectedObject.baseinfo.selectedFilter)
                }
            })

            group.on('mouseleave', function(){
                self.isOnHover = false
                document.body.style.cursor = 'default'

                if (!group.baseinfo.selectable && !self.edit) return;
                if (group.baseinfo.selected) return;

                if (group.baseinfo.current_filter !== '') {
                    obj.applyFilter(group.baseinfo[group.baseinfo.current_filter + 'Filter'])
                } else {
                    this.objMap.image.applyFilter(this.baseinfo.originalFilter)
                }
                if (self.multiselect) {
                    self.multiselectedObjects.applyFilter('selectedFilter')
                } else if (self.selectedObject) {
                    self.selectedObject.objMap.image.applyFilter(self.selectedObject.baseinfo.selectedFilter)
                }
            })

            group.on('click', function(evt){
                if (!group.baseinfo.selectable && !self.edit) return;

                if (this.baseinfo.selected && !self.edit) {
                    self.unselectObject(this)
                } else {
                    self.selectObject(this)
                }
            })
        }

        group.on('dragstart', function() {
            self.selectObject(this)

            this.moveTo(self.map.layers['drag'])
            self.map.layers['main'].draw()
            self.map.layers['drag'].draw()
        })

        group.on('dragend', function() {
            this.baseinfo.posX_ratio = this.attrs.x / self.container.offsetWidth
            this.baseinfo.posY_ratio = this.attrs.y / self.container.offsetHeight

            this.moveTo(self.map.layers['main'])
            self.map.layers['drag'].draw()
            self.map.layers['main'].draw()
        })

        self.map.object_map[group.baseinfo.layer].push(group)

        if (config.select) {
            self.selectObject(group)
        }

        self.map.layers[group.baseinfo.layer].add(group)
        self.map.layers[group.baseinfo.layer].batchDraw()

        if (typeof callback !== 'undefined') {
            callback(group)
        }
    }

    imageObj.src = config.urlImage || config.imageUrl
}

Map.prototype.updateObjectMap = function (layer = 'main') {
    var self = this

    var width = this.container.offsetWidth
    var height = width * this.map.aspect_ratio

    this.map.stage.width(width)
    this.map.stage.height(height)
    this.map.stage.draw()

    this.map.object_map[layer].forEach(function(group){
        var newHeight = height * group.baseinfo.size_ratio
        var newWidth = newHeight * group.baseinfo.aspect_ratio

        var objWidth = Math.ceil(newWidth)
        var objHeight = Math.ceil(newHeight)

        group.width(objWidth)
        group.height(objHeight)

        group.position({
            x: group.baseinfo.posX_ratio * width,
            y: group.baseinfo.posY_ratio * height
        })

        group.offset({
            x: Math.ceil(newWidth / 2),
            y: Math.ceil(newHeight / 2)
        })

        group.objMap.image.width(objWidth)
        group.objMap.image.height(objHeight)

        group.objMap.image.cache()

        group.objMap.text.fontSize(newHeight * self.defaultTextScale)
        group.objMap.text.text(group.baseinfo.tag)
        group.objMap.text.position({
            x: newWidth / 2,
            y: newHeight / 2
        })

        group.objMap.text.offset({
            x: group.objMap.text.getWidth() / 2,
            y: group.objMap.text.getHeight() / 2
        })
    })
    this.map.layers[layer].batchDraw()

    this.map.object_map[layer].forEach(function(group) {
        group.rotation(parseInt(group.baseinfo.rotation));
    })

    this.map.layers[layer].batchDraw()
}

Map.prototype.selectObject = function(obj) {
    obj.baseinfo.selected = true
    obj.objMap.image.applyFilter(obj.baseinfo.selectedFilter)

    if (this.multiselect) {
        this.multiselectedObjects.push(obj)
        this.exec('itemSelected', [obj, this.multiselectedObjects])
        return
    }

    if (this.selectedObject && this.selectedObject._id !== obj._id) {
        this.selectedObject.baseinfo.selected = false
        this.selectedObject.objMap.image.applyFilter(this.selectedObject.baseinfo.originalFilter)
    }

    this.selectedObject = obj
    this.exec('itemSelected', [obj])
}

Map.prototype.unselectObject = function(obj) {

    obj.baseinfo.selected = false
    if (obj.baseinfo.current_filter !== '') {
        obj.objMap.image.applyFilter(obj.baseinfo[obj.baseinfo.current_filter + 'Filter'])
    } else {
        obj.objMap.image.applyFilter(obj.baseinfo.originalFilter)
    }

    if (this.multiselect) {
        this.multiselectedObjects = this.multiselectedObjects.filter(function (object) {
            return object._id !== obj._id
        })

        this.multiselectedObjects.applyFilter = function(filter) {
            this.forEach(function(object) {
                if (object.baseinfo[filter]) {
                    object.objMap.image.applyFilter(object.baseinfo[filter])
                }
            })
        }

        this.exec('itemUnselected', [obj, this.multiselectedObjects])
        return
    }

    if (this.selectedObject) {
        if (this.selectedObject.baseinfo.current_filter !== '') {
            this.selectedObject.objMap.image.applyFilter(this.selectedObjectbaseinfo[this.selectedObject.baseinfo.current_filter + 'Filter'])
        } else {
            this.selectedObject.objMap.image.applyFilter(this.selectedObject.baseinfo.originalFilter)
        }
        this.selectedObject = undefined
    }

    this.exec('itemUnselected', [obj])
}

Map.prototype.on = function(event, callback) {
    this.listeners[event] = callback
}

Map.prototype.exec = function(event, params) {
    if (typeof this.listeners[event] !== 'undefined') {
        this.listeners[event].apply(null, params)
    }
}

Map.prototype.renderMapFromInfo = function(mapInfo) {
    var self = this
    mapInfo.forEach(function(info) {
        info.filters = self.filters
        info.urlImage = self.imageResources.find(function(image) {
            return parseInt(info.imageId) === image.id
        }).img

        info.select = false

        self.newObjectInstance(info)
    })
}

Map.prototype.clearMap = function () {
    this.map.object_map['main'].forEach(function(obj) {
        obj.destroy();
    })

    this.map.object_map['main'] = []
    this.multiselectedObjects = []
    this.multiselectedObjects.applyFilter = function(filter) {
        this.forEach(function(object) {
            if (object.baseinfo[filter]) {
                object.objMap.image.applyFilter(object.baseinfo[filter])
            }
        })
    }
    this.map.layers['main'].batchDraw()
};

Map.prototype.setMultiselectedObjects = function (objs) {
    this.multiselectedObjects = this.map.object_map['main'].filter(function (obj) {
        return objs.find(function (object) {
            return object.baseinfo.id === obj.baseinfo.id
        })
    })
    this.multiselectedObjects.applyFilter = function(filter) {
        this.forEach(function(object) {
            if (object.baseinfo[filter]) {
                object.objMap.image.applyFilter(object.baseinfo[filter])
            }
        })
    }
}

Map.prototype.getMapInfo = function(layer = 'main', filters, selecteds) {
    var exportKeys = [
        'id',
        'aspect_ratio',
        'imageId',
        'imageUrl',
        'posX_ratio',
        'posY_ratio',
        'quantity',
        'rotation',
        'selectable',
        'size_ratio',
        'tag',
        'tables',
        'piso'
    ]

    if (filters) {
        exportKeys.push('current_filter')
    }

    if (selecteds) {
        exportKeys.push('selected')
    }

    return this.map.object_map[layer].map(function (object) {
        var info = {}
        exportKeys.forEach(function(key) {
            info[key] = object.baseinfo[key]
            if (key === 'selectable') {
                info[key] = info[key] ? '1' : '0'
            }
        })

        return info
    })
}

Map.prototype.removeItem = function(obj) {
    this.map.object_map['main'] = this.map.object_map['main'].filter(function(image) {
        return obj._id !== image._id
    })
    obj.destroy()
    this.map.layers['main'].batchDraw()
}
