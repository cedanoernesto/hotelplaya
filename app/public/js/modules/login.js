(function () {
   angular.module('App')
       .service('$getRecords',function ($resource, $data) {
           return {
               records : $resource($data.base_url+'login/login')
           }
       })
       .controller('login',function ($scope, $getRecords, $mdDialog,$data) {
           $scope.showLoader=false
           function success(response) {
               $scope.showLoader=false
                var message = response.message;
               $mdDialog.show(
                   $mdDialog.alert()
                       .title('Mensaje')
                       .textContent(message)
                       .ok('Aceptar')
               ).then(function () {
                   if(response.status){
                      document.location = $data.base_url + 'dashboard'
                   }
               })
           }
            $scope.login = function () {
                $scope.formLogin.$setSubmitted()
                if ($scope.formLogin.$valid){
                    $scope.showLoader=true
                    $getRecords.records.get($scope.record,success)
                }
                else {
                    $scope.showLoader=false
                }
            }
       })
})()