(function () {
    var app = angular.module('App')
    app.requires.push('md.data.table','ngMaterialDatePicker');
    app
        .service('$restaurants', function ($resource, $data) {
            'use strict';
            return {
                records: $resource($data.base_url + 'administracion/restaurantes/datatable')
            }
        })
        .service('$rows',function () {
            return {
                data:{},
                set:function (data) {
                    this.data = data
                }
            }
        })
        .directive('hcChart', function () {
            return {
                restrict: 'E',
                template: '<div></div>',
                scope: {
                    options: '='
                },
                link: function (scope, element) {
                    Highcharts.chart(element[0], scope.options);
                }
            };
        })
        .controller('module',function ($scope, $restaurants, $http, $data, $mdDialog, $rows ) {
            $scope.records = []
            $scope.restaurants = []
            $scope.events = []
            $scope.query = {
                filters: [],
                order: '',
                limit: 5,
                page: 1
            };
            $scope.options = {
                rowSelection: false,
                multiSelect: false,
                autoSelect: true,
                decapitate: false,
                largeEditDialog: false,
                boundaryLinks: false,
                limitSelect: true,
                pageSelect: true
            };
            $scope.radios = [
                {value:1, label:'Hora'},
                {value:2, label:'Dia'},
                {value:3, label:'Semana'},
                {value:4, label:'Mes'},
            ]
            $scope.search = {
                group:2
            }
            function successEvents(response) {
                console.log(response)
                $scope.events = response.data
            }
            function successDatatable(response) {
                $scope.records = response.data
                $rows.set($scope.records);
            }
            function searchEvent() {
                if (arguments[0] != arguments[1]){
                    $http.get($data.base_url + 'reportes/restaurantes/eventos',{
                        params: $scope.search
                    })
                        .then(successEvents)
                }
            }
            function datatable() {
               $scope.promise =  $http.get($data.base_url + 'reportes/restaurantes/datatable',{
                    params: $scope.search
                })
                    .then(successDatatable)
            }
            function successRestaurants(response) {
                $scope.restaurants = response.data
            }
            $scope.showDialog = function () {
                $mdDialog.show({
                    clickOutsideToClose:true,
                    controller:'chart',
                    controllerAs:'ch',
                    focusOnOpen:true,
                    templateUrl:$data.base_url+ '/public/templates/dialogs/reportes/restaurantes.html'
                }).then(function () {
                })
            }
            $scope.$watch('search.id_restaurante',searchEvent)
            $scope.$watch('search.fecha_inicio',function (newValue,oldValue) {
                // $scope.search.fecha_inicio.setDate($scope.search.fecha_inicio.getDate()-1)
                searchEvent(newValue, oldValue)

            })
            $scope.$watch('search.fecha_fin',function (newValue, oldValue) {
                //$scope.search.fecha_fin.setDate($scope.search.fecha_fin.getDate()-1)

                searchEvent(newValue, oldValue)

            })

            $scope.$watch('search.id_evento',datatable)
            $scope.$watch('search.group',datatable)
            $restaurants.records.get({},successRestaurants)

        })
        .controller('chart',function ($scope,$mdDialog,$rows) {
            $scope.cancel = $mdDialog.hide
            var categories = []
            var categoriesCantidad = []
            var series = [
                {
                    name:'Total',
                    data: []
                }
            ]
            var seriesCantidad = [
                {
                    name:'Cant de adultos',
                    data:[]
                },
                {
                    name:'Cant de menores',
                    data:[]
                }
            ]

            $rows.data.data.forEach(function (item) {
                categories.push(item.fecha)
                categoriesCantidad.push(item.fecha)
                series[0].data.push(parseFloat(item.total_pago))
                seriesCantidad[0].data.push(parseInt(item.cant_adultos))
                seriesCantidad[1].data.push(parseInt(item.cant_menores))
            })

            console.log(series[0].data)
            $scope.chartOptionsConsumo = {
                title: {
                    text: 'Gráfica de consumo'
                },
                xAxis: {
                    categories: categories
                },

                series: series
            };
            $scope.chartOptionsCantidad = {
                title: {
                    text: 'Gráfica de adultos y menores'
                },
                xAxis: {
                    categories: categoriesCantidad
                },

                series: seriesCantidad
            };
        })
})()