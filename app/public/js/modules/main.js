(function () {
    angular.module('App',['ngMaterial','ngResource','ngMessages','material.components.expansionPanels'])
        .config(['$qProvider', '$mdThemingProvider', function ($qProvider, $mdThemingProvider) {
            $qProvider.errorOnUnhandledRejections(false);
            $mdThemingProvider.theme('default')
                .primaryPalette('blue', {
                    'default': '600',
                    'hue-1': '900',
                    'hue-2': '900'
                })
        }])
        .service('$data',function () {
            return {
                base_url : 'http://54.213.18.163/'
            }
        })
        .service('$translationService', function($data, $resource) {
            this.getTranslation = function($scope, language) {
                var languageFilePath = $data.base_url + 'public/translations/' + language + '.json';
                $resource(languageFilePath).get(function (data) {
                    this.lang = language
                    $scope.translation = data
                }, function(err) {
                    var languageFilePath = $data.base_url + 'public/translations/es.json';
                    $resource(languageFilePath).get(function (data) {
                        this.lang = 'es'
                        $scope.translation = data
                    })
                });
            };
        })
        .service('$getSections',function ($resource,$data) {
            'use strict';
            return {
                records: $resource($data.base_url+ 'login/sections')
            }
        })
        .service('$reservationConfig', function($http, $data) {
            var config = {
                data: {},
                setConfig: function(config) {
                    this.data = config
                },
                set: function(key, value) {
                    this.data[key] = value;
                },
                get: function(key, callback) {
                    return this.data[key]
                },
                reload: function() {
                    var data = getConfig()
                    for (var key in data) {
                        config.data[key] = parseInt(data[key])
                    }
                }
            }

            function getConfig() {
                var request = new XMLHttpRequest();
                request.open('GET', $data.base_url + 'kiosko/reservacion/config', false)
                request.send(null)

                if (request.status === 200) {
                    return JSON.parse(request.responseText)
                }
            }

            config.reload()

            return config
        })
        .service('$dateUtils', function($reservationConfig) {
            return {
                getTimeRangeByArray: function(date, hours){
                    var self = this
                    return hours.map(function(hour, i) {
                        hour = self.parseTime(date, hour)
                        return {
                            id: i + 1,
                            value: hour.toLocaleTimeString('en-US', {
                                hour: 'numeric',
                                minute: 'numeric'
                            }),
                            fullvalue: hour.toLocaleTimeString('es-MX')
                        }
                    })
                },
                getTimeRange: function(date, start, end) {
                    var startTime = this.parseTime(date, start)
                    var endTime = this.parseTime(date, end)

                    var now = new Date()
                    var endDay = new Date(date);
                    endDay.setHours(23)
                    endDay.setMinutes(59)
                    endDay.setSeconds(59)

                    var timeArray = []
                    var count = 0

                    if (endTime <= startTime || startTime.valueOf() == endTime.valueOf()) {
                        endTime.setDate(endTime.getDate() + 1)
                        timeArray = timeArray.concat(this.getTimeRange(date, '00:00:00', end))
                        count = timeArray.length
                    }

                    var currentTime = startTime;
                    while (currentTime <= endTime) {
                        if (currentTime >= now && currentTime <= endDay) {
                            timeArray.push({
                                id: ++count,
                                value: currentTime.toLocaleTimeString('en-US', {
                                    hour: 'numeric',
                                    minute: 'numeric'
                                }),
                                fullvalue: currentTime.toLocaleTimeString('es-MX')
                            });
                        }
                        currentTime.setMinutes(currentTime.getMinutes() + $reservationConfig.get('reservation_interval'));
                    }
                    return timeArray;
                },
                parseTime: function(date, timeString) {
                    var components = timeString.split(' ')
                    var time = components.length > 1 ? components[1] : components[0]

                    return new Date(this.convertDateString(date) + ' ' + time)
                },
                convertDateString: function(date) {
                    var dateComponents = date.toLocaleDateString('en-US').split('/');
                    return [dateComponents[2], dateComponents[0], dateComponents[1]].join('-')
                },
                convertDateTimeString: function(date) {
                    return this.convertDateString(date) + ' ' + date.toLocaleTimeString('es-MX')
                }
            }
        })
        .controller('appController', function($rootScope, $translationService) {
            $translationService.getTranslation($rootScope, 'es')
        })
        .controller('toolbar',function ($scope, $mdSidenav, $mdDialog, $data) {
            $scope.title = 'Panel de administración Hotel playa.'
            $scope.toggleLeft = buildToggler('left');
            function buildToggler(componentId) {
                return function() {
                    $mdSidenav(componentId).toggle();
                };
            }

            $scope.showLanguageSelector = function(event) {
                $mdDialog.show({
                    controller:'languageSelectorController',
                    controllerAs:'crtl',
                    focusOnOpen:true,
                    targetEvent: event,
                    templateUrl: $data.base_url+ 'public/templates/dialogs/choose_language.html'
                }).then(function () {

                })
            }
        })
        .controller('languageSelectorController', function($scope, $rootScope, $mdDialog, $translationService) {
            $scope.translation = $rootScope.translation
            $scope.cancel = $mdDialog.hide
            $scope.setLanguage = function(lang) {
                $translationService.getTranslation($rootScope, lang)
                $scope.cancel()
            }
        })
        .controller('sectionController', function ($scope, $mdExpansionPanel,$getSections,$http,$data) {
            $scope.moduleFilter = ''
            function success(response) {
                $scope.sections = response.data
                $scope.sections.forEach(function (item) {
                    item.modules.forEach(function (mitem) {
                        if (mitem._id == $scope.current_module){
                            $scope.current_section = item._id
                            return
                        }
                    })
                })
            }
            $scope.loadSections = function () {
                $http.get($data.base_url+'login/sections').then(success)
            }
            $scope.loadSections()
            $scope.redirect = function (url) {
                document.location = $data.base_url + url;
            }
            $scope.expand = function (id_section, id_module) {
                if (id_module != 0 ){
                    $scope.current_module = id_module
                    $mdExpansionPanel().waitFor(id_section).then(function (instance) {
                        instance.expand();
                    })
                }

            }
            $scope.$watch('moduleFilter',function (newValue, oldValue) {
                if (newValue != oldValue) {
                    $scope.sections.forEach(function (item) {
                        $mdExpansionPanel(item._id).expand();
                    })
                    if (newValue == '') {
                        $scope.sections.forEach(function (item) {
                            if(item._id != $scope.current_section){
                                $mdExpansionPanel(item._id).collapse();
                            }
                        })
                    }
                }
            });
        })
        .controller('menuController',function ($scope, $data, $mdDialog) {
            $scope.logout = function () {
                $mdDialog.show(
                    $mdDialog.confirm()
                        .title('¿Desea salir?')
                        .ok('Si')
                        .cancel('No')
                ).then(function () {
                    document.location = $data.base_url + 'login/logout';
                })
            }
        })

})();
