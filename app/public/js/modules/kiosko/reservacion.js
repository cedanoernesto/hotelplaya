(function() {
    var app = angular.module('App')
    app.requires.push('md-steppers');
    app.requires.push('timer');
    app
        .service('$record', function () {

        })
        .service('$restaurantes', function($resource, $data) {
            return {
                records: $resource($data.base_url + 'index.php/administracion/restaurantes/all')
            }
        })
        .service('$horarios', function($resource, $data) {
            return {
                records: $resource($data.base_url + 'index.php/administracion/restaurantes/horario')
            }
        })
        .service('$event', function($resource, $data) {
            return {
                records: $resource($data.base_url + 'index.php/kiosko/reservacion/restaurantInfo')
            }
        })
        .service('$distributionResources', function ($resource, $data) {
            'use strict';
            return {
                records: $resource($data.base_url + 'kiosko/reservacion/items')
            }
        })
        .service('$mobiliario', function ($resource, $data) {
            'use strict';
            return {
                records: $resource($data.base_url + 'administracion/mobiliario/items')
            }
        })
        .service('$hotelWebService', function($resource, $data) {
            return {
                records: $resource($data.base_url + 'index.php/kiosko/reservacion/fakeWebservice')
            }
        })
        .service('$reservation', function($resource, $data) {
            return {
                records: $resource($data.base_url + 'index.php/kiosko/reservacion/save'),
                savedRecords: $resource($data.base_url + 'index.php/kiosko/reservacion/check')
            }
        })
        .service('$seats', function($resource, $data) {
            return {
                records: $resource($data.base_url + 'index.php/kiosko/reservacion/seat')
            }
        })
        .service('dataStore', function() {
            return {
                data: {},
                controllersData: [],
                state: {}
            }
        })
        .service('$map', function () {
            var map = new Map({
                container: 'map-container',
                imageResources: [],
                edit: false,
                multiselect: true,
                originalFilter: { red: 0, green: 0, blue: 0, alpha: 0 },
                hoverFilter: { red: 0, green: 0, blue: 255, alpha: 0.5 },
                selectedFilter: { red: 0, green: 255, blue: 0, alpha: 0.5 },
                reservedFilter: { red: 255, green: 0, blue: 0, alpha: 0.5 },
                availableFilter: { red: 255, green: 255, blue: 150, alpha: 0.5 }
            })

            var wrapper = function() {
                return map;
            }

            wrapper.isMapShown = false;
            wrapper.show = function() {
                this.isMapShown = true;
            }

            wrapper.hide = function() {
                this.isMapShown = false;
            }

            wrapper.setResources = function(resources) {
                map.imageResources = resources;
                map.defaultInfo.imageId =  resources[0].id;
                map.defaultInfo.imageUrl = resources[0].img;
            }

            wrapper.load = function(objmapinfo, index) {
                this.floors = objmapinfo
                index = index || 0
                map.renderMapFromInfo(objmapinfo[index])
                map.updateObjectMap('main')
                setTimeout(function() {
                    map.updateObjectMap('main')
                }, 500)
            }

            wrapper.clear = function() {
                map.clearMap()
            }


            return wrapper;
        })

    app.controller('KioskoController', function($scope, $http, $restaurantes, dataStore, $map, $timeout, $reservationConfig, $mdDialog, $translationService, $rootScope, $dateUtils) {
        $rootScope.multilanguage = true
        $scope.selectedIndex = 0
        $scope.data = dataStore.data
        $scope.controllersData = dataStore.controllersData
        $scope.state = dataStore.state

        $scope.timeout = $reservationConfig.get('timeout')
        $scope.timeoutFinish = function() {
            $mdDialog.show(
                $mdDialog.alert()
                .clickOutsideToClose(true)
                .title($scope.translation.LABELS.ATENTION)
                .textContent($scope.translation.MESSAGES.TIMEOUT)
                .ariaLabel('Timeout')
                .ok($scope.translation.LABELS.UNDERSTOOD)
            ).then(function () {
                $scope.state.resetProcess()
            })
        }

        $scope.state.resetProcess = function() {
            $reservationConfig.reload()
            $scope.$broadcast('timer-stop');
            $scope.timerRunning = false;
            $scope.$broadcast('timer-set-countdown', $reservationConfig.get('timeout'));

            $map.clear()
            $map.hide()

            $scope.state.freeSelectedSeats()

            $scope.controllersData.forEach(function (data) {
                data.reset()
            })

            $scope.data.reservationdate = new Date()

            $restaurantes.records.get({}, function(response) {
                $scope.restaurants = response.data
                $scope.selectedIndex = 0
                $scope.step = 0
            })
        }

        $scope.cancel = function() {
            $mdDialog.show($mdDialog.confirm()
                .title($scope.translation.LABELS.CANCEL)
                .textContent($scope.translation.MESSAGES.CANCEL_PROCESS)
                .ariaLabel('Lucky day')
                .targetEvent(event)
                .ok($scope.translation.LABELS.YES)
                .cancel($scope.translation.LABELS.CONTINUE)
            ).then(function() {
                $scope.state.resetProcess();
            }, function() {

            });
        }

        $scope.map = $map

        $scope.restaurants = []
        $scope.data.selectedRestaurant = {}

        $restaurantes.records.get({}, function(response) {
            $scope.restaurants = response.data
        })

        $scope.step = 0
        $scope.selectDate = function(restaurant) {
            $scope.data.selectedRestaurant = restaurant
            $scope.selectedIndex = 1
            $scope.$broadcast('timer-start');
            $scope.timerRunning = true;
        }

        $scope.state.nextStep = function(direction) {
            $scope.step += direction
        }
    })

    app.controller('DateController', function($scope, $http, dataStore, $dateUtils, $event, $distributionResources, $map, $mdDialog, $reservationConfig, $horarios) {
        $scope.data = dataStore.data
        $scope.controllersData = dataStore.controllersData
        $scope.state = dataStore.state

        $scope.options = []

        $scope.$watch('data.selectedRestaurant', function(newValue, oldValue) {
            if ($scope.data.selectedRestaurant.hora_apertura && $scope.data.selectedRestaurant.hora_cerrado) {
                $horarios.records.get({
                    id_restaurante: $scope.data.selectedRestaurant.id,
                    fecha: $dateUtils.convertDateString($scope.data.reservationdate),
                    dia: $scope.data.reservationdate.getDay()
                }, function(response) {
                    $scope.options = $dateUtils.getTimeRangeByArray(
                        $scope.data.reservationdate,
                        response.data
                    )
                })
            }
        })

        $scope.$watch('data.reservationdate', function(newValue, oldValue) {
            if ($scope.data.selectedRestaurant.hora_apertura && $scope.data.selectedRestaurant.hora_cerrado) {
                $horarios.records.get({
                    id_restaurante: $scope.data.selectedRestaurant.id,
                    fecha: $dateUtils.convertDateString($scope.data.reservationdate),
                    dia: $scope.data.reservationdate.getDay()
                }, function(response) {
                    $scope.data.reservationtime = null
                    $scope.options = $dateUtils.getTimeRangeByArray(
                        $scope.data.reservationdate,
                        response.data
                    )
                })
            }
        })

        $scope.controllersData[0] = {}

        $scope.controllersData[0].reset = function () {
            $scope.reservation = {}
            $scope.reservation.mindate = new Date()
            $scope.reservation.maxdate = new Date($scope.reservation.mindate)
            $scope.reservation.maxdate.setDate($scope.reservation.maxdate.getDate() + $reservationConfig.get('days_to_reserve'))

            $scope.data.reservationdate = new Date()
            $scope.data.reservationtime = undefined

            $scope.dateForm.$setPristine()
            $scope.dateForm.$setUntouched()
        }

        $scope.controllersData[0].isCompleted = function () {
            return $scope.dateForm.$valid
        }

        $scope.controllersData[0].isValid = function () {

            $event.records.save({
                id_restaurante: $scope.data.selectedRestaurant.id,
                fecha: $dateUtils.convertDateString($scope.data.reservationdate),
                hora: $scope.state.selectedDate().fullvalue,
                dia: $scope.data.reservationdate.getDay()
            }, function(response) {
                if (response.status === 1) {
                    $distributionResources.records.get({
                        id_distribucion: response.data.id_distribucion,
                        fecha: $dateUtils.convertDateString($scope.data.reservationdate),
                        hora: $scope.state.selectedDate().fullvalue,
                        dia: $scope.data.reservationdate.getDay(),
                        asiento_seleccionable: response.data.asiento_seleccionable
                    }, function(distribucion) {
                        $scope.data.distribucion = {
                            id_evento: response.data.id,
                            evento_asiento_seleccionable: response.data.asiento_seleccionable,
                            nombre_evento: response.data.nombre,
                            map: distribucion.data,
                            available: distribucion.available,
                            id_distribucion: response.data.id_distribucion
                        }

                        if (distribucion.available <= 0) {
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .clickOutsideToClose(true)
                                    .title($scope.translation.LABELS.ATENTION)
                                    .textContent($scope.translation.MESSAGES.SOLD_OUT)
                                    .ariaLabel('Alert Dialog Demo')
                                    .ok($scope.translation.LABELS.UNDERSTOOD)
                            )

                            return false;
                        }

                        $map.clear();
                        $map.load($scope.data.distribucion.map);
                        $scope.data.selectedSeats = []
                        $scope.state.nextStep(1)
                    })

                } else {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(true)
                            .title($scope.translation.LABELS.ATENTION)
                            .textContent($scope.translation.MESSAGES.NO_EVENT)
                            .ariaLabel('Alert Dialog Demo')
                            .ok($scope.translation.LABELS.UNDERSTOOD)
                    )
                }
            }, function(error) {

            })

            return false
        }

        $scope.state.selectedDate = function() {
            return $scope.options.find(function(option) {
                return option.id === $scope.data.reservationtime
            })
        }

        $scope.reservation = {}
        $scope.reservation.mindate = new Date()
        $scope.reservation.maxdate = new Date($scope.reservation.mindate)
        $scope.reservation.maxdate.setDate($scope.reservation.maxdate.getDate() + $reservationConfig.get('days_to_reserve'))

        $scope.data.reservationdate = new Date()
    })

    app.controller('RoomController', function($scope, $http, $hotelWebService, dataStore, $mdDialog, $dateUtils) {
        $scope.data = dataStore.data
        $scope.controllersData = dataStore.controllersData
        $scope.state = dataStore.state

        $scope.controllersData[1] = {}

        $scope.controllersData[1].reset = function () {
            $scope.data.customers = []
            $scope.data.room = {}
            $scope.data.no_huesped_nombre = null;

            $scope.roomForm.$setPristine()
            $scope.roomForm.$setUntouched()
        }

        $scope.controllersData[1].isCompleted = function () {
            return $scope.roomForm.$valid
        }

        $scope.controllersData[1].isValid = function () {

            $hotelWebService.records.get({
                habitacion: $scope.data.room.number,
                nombre: $scope.data.room.name,
                fecha: $dateUtils.convertDateString($scope.data.reservationdate),
                hora: $scope.state.selectedDate().fullvalue,
            }, function(response) {
                if (response.status === 0) {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(true)
                            .title($scope.translation.LABELS.ATENTION)
                            .textContent($scope.translation.MESSAGES[response.message_code])
                            .ariaLabel('Alert Dialog Demo')
                            .ok($scope.translation.LABELS.UNDERSTOOD)
                    )
                } else if (response.data && response.data.length > 0) {
                    $scope.data.customers = [].concat(response.data)

                    if ($scope.data.customers[0].todo_incluido) {
                        $scope.state.nextStep(1)
                    } else {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title($scope.translation.LABELS.ATENTION)
                                .textContent($scope.translation.MESSAGES.NO_GUEST_ALERT)
                                .ariaLabel('Alert Dialog Demo')
                                .ok($scope.translation.LABELS.UNDERSTOOD)
                        ).then(function () {
                            $scope.state.nextStep(1)
                        })
                    }
                } else {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(true)
                            .title($scope.translation.LABELS.ATENTION)
                            .textContent($scope.translation.MESSAGES.NO_GUEST_MATCH)
                            .ariaLabel('Alert Dialog Demo')
                            .ok($scope.translation.LABELS.UNDERSTOOD)
                    )
                }
            }, function() {
                $mdDialog.show(
                    $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title($scope.translation.LABELS.ATENTION)
                        .textContent($scope.translation.MESSAGES.NO_GUEST_MATCH)
                        .ariaLabel('Alert Dialog Demo')
                        .ok($scope.translation.LABELS.UNDERSTOOD)
                    )
                })

                return false
            }

            $scope.skip = function(ev) {
                var confirm = $mdDialog.prompt()
                    .title($scope.translation.LABELS.CUSTOMER_NAME)
                    .textContent($scope.translation.MESSAGES.ENTER_NAME_RESERV)
                    .placeholder($scope.translation.LABELS.NAME)
                    .targetEvent(ev)
                    .ok($scope.translation.LABELS.ACEPT)
                    .cancel($scope.translation.LABELS.CANCEL);

                $mdDialog.show(confirm).then(function(result) {
                    if (!!result) {
                        $scope.data.customers = []
                        $scope.data.no_huesped_nombre = result
                        $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title($scope.translation.LABELS.ATENTION)
                                .textContent($scope.translation.MESSAGES.NO_GUEST_ALERT)
                                .ariaLabel('Alert Dialog Demo')
                                .ok($scope.translation.LABELS.UNDERSTOOD)
                        ).then(function () {
                            $scope.state.nextStep(1)
                        })
                    }
                }, function() {

                });
        }

        $scope.state.skip = $scope.skip;
    })

    app.controller('QuantityController', function($scope, $http, $hotelWebService, dataStore, $mdDialog, $data, $dateUtils) {
        $scope.data = dataStore.data
        $scope.controllersData = dataStore.controllersData
        $scope.state = dataStore.state

        $scope.controllersData[2] = {}

        $scope.controllersData[2].reset = function () {
            $scope.data.quantity.adults = 0
            $scope.data.quantity.children = 0
        }

        $scope.controllersData[2].isCompleted = function () {
            return $scope.data.quantity.adults > 0
        }

        $scope.controllersData[2].isValid = function () {
            var isValid = $scope.data.quantity.adults > 0
            var adultos = 0, menores = 0

            if ($scope.data.distribucion.available - ($scope.data.quantity.adults + $scope.data.quantity.children) < 0 ) {
                $mdDialog.show(
                    $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title($scope.translation.LABELS.ATENTION)
                        .textContent($scope.translation.MESSAGES.SOLD_OUT_EXCEED)
                        .ariaLabel('Alert Dialog Demo')
                        .ok($scope.translation.LABELS.UNDERSTOOD)
                )
                return
            }

            if ($scope.data.customers && $scope.data.customers.length > 0) {
                adultos = $scope.data.customers.reduce(function(acc, customer) {
                    return acc + ((customer.todo_incluido) ? parseInt(customer.adultos) : 0)
                }, 0)

                menores = $scope.data.customers.reduce(function(acc, customer) {
                    return acc + ((customer.todo_incluido) ? parseInt(customer.menores) : 0)
                }, 0)
            }

            if (isValid && (adultos > 0 || menores > 0)) {
                var restAdults = $scope.data.quantity.adults - adultos
                var restChildren = $scope.data.quantity.children - menores

                if (isValid && (restAdults > 0 || restChildren > 0 )) {
                    isValid = false

                    $mdDialog.show($mdDialog.confirm()
                        .title($scope.translation.LABELS.LIMIT)
                        .textContent($scope.translation.MESSAGES.SEATS_EXCEEDED)
                        .ariaLabel('Lucky day')
                        .targetEvent(event)
                        .ok($scope.translation.LABELS.ADD_INFO)
                        .cancel($scope.translation.LABELS.CANCEL_ADD_INFO)
                    ).then(function() {
                        $mdDialog.hide()
                        $mdDialog.show({
                            clickOutsideToClose: false,
                            controller: 'addHotelInformation',
                            controllerAs: 'crtl',
                            focusOnOpen: true,
                            targetEvent: event,
                            templateUrl: $data.base_url+ '/public/templates/dialogs/kiosko/agregar_informacion_huesped.html'
                        }).then(function() {

                        })
                    }, function() {

                    });
                } else if ($scope.data.distribucion.evento_asiento_seleccionable === '0') {
                    isValid = false
                    $scope.controllersData[3].isValid();
                }
            }

            return isValid
        }

        $scope.data.quantity = { adults: 0, children: 0 }

        $scope.addQuantityAdults = function(number) {
            if ($scope.data.quantity.adults + number >= 0) {
                $scope.data.quantity.adults += number
            }
        }

        $scope.addQuantityChildren = function(number) {
            if ($scope.data.quantity.children + number >= 0) {
                $scope.data.quantity.children += number
            }
        }
    })

    app.controller('addHotelInformation', function($scope, $mdDialog, $hotelWebService, dataStore, $rootScope, $dateUtils) {
        $scope.translation = $rootScope.translation
        $scope.customers = dataStore.data.customers
        $scope.data = dataStore.data,
        $scope.state = dataStore.state
        $scope.cancel = $mdDialog.hide

        $scope.add = function() {
            $hotelWebService.records.get({
                habitacion: $scope.data.room.number,
                nombre: $scope.data.room.name,
                fecha: $dateUtils.convertDateString($scope.data.reservationdate),
                hora: $scope.state.selectedDate().fullvalue,
            }, function(response) {
                if (response.data.length > 0) {
                    var news = response.data.reduce(function(acc, guest) {
                        var customer = $scope.data.customers.find(function (customer) {
                            return customer.habitacion === guest.habitacion
                        })

                        if (!customer) {
                            acc.push(guest)
                        }

                        return acc
                    }, []);

                    if (news.length > 0) {
                        $scope.data.customers = $scope.data.customers.concat(news)
                        $scope.customers = $scope.data.customers;
                    } else {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title($scope.translation.LABELS.ATENTION)
                                .textContent($scope.translation.MESSAGES.NO_GUEST_MATCH)
                                .ariaLabel('Alert Dialog Demo')
                                .ok($scope.translation.LABELS.UNDERSTOOD)
                        )
                    }
                }
            })
        }

        $scope.remove = function(customer) {
            $scope.customers = $scope.customers.filter(function(savedCustomer) {
                return savedCustomer !== customer
            })
            $scope.data.customers = $scope.customers;
        }
    })

    app.controller('SeatsController', function($scope, $http, dataStore, $reservation, $mdDialog, $data, $map, $dateUtils) {
        $scope.data = dataStore.data
        $scope.controllersData = dataStore.controllersData
        $scope.state = dataStore.state

        $scope.controllersData[3] = {}

        $scope.controllersData[3].reset = function () {
            $scope.data.selectedSeats = []
            $map.hide()
            $map.clear()
        }

        $scope.controllersData[3].isCompleted = function () {
            return $scope.state.getSelectedSeats().length > 0
        }

        $scope.controllersData[3].isValid = function (event) {
            var residuo =  ($scope.data.quantity.adults + $scope.data.quantity.children) - $scope.data.selectedSeats.length
            if (residuo > 0) {
                $mdDialog.show(
                    $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title($scope.translation.LABELS.ATENTION)
                        .textContent($scope.translation.MESSAGES.NOT_ENOUGHT.replace('{{seats}}', residuo))
                        .ariaLabel('Alert Dialog Demo')
                        .ok($scope.translation.LABELS.UNDERSTOOD)
                )

                return false
            }

            var confirm = $mdDialog.confirm()
                .title($scope.translation.LABELS.ATENTION)
                .textContent($scope.translation.MESSAGES.CONFIRM_RESERV)
                .ariaLabel('Lucky day')
                .targetEvent(event)
                .ok($scope.translation.LABELS.MAKE_RESERV)
                .cancel($scope.translation.LABELS.CANCEL);

            $mdDialog.show(confirm).then(function() {
                var nombre = ($scope.data.customers && $scope.data.customers[0]) ? $scope.data.customers[0].nombre : $scope.data.no_huesped_nombre
                $reservation.records.save({
                    id_restaurante: $scope.data.selectedRestaurant.id,
                    id_distribucion: $scope.data.distribucion.id_distribucion,
                    fecha: $dateUtils.convertDateString($scope.data.reservationdate),
                    hora: $scope.state.selectedDate().fullvalue,
                    nombre_persona: nombre,
                    huespedes: $scope.data.customers,
                    asientos: $scope.data.selectedSeats,
                    adultos: $scope.data.quantity.adults,
                    menores: $scope.data.quantity.children,
                    id_evento: $scope.data.distribucion.id_evento
                }, function(response) {

                    if (response.status && response.status === 2) {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title($scope.translation.LABELS.ATENTION)
                                .textContent($scope.translation.MESSAGES[response.message_code] + response.rooms.join(', '))
                                .ariaLabel('Alert Dialog Demo')
                                .ok($scope.translation.LABELS.UNDERSTOOD)
                        )
                        return
                    }

                    if (response.status && response.status !== 0) {
                        $mdDialog.hide()
                        $scope.data.ticket = response.data
                        $scope.$broadcast('timer-stop');
                        $scope.timerRunning = false;
                        $mdDialog.show({
                            controller:'ticket',
                            controllerAs:'crtl',
                            focusOnOpen:true,
                            templateUrl: $data.base_url+ '/public/templates/dialogs/kiosko/ticket.html'
                        }).then(function () {

                        })
                    } else {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title($scope.translation.LABELS.ATENTION)
                                .textContent($scope.translation.MESSAGES.ERROR)
                                .ariaLabel('Alert Dialog Demo')
                                .ok($scope.translation.LABELS.UNDERSTOOD)
                        )
                    }
                }, function () {
                    $mdDialog.hide()
                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(true)
                            .title($scope.translation.LABELS.ATENTION)
                            .textContent($scope.translation.MESSAGES.ERROR)
                            .ariaLabel('Alert Dialog Demo')
                            .ok($scope.translation.LABELS.UNDERSTOOD)
                    )
                })
            }, function() {

            });

            return true
        }


    })

    app.controller('ticket', function($scope, $mdDialog, dataStore, $reservationConfig, $data, $rootScope, $dateUtils) {
        $scope.translation = $rootScope.translation
        $scope.data = dataStore.data
        $scope.state = dataStore.state
        $scope.time = $reservationConfig.get('time_to_pay')

        $scope.cancel = function () {
            $mdDialog.hide()
            $scope.state.resetProcess()
        }

        $scope.barcodeURL = function() {
            return $data.base_url + 'kiosko/reservacion/barcode/' + $scope.data.ticket.codigo
        }

        $scope.imprimir = function() {
            var ticket = document.getElementById('ticket-content')
            var WinPrint = window.open('', '', 'left=0,top=0,width=500,height=400,toolbar=0,scrollbars=0,status=0');
            WinPrint.document.write(ticket.innerHTML);
            WinPrint.document.write('<link href="http://ajax.googleapis.com/ajax/libs/angular_material/1.1.0/angular-material.min.css" rel="stylesheet" type="text/css">')
            WinPrint.document.write('<link href="' + $data.base_url + 'public/css/main.css" rel="stylesheet" type="text/css">')
            WinPrint.document.close();
            WinPrint.focus();
            setTimeout(function() {
                WinPrint.print();
                WinPrint.close();
            }, 500);
        }
    })

    app.controller('MapController', function($scope, $http, dataStore, $mdDialog, $data, $map, $mobiliario, $seats, $dateUtils) {
        $scope.currentIndex = 0;
        $scope.data = dataStore.data
        $scope.controllersData = dataStore.controllersData
        $scope.state = dataStore.state

        $scope.map = $map;
        var selectedSeatsByFloor = []
        $scope.showFloor = function(index) {
            $scope.map.floors[$scope.currentIndex] = $map().getMapInfo('main', true, true)
            $map.clear();
            $map.load($scope.map.floors, index);
            setTimeout(function(){
                $map().updateObjectMap('main')
                $map().setMultiselectedObjects(selectedSeatsByFloor[$scope.currentIndex] || [])
            }, 50)
            $scope.currentIndex = index
        }

        var container = document.querySelector('#map-container')
        $scope.containerWidth = container.offsetWidth

        $mobiliario.records.get({}, function(response) {
            $map.setResources(response.data.map(function (mobiliario) {
                mobiliario.img = $data.base_url + 'public/assets/images/mobiliario/' + mobiliario.img
                mobiliario.id = parseInt(mobiliario.id)
                return mobiliario
            }))
        })

        $scope.data.selectedSeats = []

        $scope.state.showMap = function() {
            $map.show()
            setTimeout(function(){
                $map().updateObjectMap('main')
            }, 500)
        }

        $scope.state.hideMap = function() {
            $scope.selectedIndex = 1
            $map.hide()
        }

        function reduceFloors (array, floor) {
            return array.concat(floor)
        }

        function mapSeat(seat) {
            return {
                id: seat.baseinfo.id,
                tag: seat.baseinfo.tag,
                tables: seat.baseinfo.tables,
                quantity: seat.baseinfo.quantity
            }
        }

        $map().on('itemSelected', function(obj, objs) {
            selectedSeatsByFloor[$scope.currentIndex] = objs
            var total = $scope.data.quantity.adults + $scope.data.quantity.children

            var seatsCapacity = selectedSeatsByFloor.reduce(function (acc, objs) {
                return acc + objs.reduce(function(accumulator, object) {
                    return accumulator + (object.baseinfo.quantity || 0)
                }, 0)
            }, 0)

            if (seatsCapacity > total) {
                $scope.data.selectedSeats = selectedSeatsByFloor
                    .reduce(reduceFloors, [])
                    .map(mapSeat)
                $map().unselectObject(obj)

                $mdDialog.show(
                    $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title($scope.translation.MESSAGES.SEATS_SELECTION_EXCEED_H)
                        .textContent($scope.translation.MESSAGES.SEATS_SELECTION_EXCEED)
                        .ariaLabel('Alert Dialog Demo')
                        .ok($scope.translation.LABELS.UNDERSTOOD)
                )
            } else {
                $scope.data.selectedSeats = selectedSeatsByFloor
                    .reduce(reduceFloors, [])
                    .map(mapSeat)
                $seats.records.save({
                    id: [obj.baseinfo.id],
                    estatus: 1,
                    fecha: $dateUtils.convertDateString($scope.data.reservationdate),
                    hora: $scope.state.selectedDate().fullvalue
                }, function(response) {
                    if (response.status === 0) {
                        obj.baseinfo.selectable = false
                        $map().unselectObject(obj)
                        obj.objMap.image.applyFilter(obj.baseinfo['reservedFilter'])
                        $mdDialog.show(
                            $mdDialog.alert()
                                .title($scope.translation.LABELS.ATENTION)
                                .textContent($scope.translation.MESSAGES.ALREADY_SELECTED)
                                .ariaLabel('Alert Dialog Demo')
                                .ok($scope.translation.LABELS.UNDERSTOOD)
                        )
                    }
                }, function(error) {

                })

            }
        })

        $map().on('itemUnselected', function(obj, objs) {
            selectedSeatsByFloor[$scope.currentIndex] = objs
            $scope.data.selectedSeats = selectedSeatsByFloor
                .reduce(reduceFloors, [])
                .map(mapSeat)

            if (obj.baseinfo.selectable) {
                $seats.records.save({
                    id: [obj.baseinfo.id],
                    estatus: 0,
                    fecha: $dateUtils.convertDateString($scope.data.reservationdate),
                    hora: $scope.state.selectedDate().fullvalue
                }, function(response) {

                }, function(error) {

                })
            }
        })

        $scope.state.getSelectedSeats = function() {
            return $scope.data.selectedSeats
                .map(function (seat) {
                    return seat.tables + '-' + seat.tag
                })
                .sort(function(a, b) {
                    return a - b
                })
                .join(', ')
        }

        $scope.$watch(function(){
            return container.offsetWidth
        }, function(){
            $map().updateObjectMap('main')
        })

        window.onresize = function() {
            $map().updateObjectMap('main')
        }

        $scope.state.freeSelectedSeats = function() {
            if ($scope.state.selectedDate()) {
                var ids = $scope.data.selectedSeats.map(function(seat) {
                    return seat.id
                })
                $seats.records.save({
                    id: ids,
                    estatus: 0,
                    fecha: $dateUtils.convertDateString($scope.data.reservationdate),
                    hora: $scope.state.selectedDate().fullvalue
                }, function(response) {

                }, function(error) {

                })
            }
        }

    })
})()
