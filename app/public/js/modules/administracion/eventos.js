(function () {
    var app = angular.module('App')
    app.requires.push('md.data.table','ngMaterialDatePicker');
    app
        .service('$record',function () {
            return {
                data: {},
                set : function (record) {
                    this.data = record;
                }
            };
        })
        .service('$tiposRecords',function ($resource, $data) {
            'use strict';
            return {
                records: $resource($data.base_url+ 'administracion/eventos/tipos')
            }
        })
        .service('$records',function ($resource, $data) {
            'use strict';
            return {
                records: $resource($data.base_url+ 'administracion/eventos/datatable')
            }
        })
        .service('$saveRecords',function ($resource, $data) {
            'use strict';
            return {
                records: $resource($data.base_url+ 'administracion/eventos/save')
            }
        })
        .controller('module',function($scope,$records,$mdDialog,$record,$saveRecords,$data) {
            $scope.record = []
            $scope.records = []
            $scope.selected = []
            $scope.limitOptions = [5,10,15]
            $scope.filter = {
                options: {
                    debounce: 500
                }
            };
            $scope.options = {
                rowSelection: false,
                multiSelect: false,
                autoSelect: true,
                decapitate: false,
                largeEditDialog: false,
                boundaryLinks: false,
                limitSelect: true,
                pageSelect: true
            };
            $scope.query = {
                filters: [],
                order: '',
                limit: 5,
                page: 1
            };
            function success (response) {
                $scope.records = response
            }
            $scope.getRecords = function () {
                $scope.promise = $records.records.get($scope.query,success).$promise
            }
            $scope.removeFilter = function () {
                $scope.filter.show = false;
                $scope.query.filter = '';
                if($scope.filter.form.$dirty) {
                    $scope.filter.form.$setPristine();
                }
            };
            $scope.$watch('query.filter', function (newValue, oldValue) {
                if(!oldValue) {
                    bookmark = $scope.query.page;
                }
                if(newValue !== oldValue) {
                    $scope.query.page = 1;
                }
                if(!newValue) {
                    $scope.query.page = bookmark;
                }
                $scope.getRecords();
            });
            $scope.showDialog = function (event,record) {
                record.privado = (record.privado == '1')?true:false;
                $record.set(record)
                $mdDialog.show({
                    clickOutsideToClose:true,
                    controller:'saveRecord',
                    controllerAs:'crtl',
                    focusOnOpen:true,
                    templateUrl:$data.base_url+ '/public/templates/dialogs/administracion/eventos.html'
                }).then($scope.getRecords)
            }

            $scope.remove = function($event,id){
                $event.stopPropagation()
                var confirm = $mdDialog.confirm()
                    .title('Eliminar')
                    .textContent('¿Desea eliminar el evento?')
                    .ariaLabel('')
                    .targetEvent($event)
                    .ok('Si')
                    .cancel('Cancelar');
                $mdDialog.show(confirm).then(function () {
                    $saveRecords.records.save({record:{id:id, estatus: 2}},function () {
                        $scope.getRecords()
                    })
                },function () {
                    //console.log('no')
                })
            }
            $scope.propagation = function ($event) {
                $event.stopPropagation()
            }
            /*  $mdDialog.show({
             clickOutsideToClose:true,
             controller:'eventoConfirmacion',
             controllerAs:'crtl',
             focusOnOpen:true,
             templateUrl:$data.base_url+ '/public/templates/dialogs/administracion/eventos_confirmacion.html'
             }).then($scope.getRecords)*/

        })
        .controller('saveRecord',function ($scope,$record, $mdDialog, $saveRecords,$tiposRecords,$data,$http) {
            $scope.record = $record.data
            $scope.response = {
                status: 1,
                message: ''
            }
            $scope.details = []
            $scope.days = []
            $scope.restaurants = []
            $scope.saveRecord = function () {
                $scope.formRecord.$setSubmitted();
                if ($scope.formRecord.$valid){
                    $scope.record = $record.data
                    delete $scope.record['evento']
                        $scope.details.forEach(function (item) {
                            item.hora_inicio = new Date(item.hora_inicio).toLocaleTimeString()
                            item.hora_fin = new Date(item.hora_fin).toLocaleTimeString()
                        })
                        $saveRecords.records.save({record:$scope.record,details:$scope.details},function (response) {
                            $scope.response = response
                            if (response.status){
                                $mdDialog.hide();
                                if ($scope.record.id==0){
                                    $scope.record.id_evento = response.id_md5
                                    $mdDialog.show({
                                        clickOutsideToClose:true,
                                        controller:'eventoConfirmacion',
                                        controllerAs:'crtl',
                                        focusOnOpen:true,
                                        templateUrl:$data.base_url+ '/public/templates/dialogs/administracion/eventos_confirmacion.html'
                                    }).then($scope.getRecords)
                                }
                                else {
                                    $mdDialog.show(
                                        $mdDialog.alert()
                                            .title('Mensaje')
                                            .textContent(response.message)
                                            .ok('Aceptar'))
                                }
                            }
                        })

                }
            }
            $scope.getDetails = function () {
                $http.get($data.base_url + 'administracion/eventos/detalles/'+ $record.data.id).then(successDetails)
            }
            $scope.removeDetail = function ($index) {
                if ($record.data.id != 0) {
                    $http.post($data.base_url + 'administracion/eventos/detalle/',{id:$scope.details[$index].id})
                }
                $scope.details.splice($index,1)
            }
            function success(response) {
                $scope.records = response.data
            }
            function successDays(response) {
                $scope.days = response.data
            }
            function successDetails(response) {
                $scope.details = response.data
            }
            function successRestaurants(response) {
                $scope.restaurants = response.data.data
            }
            $scope.getTypes = function () {
                return  $tiposRecords.records.get({},success).$promise
            }
            $scope.getDays = function () {
                $http.get($data.base_url + 'administracion/eventos/dias').then(successDays)
            }
            $scope.getRestaurants = function () {
                $http.get($data.base_url + 'administracion/restaurantes/datatable').then(successRestaurants)
            }
            $scope.getTypes()
            $scope.getDays()
            $scope.getDetails()
            $scope.getRestaurants()

            $scope.cancel = $mdDialog.hide;
        }).controller('eventoConfirmacion',function ($scope,$mdDialog,$http,$data,$record) {
        $scope.restaurants = []
        $scope.select = 0
        function successRestaurants(response) {
            $scope.restaurants = response.data.data
        }
        $scope.getRestaurants = function () {
            $http.get($data.base_url + 'administracion/restaurantes/datatable').then(successRestaurants)
        }
        $scope.saveDistribucion = function () {
            $scope.formSelectDistribution.$setSubmitted();
            if ($scope.formSelectDistribution.$valid){
                $http.post($data.base_url + 'administracion/eventos/saveDistribucion',{
                    id_restaurante:$scope.record.id_restaurante,
                    nombre:$record.data.nombre,
                    id_evento: $record.data.id_evento
                }).then(function (response) {
                    if (response.status){
                        $mdDialog.show(
                            $mdDialog.alert()
                                .title('Mensaje')
                                .textContent('Datos guardados correctamente')
                                .ok('Aceptar'))
                    }

                })
            }
        }
        $scope.getRestaurants()
        $scope.cancel = $mdDialog.hide;
    })
})();
