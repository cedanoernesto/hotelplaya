(function () {
    angular.module('App')
        .service('$reservacion', function($resource, $data) {
            return {
                get: $resource($data.base_url + 'administracion/pago_reservacion/reservacion').get,
                save: $resource($data.base_url + 'administracion/pago_reservacion/save').save,
            }
        })
        .service('$methods', function($resource, $data) {
            return {
                get: $resource($data.base_url + 'administracion/pago_reservacion/metodos').get
            }
        })
        .controller('pay', function ($scope, $reservacion, $mdDialog, $methods) {

            $methods.get({}, function(response) {
                $scope.methods = response.data
                $scope.id_metodo_pago = $scope.methods[0].id
            }, function(err) {
            })

            $scope.getInfo = function() {

                if ($scope.reservation_code !== '') {
                    $reservacion.get({
                        id_reservacion: $scope.reservation_code,
                        id_tipo_pago: $scope.id_metodo_pago
                    }, function(response) {
                        if (response.status === 0) {
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .title('Alerta')
                                    .textContent('No existe reservacion con ese código')
                                    .ok('Aceptar')
                            )
                            return;
                        }

                        if (response.estatus_pago === '0') {
                            $scope.reservation = response
                            $scope.reservation.fecha = new Date($scope.reservation.fecha).toLocaleDateString('es-MX', {
                                weekday: 'long',
                                day: 'numeric',
                                month: 'long',
                                year: 'numeric'
                            })
                        } else if (response.estatus_pago === '1'){
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .title('Alerta')
                                    .textContent('Esta reservación ya ha sido pagada')
                                    .ok('Aceptar')
                            )
                        }
                    }, function (response) {

                    })
                }
            }

            $scope.pay = function () {
                $reservacion.save({ id: $scope.reservation.id, estatus_pago: 1}, function(response) {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .title('Alerta')
                            .textContent('Reservacion pagada correctamente')
                            .ok('Aceptar')
                    ).then(function() {
                        $scope.reservation = null
                        $scope.reservation_code = ''
                        $scope.payForm.$setPristine()
                        $scope.payForm.$setUntouched()
                    })
                })
            }

        })
})()
