(function () {
    var app = angular.module('App')
    app.requires.push('md.data.table','ngMaterialDatePicker');
    app
        .service('$record',function () {
            return {
                data: {},
                set : function (record) {
                    this.data = record;
                },
                setRestaurant: function (restaurant) {
                    this.restaurant = restaurant
                }
            };
        })
        .service('$records',function ($resource, $data) {
            'use strict';
            return {
                records: $resource($data.base_url+ 'administracion/reservaciones/datatable')
            }
        })
        .service('$restaurants', function ($resource, $data) {
            'use strict';
            return {
                records: $resource($data.base_url + 'administracion/restaurantes/datatable')
            }
        })
        .controller('reservaciones',function($scope,$records,$mdDialog,$record,$data, $restaurants) {
            $scope.fechas_filtro = 0;
            $scope.record = [];
            $scope.records = []
            $scope.selected = [];
            $scope.limitOptions = [5,10,15]
            $scope.filter = {
                options: {
                    debounce: 500
                }
            };
            $scope.options = {
                rowSelection: false,
                multiSelect: false,
                autoSelect: true,
                decapitate: false,
                largeEditDialog: false,
                boundaryLinks: false,
                limitSelect: true,
                pageSelect: true
            };
            $scope.query = {
                filters: [],
                order: '',
                limit: 5,
                page: 1
            };
            function success (response) {
                $scope.records = response
            }
            $scope.getRecords = function () {
                $scope.query.restaurant = $record.restaurant
                $scope.query.fecha_inicio = $record.fecha_inicio
                $scope.query.fecha_fin = $record.fecha_fin
                $scope.query.fechas_filtro = $scope.fechas_filtro
                $scope.promise = $records.records.get($scope.query,success).$promise
            }
            $scope.removeFilter = function () {
                $scope.filter.show = false;
                $scope.query.filter = '';
                if($scope.filter.form.$dirty) {
                    $scope.filter.form.$setPristine();
                }
            };
            $scope.$watch('query.filter', function (newValue, oldValue) {
                if(!oldValue) {
                    bookmark = $scope.query.page;
                }
                if(newValue !== oldValue) {
                    $scope.query.page = 1;
                }
                if(!newValue) {
                    $scope.query.page = bookmark;
                }
                $scope.getRecords();
            });
            $scope.showDialog = function (event,record) {
                $record.set(record)
                $mdDialog.show({
                    clickOutsideToClose:true,
                    controller:'detail',
                    controllerAs:'crtl',
                    focusOnOpen:true,
                    templateUrl:$data.base_url+ '/public/templates/dialogs/administracion/reservaciones.html'
                }).then($scope.getRecords)
            }

            $scope.propagation = function ($event) {
                $event.stopPropagation()
            }

            $restaurants.records.get({}, function(response) {
                $scope.restaurants = response.data;
            })

            $scope.$watch('restaurant_select', function (newValue, oldValue) {
                if(newValue !== oldValue) {
                    $record.setRestaurant(newValue)
                    $scope.getRecords()
                }
            });

            $scope.$watch('fecha_inicio', function (newValue, oldValue) {
                if(newValue !== oldValue) {
                    $record.fecha_inicio = newValue.toISOString().split('T')[0]
                    $scope.getRecords()
                }
            });

            $scope.$watch('fecha_fin', function (newValue, oldValue) {
                if(newValue !== oldValue) {
                    $record.fecha_fin = newValue.toISOString().split('T')[0]
                    $scope.getRecords()
                }
            });

            $scope.$watch('fechas_filtro', function(newValue, oldValue) {
                if(newValue !== oldValue) {
                    $scope.getRecords()
                }
            });

        })
        .controller('detail', function($scope, $record, $mdDialog) {
            $scope.cancel = $mdDialog.hide
            $scope.record = $record.data
        })
})();
