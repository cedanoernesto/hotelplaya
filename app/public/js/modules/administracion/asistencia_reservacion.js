(function () {
    angular.module('App')
        .service('$reservacion', function($resource, $data) {
            return {
                get: $resource($data.base_url + 'administracion/asistencia_reservacion/reservacion').get,
                save: $resource($data.base_url + 'administracion/asistencia_reservacion/save').save,
            }
        })
        .service('$mobiliario', function ($resource, $data) {
            'use strict';
            return {
                records: $resource($data.base_url + 'administracion/mobiliario/items')
            }
        })
        .service('$map', function() {
            var map = new Map({
                container: 'map-container',
                imageResources: [],
                edit: false,
                select: false,
                originalFilter: { red: 0, green: 0, blue: 0, alpha: 0 },
                hoverFilter: { red: 0, green: 0, blue: 255, alpha: 0.5 },
                selectedFilter: { red: 0, green: 255, blue: 0, alpha: 0.5 },
                reservedFilter: { red: 255, green: 0, blue: 0, alpha: 0.5 },
                availableFilter: { red: 255, green: 255, blue: 150, alpha: 0.5 }
            })

            var wrapper = function() {
                return map;
            }

            wrapper.isMapShown = false;
            wrapper.show = function() {
                this.isMapShown = true;
            }

            wrapper.hide = function() {
                this.isMapShown = false;
            }

            wrapper.setResources = function(resources) {
                map.imageResources = resources;
                map.defaultInfo.imageId =  resources[0].id;
                map.defaultInfo.imageUrl = resources[0].img;
            }

            wrapper.load = function(objmapinfo, index) {
                this.floors = objmapinfo
                this.index = index || 0
                map.renderMapFromInfo(objmapinfo[this.index])
                setTimeout(function() {
                    map.updateObjectMap('main')
                }, 500)
            }

            wrapper.clear = function() {
                map.clearMap()
            }


            return wrapper;
        })
        .controller('check', function ($scope, $reservacion, $mdDialog, $map, $dateUtils) {
            $scope.getInfo = function() {

                if ($scope.reservation_code !== '') {
                    $reservacion.get({ id_reservacion: $scope.reservation_code, fecha: $dateUtils.convertDateTimeString(new Date())}, function(response) {
                        $scope.reservation = null
                        $map.clear()
                        if (response.status === 0) {
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .title('Alerta')
                                    .textContent('No existe reservacion con ese código')
                                    .ok('Aceptar')
                            )
                            return;
                        }

                        if (response.estatus_pago === '1') {
                            $scope.reservation = response
                            $scope.reservation.fecha = new Date($scope.reservation.fecha).toLocaleDateString('es-MX', {
                                weekday: 'long',
                                day: 'numeric',
                                month: 'long',
                                year: 'numeric'
                            })

                            $map.clear()
                            $map.load($scope.reservation.distribucion)
                            $reservacion.save({ id: response.id, asistencia: 1 }, function(response) {

                            })


                        } else if (response.estatus_pago === '0'){
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .title('Alerta')
                                    .textContent('Esta reservación no ha sido pagada')
                                    .ok('Aceptar')
                            )
                        }
                    }, function (response) {

                    })
                }
            }
        })
        .controller('map', function($scope, $map, $data, $mobiliario) {
            $scope.map = $map;
            $scope.map.index = 0

            $scope.showFloor = function(index) {
                $scope.map.floors[$map.index] = $map().getMapInfo('main', true, true)
                $map.clear();
                $map.load($scope.map.floors, index);
                $scope.currentIndex = index
            }

            var container = document.querySelector('#map-container')
            $scope.containerWidth = container.offsetWidth

            $mobiliario.records.get({}, function(response) {
                $map.setResources(response.data.map(function (mobiliario) {
                    mobiliario.img = $data.base_url + 'public/assets/images/mobiliario/' + mobiliario.img
                    mobiliario.id = parseInt(mobiliario.id)
                    return mobiliario
                }))
            })

            $scope.$watch(function(){
                return container.offsetWidth
            }, function(){
                $map().updateObjectMap('main')
            })

            window.onresize = function() {
                $map().updateObjectMap('main')
            }
        })
})()
