(function () {
    var app = angular.module('App')
    app.requires.push('md.data.table','ngMaterialDatePicker');
    app
        .service('$record',function () {
            return {
                data: {},
                set : function (record) {
                    this.data = record;
                }
            };
        })
        .service('$records',function ($resource, $data) {
            'use strict';
            return {
                records: $resource($data.base_url+ 'administracion/restaurantes/datatable')
            }
        })
        .service('$saveRecords',function ($resource, $data) {
            'use strict';
            return {
                records: $resource($data.base_url+ 'administracion/restaurantes/save')
            }
        })
        .controller('restaurantes',function($scope,$records,$mdDialog,$record,$saveRecords,$data) {
            $scope.record = [];
            $scope.records = []
            $scope.selected = [];
            $scope.limitOptions = [5,10,15]
            $scope.filter = {
                options: {
                    debounce: 500
                }
            };
            $scope.options = {
                rowSelection: false,
                multiSelect: false,
                autoSelect: true,
                decapitate: false,
                largeEditDialog: false,
                boundaryLinks: false,
                limitSelect: true,
                pageSelect: true
            };
            $scope.query = {
                filters: [],
                order: '',
                limit: 5,
                page: 1
            };
            function success (response) {
                $scope.records = response
            }
            $scope.getRecords = function () {
                $scope.promise = $records.records.get($scope.query,success).$promise
            }
            $scope.removeFilter = function () {
                $scope.filter.show = false;
                $scope.query.filter = '';
                if($scope.filter.form.$dirty) {
                    $scope.filter.form.$setPristine();
                }
            };
            $scope.$watch('query.filter', function (newValue, oldValue) {
                if(!oldValue) {
                    bookmark = $scope.query.page;
                }
                if(newValue !== oldValue) {
                    $scope.query.page = 1;
                }
                if(!newValue) {
                    $scope.query.page = bookmark;
                }
                $scope.getRecords();
            });
            $scope.showDialog = function (event,record) {
                $record.set(record)
                $mdDialog.show({
                    clickOutsideToClose:true,
                    controller:'saveRecord',
                    controllerAs:'crtl',
                    focusOnOpen:true,
                    templateUrl:$data.base_url+ '/public/templates/dialogs/administracion/restaurantes.html'
                }).then($scope.getRecords)
            }

            $scope.remove = function($event,id){
                $event.stopPropagation()
                var confirm = $mdDialog.confirm()
                    .title('Eliminar')
                    .textContent('¿Desea eliminar el restaurante?')
                    .ariaLabel('')
                    .targetEvent($event)
                    .ok('Si')
                    .cancel('Cancelar');
                $mdDialog.show(confirm).then(function () {
                    $saveRecords.records.save({id:id, estatus: 2},function () {
                        $scope.getRecords()
                    })
                },function () {
                    //console.log('no')
                })
            }
            $scope.propagation = function ($event) {
                $event.stopPropagation()
            }

        })
        .controller('saveRecord',function ($scope,$record, $mdDialog, $saveRecords) {
            $scope.record = $record.data
            $scope.response = {
                status: 1,
                message: ''
            }
            $scope.saveRecord = function () {
                $scope.formRecord.$setSubmitted();
                if ($scope.formRecord.$valid){
                    $scope.record.hora_apertura = new Date($scope.record.hora_apertura ).toLocaleTimeString();
                    $scope.record.hora_cerrado = new Date($scope.record.hora_cerrado ).toLocaleTimeString();
                    if ($scope.response.status){
                        $saveRecords.records.save($scope.record,function (response) {
                            $scope.response = response
                            if (response.status){
                                $mdDialog.hide();
                                $mdDialog.show(
                                    $mdDialog.alert()
                                        .title('Mensaje')
                                        .textContent(response.message)
                                        .ok('Aceptar')
                                )
                                //$records.records.get({},function () {})
                            }
                        })
                    }
                }
            }
            $scope.cancel = $mdDialog.hide;
        })
})();