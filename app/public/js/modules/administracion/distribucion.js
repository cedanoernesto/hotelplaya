(function () {
    var app = angular.module('App')
    app.requires.push('md.data.table');
    app
        .service('$record',function () {
            return {
                data: {},
                restaurant: {},
                set : function (record) {
                    this.data = record;
                },
                setRestaurant: function (restaurant) {
                    this.restaurant = restaurant
                }
            };
        })
        .service('$records',function ($resource, $data, $record) {
            'use strict';
            return {
                records: $resource($data.base_url+ 'administracion/distribucion/datatable')
            }
        })
        .service('$saveRecords',function ($resource, $data) {
            'use strict';
            return {
                records: $resource($data.base_url+ 'administracion/distribucion/save')
            }
        })
        .service('$restaurants', function ($resource, $data) {
            'use strict';
            return {
                records: $resource($data.base_url + 'administracion/restaurantes/datatable')
            }
        })
        .service('$events', function ($resource, $data) {
            'use strict';
            return {
                records: $resource($data.base_url + 'administracion/eventos/datatable')
            }
        })
        .service('$distributionResources', function ($resource, $data) {
            'use strict';
            return {
                records: $resource($data.base_url + 'administracion/distribucion/items')
            }
        })
        .service('$mobiliario', function ($resource, $data) {
            'use strict';
            return {
                records: $resource($data.base_url + 'administracion/mobiliario/items')
            }
        })
        .service('$map', function () {
            var map = new Map({
                container: 'container',
                scale: 1 / 10,
                imageResources: [],
                edit: true,
                originalFilter: { red: 0, green: 0, blue: 0, alpha: 0 },
                hoverFilter: { red: 0, green: 0, blue: 255, alpha: 0.5 },
                selectedFilter: { red: 0, green: 255, blue: 0, alpha: 0.5 }
            })

            var wrapper = function() {
                return map;
            }

            wrapper.isMapShown = false;
            wrapper.show = function() {
                this.isMapShown = true;
            }

            wrapper.hide = function() {
                this.isMapShown = false;
            }

            wrapper.setResources = function(resources) {
                map.imageResources = resources;
                map.defaultInfo.imageId =  resources[0].id;
                map.defaultInfo.imageUrl = resources[0].img;
            }

            wrapper.load = function(objmapinfo, index) {
                index = index || 0
                objmapinfo[index] = objmapinfo[index] || []
                this.floors = objmapinfo
                map.renderMapFromInfo(objmapinfo[index])
                map.updateObjectMap('main')
            }

            wrapper.clear = function() {
                map.clearMap()
            }

            wrapper.floors = []


            return wrapper;
        })
        .controller('distribucion',function($scope,$records,$mdDialog,$record,$saveRecords,$data, $map, $restaurants) {
            $scope.map = $map

            $restaurants.records.get({}, function(response) {
                $scope.restaurants = response.data;
            })

            $scope.$watch('restaurant_select', function (newValue, oldValue) {
                if(newValue !== oldValue) {
                    $record.setRestaurant({ id: newValue })
                    $scope.getRecords()
                }
            });

            $scope.record = [];
            $scope.records = []
            $scope.selected = [];
            $scope.limitOptions = [5,10,15]
            $scope.filter = {
                options: {
                    debounce: 500
                }
            };
            $scope.options = {
                rowSelection: false,
                multiSelect: false,
                autoSelect: true,
                decapitate: false,
                largeEditDialog: false,
                boundaryLinks: false,
                limitSelect: true,
                pageSelect: true
            };
            $scope.query = {
                filters: [],
                order: '',
                limit: 5,
                page: 1
            };
            function success (response) {
                $scope.records = response
            }
            $scope.getRecords = function () {
                $scope.query.restaurant = $record.restaurant.id
                $scope.promise = $records.records.get($scope.query,success).$promise
            }
            $scope.removeFilter = function () {
                $scope.filter.show = false;
                $scope.query.filter = '';
                if($scope.filter.form.$dirty) {
                    $scope.filter.form.$setPristine();
                }
            };
            $scope.$watch('query.filter', function (newValue, oldValue) {
                if(!oldValue) {
                    bookmark = $scope.query.page;
                }
                if(newValue !== oldValue) {
                    $scope.query.page = 1;
                }
                if(!newValue) {
                    $scope.query.page = bookmark;
                }
                $scope.getRecords();
            });

            $scope.isMapShown = $map.isMapShown;

            $scope.showMap = function() {
                if ($scope.restaurant_select) {
                    $scope.map.clear();
                    $record.set({});
                    $scope.map.show();
                }
            }

            $scope.showDialog = function (event,record) {
                $record.set(record);
                $mdDialog.show({
                    clickOutsideToClose:true,
                    controller:'editDistribucion',
                    controllerAs:'crtl',
                    focusOnOpen:true,
                    targetEvent:event,
                    templateUrl:$data.base_url+ '/public/templates/dialogs/administracion/distribucion.html'
                }).then($scope.getRecords)
            }

            $scope.remove = function($event,id){
                $event.stopPropagation()
                var confirm = $mdDialog.confirm()
                    .title('Eliminar')
                    .textContent('¿Desea eliminar el usuario?')
                    .ariaLabel('')
                    .targetEvent($event)
                    .ok('Si')
                    .cancel('Cancelar');
                $mdDialog.show(confirm).then(function () {
                    $saveRecords.records.save({id:id, estatus: 2},function () {
                        $scope.getRecords()
                    })
                },function () {
                    //console.log('no')
                })
            }
            $scope.propagation = function ($event) {
                $event.stopPropagation()
            }
        })
        .controller('editDistribucion', function($scope, $mdDialog, $map, $record, $saveRecords, $distributionResources) {
            $scope.map = $map
            $scope.record = $record.data
            $scope.response = {
                status: 1,
                message: ''
            }

            $scope.addRecord = function () {
                $scope.distributionForm.$setSubmitted();
                if ($scope.distributionForm.$valid) {
                    $saveRecords.records.save($scope.record,function (response) {
                        $scope.response = response
                        if (response.status){
                            $mdDialog.hide();
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .title('Mensaje')
                                    .textContent(response.message)
                                    .ok('Aceptar')
                            )
                        }
                    })
                }
            }

            $scope.cancel = $mdDialog.hide;

            $scope.edit = function() {
                $distributionResources.records.get({ id_distribucion: $scope.record.id }, function (response) {
                    $map.clear();
                    $map.load(response.data);
                    $scope.cancel();
                    $scope.map.show();
                });
            }

        })
        .controller('map', function($scope, $map, $mdDialog, $record, $saveRecords, $events, $mobiliario, $data) {
            $scope.currentIndex = 0
            $scope.map = $map
            $scope.events = []
            var removedItems = []
            var removedFloors = []

            $scope.showFloor = function(index) {
                $scope.map.floors[$scope.currentIndex] = $map().getMapInfo()
                $map.clear();
                $map.load($scope.map.floors, index);
                $scope.currentIndex = index
            }

            $scope.addFloor = function() {
                $scope.map.floors.push([])
                $scope.showFloor($scope.map.floors.length - 1)
            }

            $scope.removeFloor = function() {
                if ($scope.map.floors.length > 1) {
                    $scope.map.floors[$scope.currentIndex] = $map().getMapInfo()
                    var removed = $scope.map.floors[$scope.currentIndex]

                    if (removed.length > 0) {
                        removedFloors.push(removed[0].piso)
                    }

                    $scope.map.floors.splice($scope.currentIndex, 1)
                    $map.clear();
                    $map.load($scope.map.floors, 0);
                    $scope.currentIndex = 0
                }
            }

            $events.records.get({}, function(response) {
                $scope.events = response.data
            })

            var container = document.querySelector('#container')
            $scope.containerWidth = container.offsetWidth
            $scope.degrees = 0
            $scope.scale = 1

            function getImageOptionObj(id) {
                return $scope.options.find(function (option) {
                    return option.id === id
                })
            }

            $scope.showSettings = false
            selectedObject = undefined

            $mobiliario.records.get({}, function(response) {
                $scope.options = response.data.map(function (mobiliario) {
                    mobiliario.img = $data.base_url + 'public/assets/images/mobiliario/' + mobiliario.img
                    mobiliario.id = parseInt(mobiliario.id)
                    return mobiliario
                })
                $map.setResources($scope.options)
                $scope.selectedOption = $scope.options[0].id
            })

            $map().on('itemSelected', function(obj) {
                selectedObject = obj
                if (selectedObject) {
                    $scope.scale = obj.baseinfo.size_ratio * 10
                    $scope.degrees = obj.baseinfo.rotation
                    $scope.selectedOption = obj.baseinfo.imageId
                    $scope.selectable = obj.baseinfo.selectable
                    $scope.quantity = obj.baseinfo.quantity
                    $scope.tag = obj.baseinfo.tag
                    $scope.table = obj.baseinfo.tables
                    $scope.showSettings = true
                    $scope.$digest()
                } else {
                    $scope.showSettings = false
                }

                $scope.$digest()
            })

            $scope.removeItem = function() {
                if (selectedObject) {
                    if (selectedObject.baseinfo.id !== 0) {
                        removedItems.push(selectedObject.baseinfo.id);
                    }
                    $map().removeItem(selectedObject)
                    $scope.showSettings = false
                    selectedObject = null
                }
            }

            $scope.updateObjScale = function(){
                if (selectedObject) {
                    selectedObject.setScale($scope.scale / 10)
                    selectedObject.update()
                }

                $scope.updateDefaultInfo()
            }

            $scope.updateImage = function(){
                if (selectedObject) {
                    var option = getImageOptionObj($scope.selectedOption)
                    selectedObject.setImage(option.id, option.img)

                    selectedObject.update()
                }

                $scope.updateDefaultInfo()
            }

            $scope.rotate = function(degrees){
                if (selectedObject) {
                    $scope.degrees += degrees
                    selectedObject.changeRotation(degrees)
                }

                $scope.updateDefaultInfo()
            }

            $scope.updateTag = function() {
                if (selectedObject) {
                    selectedObject.setTag($scope.tag)
                    selectedObject.update()
                }
            }

            $scope.updateTable = function() {
                if (selectedObject) {
                    selectedObject.setTable($scope.table)
                }
            }

            $scope.updateSelectable = function() {
                if (selectedObject) {
                    selectedObject.setSelectable($scope.selectable)
                }
            }

            $scope.updateQuantity = function() {
                if (selectedObject) {
                    selectedObject.setQuantity($scope.quantity)
                }
            }

            $scope.updateDefaultInfo = function() {
                $map().defaultInfo.imageId = $scope.selectedOption
                $map().defaultInfo.imageUrl = $scope.options.find(function(option) {
                    return $scope.selectedOption === option.id
                }).img
                $map().defaultInfo.size_ratio = $scope.scale / 10
                $map().defaultInfo.rotation = $scope.degrees
            }

            $scope.$watch(function(){
                return container.offsetWidth
            }, function(){
                $scope.record = $record.data
                $map().updateObjectMap('main')
            })

            window.onresize = function() {
                $map().updateObjectMap('main')
            }

            $scope.save = function(ev) {
                $scope.map.floors[$scope.currentIndex] = $map().getMapInfo()

                $scope.distributionForm.$setSubmitted()
                if ($scope.distributionForm.$valid) {
                    $mdDialog.show(
                        $mdDialog.confirm()
                            .title('Guardar distribución')
                            .textContent('¿Esta seguro que desea guardar esta distribución de mesas?')
                            .targetEvent(ev)
                            .ok('Sí')
                            .cancel('No')
                    ).then(function() {
                        $saveRecords.records.save({
                            id: $record.data.id || 0,
                            id_restaurante: $record.restaurant.id,
                            id_evento: $scope.record.id_evento,
                            nombre: $scope.record.nombre,
                            map: $scope.map.floors,
                            removed: removedItems,
                            removedFloors: removedFloors
                        }, function(response) {

                            if (response.id_md5) {
                                $mdDialog.show(
                                    $mdDialog.alert()
                                        .title('Mensaje')
                                        .textContent(response.message)
                                        .ok('Aceptar')
                                ).then(function() {
                                    removedItems = [];
                                    removedFloors = [];
                                    $scope.currentIndex = 0;
                                    $map.hide();
                                });
                            }

                        })

                    }, function() {

                    });
                } else {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .title('Datos insuficientes')
                            .textContent('Debe ingresar un nombre para la distribución de mesas')
                            .ok('Aceptar')
                    )
                }
            }

            $scope.cancel = function() {
                $mdDialog.show(
                    $mdDialog.confirm()
                        .title('Salir')
                        .textContent('¿Esta seguro que desea salir sin guardar?')
                        .ok('Sí')
                        .cancel('No')
                ).then(function() {
                    $map.hide()
                });
            }
        })
})();
