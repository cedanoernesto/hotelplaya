(function () {
    angular.module('App')
        .service('$reservacion', function($resource, $data) {
            return {
                get: $resource($data.base_url + 'administracion/asistencia_reservacion/reservacion').get,
                save: $resource($data.base_url + 'administracion/asistencia_reservacion/save').save,
            }
        })
        .service('$mobiliario', function ($resource, $data) {
            'use strict';
            return {
                records: $resource($data.base_url + 'administracion/mobiliario/items')
            }
        })
        .service('$horarios', function($resource, $data) {
            return {
                records: $resource($data.base_url + 'index.php/administracion/restaurantes/horario')
            }
        })
        .service('$restaurantes', function($resource, $data) {
            return {
                records: $resource($data.base_url + 'index.php/administracion/restaurantes/all')
            }
        })
        .service('$event', function($resource, $data) {
            return {
                records: $resource($data.base_url + 'index.php/kiosko/reservacion/restaurantInfo')
            }
        })
        .service('$distributionResources', function ($resource, $data) {
            'use strict';
            return {
                records: $resource($data.base_url + 'kiosko/reservacion/items')
            }
        })
        .service('$map', function() {
            var map = new Map({
                container: 'map-container',
                imageResources: [],
                edit: false,
                select: false,
                originalFilter: { red: 0, green: 0, blue: 0, alpha: 0 },
                hoverFilter: { red: 0, green: 0, blue: 255, alpha: 0.5 },
                selectedFilter: { red: 0, green: 255, blue: 0, alpha: 0.5 },
                reservedFilter: { red: 255, green: 0, blue: 0, alpha: 0.5 },
                availableFilter: { red: 255, green: 255, blue: 150, alpha: 0.5 }
            })

            var wrapper = function() {
                return map;
            }

            wrapper.isMapShown = false;
            wrapper.show = function() {
                this.isMapShown = true;
            }

            wrapper.hide = function() {
                this.isMapShown = false;
            }

            wrapper.setResources = function(resources) {
                map.imageResources = resources;
                map.defaultInfo.imageId =  resources[0].id;
                map.defaultInfo.imageUrl = resources[0].img;
            }

            wrapper.load = function(objmapinfo) {
                map.renderMapFromInfo(objmapinfo)
                setTimeout(function() {
                    map.updateObjectMap('main')
                }, 500)
            }

            wrapper.clear = function() {
                map.clearMap()
            }


            return wrapper;
        })
        .controller('estatus', function ($scope, $reservacion, $mdDialog, $map, $dateUtils, $restaurantes, $horarios, $event, $distributionResources) {
            $scope.data = {}
            $scope.reservation = {}
            $scope.reservation.mindate = new Date()
            $scope.data.reservationdate = new Date()

            $scope.state = {}
            $scope.state.selectedDate = function() {
                return $scope.options.find(function(option) {
                    return option.id === $scope.data.reservationtime
                })
            }

            $restaurantes.records.get({}, function(response) {
                $scope.restaurants = response.data
            })

            $scope.$watch('restaurant_select', function(newValue, oldValue) {
                if ($scope.restaurants) {
                    var restaurant = $scope.restaurants.find(function (restaurant) {
                        return restaurant.id == newValue;
                    });

                    $scope.data.reservationtime = null

                    $horarios.records.get({
                        id_restaurante: restaurant.id,
                        fecha: $dateUtils.convertDateString($scope.data.reservationdate),
                        dia: $scope.data.reservationdate.getDay()
                    }, function(response) {
                        $scope.options = $dateUtils.getTimeRangeByArray(
                            $scope.data.reservationdate,
                            response.data
                        )
                    })
                }
            });

            $scope.$watch('data.reservationdate', function(newValue, oldValue) {
                if ($scope.restaurants) {
                    var restaurant = $scope.restaurants.find(function (restaurant) {
                        return restaurant.id == $scope.restaurant_select;
                    });

                    $horarios.records.get({
                        id_restaurante: restaurant.id,
                        fecha: $dateUtils.convertDateString(newValue),
                        dia: newValue.getDay()
                    }, function(response) {
                        $scope.data.reservationtime = null
                        $scope.options = $dateUtils.getTimeRangeByArray(
                            $scope.data.reservationdate,
                            response.data
                        )
                    })
                }
            })

            $scope.getInfo = function() {
                var restaurant = $scope.restaurants.find(function (restaurant) {
                    return restaurant.id == $scope.restaurant_select;
                });

                $event.records.save({
                    id_restaurante: restaurant.id,
                    fecha: $dateUtils.convertDateString($scope.data.reservationdate),
                    hora: $scope.state.selectedDate().fullvalue,
                    dia: $scope.data.reservationdate.getDay()
                }, function(response) {
                    if (response.status === 1) {
                        $scope.event = response.data
                        $distributionResources.records.get({
                            id_distribucion: response.data.id_distribucion,
                            fecha: $dateUtils.convertDateString($scope.data.reservationdate),
                            hora: $scope.state.selectedDate().fullvalue,
                            dia: $scope.data.reservationdate.getDay(),
                            asiento_seleccionable: response.data.asiento_seleccionable
                        }, function(distribucion) {
                            $scope.data.distribucion = {
                                id_evento: response.data.id,
                                evento_asiento_seleccionable: response.data.asiento_seleccionable,
                                nombre_evento: response.data.nombre,
                                map: distribucion.data,
                                available: distribucion.available,
                                id_distribucion: response.data.id_distribucion
                            }

                            $scope.event.available = distribucion.available

                            if (distribucion.available <= 0) {
                                $mdDialog.show(
                                    $mdDialog.alert()
                                        .clickOutsideToClose(true)
                                        .title($scope.translation.LABELS.ATENTION)
                                        .textContent($scope.translation.MESSAGES.SOLD_OUT)
                                        .ariaLabel('Alert Dialog Demo')
                                        .ok($scope.translation.LABELS.UNDERSTOOD)
                                )
                            }

                            $map.clear();
                            $map.load($scope.data.distribucion.map);
                        })

                    } else {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title($scope.translation.LABELS.ATENTION)
                                .textContent($scope.translation.MESSAGES.NO_EVENT)
                                .ariaLabel('Alert Dialog Demo')
                                .ok($scope.translation.LABELS.UNDERSTOOD)
                        )
                    }
                }, function(error) {

                })
            }
        })
        .controller('map', function($scope, $map, $data, $mobiliario) {
            $scope.map = $map;

            var container = document.querySelector('#map-container')
            $scope.containerWidth = container.offsetWidth

            $mobiliario.records.get({}, function(response) {
                $map.setResources(response.data.map(function (mobiliario) {
                    mobiliario.img = $data.base_url + 'public/assets/images/mobiliario/' + mobiliario.img
                    mobiliario.id = parseInt(mobiliario.id)
                    return mobiliario
                }))
            })

            $scope.$watch(function(){
                return container.offsetWidth
            }, function(){
                $map().updateObjectMap('main')
            })

            window.onresize = function() {
                $map().updateObjectMap('main')
            }
        })
})()
