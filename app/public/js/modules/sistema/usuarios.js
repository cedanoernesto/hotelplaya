(function () {
    var app = angular.module('App')
    app.requires.push('md.data.table');
    app

        .service('$record',function () {
            return {
                data: {},
                set : function (record) {
                    this.data = record;
                }
            };
        })
        .service('$usuariosRecords',function ($resource, $data) {
            'use strict';
            return {
                records: $resource($data.base_url+ 'sistema/usuarios/datatable')
            }
        })
        .service('$perfilesRecords',function ($resource, $data) {
            'use strict';
            return {
                records: $resource($data.base_url+ 'sistema/usuarios/perfiles')
            }
        })
        .service('$saveRecords',function ($resource, $data) {
            'use strict';
            return {
                records: $resource($data.base_url+ 'sistema/usuarios/save')
            }
        })
        .controller('usuarios',function($scope,$usuariosRecords,$mdDialog,$record,$saveRecords,$data) {
            $scope.record = [];
            $scope.records = []
            $scope.selected = [];
            $scope.limitOptions = [5,10,15]
            $scope.filter = {
                options: {
                    debounce: 500
                }
            };
            $scope.options = {
                rowSelection: false,
                multiSelect: false,
                autoSelect: true,
                decapitate: false,
                largeEditDialog: false,
                boundaryLinks: false,
                limitSelect: true,
                pageSelect: true
            };
            $scope.query = {
                filters: [],
                order: '',
                limit: 5,
                page: 1
            };
            function success (response) {
                $scope.records = response
            }
            $scope.getRecords = function () {
                $scope.promise = $usuariosRecords.records.get($scope.query,success).$promise
            }
            $scope.removeFilter = function () {
                $scope.filter.show = false;
                $scope.query.filter = '';
                if($scope.filter.form.$dirty) {
                    $scope.filter.form.$setPristine();
                }
            };
            $scope.$watch('query.filter', function (newValue, oldValue) {
                if(!oldValue) {
                    bookmark = $scope.query.page;
                }

                if(newValue !== oldValue) {
                    $scope.query.page = 1;
                }

                if(!newValue) {
                    $scope.query.page = bookmark;
                }
                $scope.getRecords();
            });
            $scope.showDialog = function (event,record) {
                $record.set(record);
                $mdDialog.show({
                    clickOutsideToClose:true,
                    controller:'addUsuarioController',
                    controllerAs:'crtl',
                    focusOnOpen:true,
                    targetEvent:event,
                    templateUrl:$data.base_url+ '/public/templates/dialogs/sistema/usuarios.html'
                }).then($scope.getRecords)
            }
            $scope.remove = function($event,id){
                $event.stopPropagation()
                var confirm = $mdDialog.confirm()
                    .title('Eliminar')
                    .textContent('¿Desea eliminar el usuario?')
                    .ariaLabel('')
                    .targetEvent($event)
                    .ok('Si')
                    .cancel('Cancelar');
                $mdDialog.show(confirm).then(function () {
                    $saveRecords.records.save({id:id, estatus: 2},function () {
                        $scope.getRecords()
                    })
                },function () {
                    //console.log('no')
                })
            }
            $scope.propagation = function ($event) {
                $event.stopPropagation()
            }
        })
        .controller('addUsuarioController',function ($scope,$perfilesRecords,$record, $mdDialog, $saveRecords, $data) {
            $scope.record = $record.data
            $scope.response = {
                status: 1,
                message: ''
            }
            function success(response) {
                $scope.records = response.data
            }
            $scope.getPerfiles = function () {
                return  $perfilesRecords.records.get({},success).$promise
            }
            $scope.saveRecord = function () {
                $scope.formUsuarios.$setSubmitted();
                if ($scope.formUsuarios.$valid){
                    delete $scope.record['perfil']
                    $saveRecords.records.save($scope.record,function (response) {
                        $scope.response = response
                        if (response.status){
                            $mdDialog.hide();
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .title('Mensaje')
                                    .textContent(response.message)
                                    .ok('Aceptar')
                            )
                        }
                    })
                }
            }
            $scope.changePassword = function () {
                $mdDialog.show({
                    clickOutsideToClose:true,
                    controller:'changePassword',
                    controllerAs:'crtl',
                    focusOnOpen:true,
                    targetEvent:event,
                    templateUrl:$data.base_url+ '/public/templates/dialogs/sistema/usuarios_contrasena.html'
                })
            }
            $scope.getPerfiles()
            $scope.cancel = $mdDialog.hide;
        })
        .controller('changePassword',function ($scope, $record, $mdDialog,$saveRecords) {
            $scope.cancel = $mdDialog.hide;
            $scope.saveRecord = function () {
                $scope.formUsuarios.$setSubmitted();
                if ($scope.formUsuarios.$valid){
                    delete $scope.record['perfil']
                    console.log($record);
                    $saveRecords.records.save({
                        id: $record.data.id,
                        contrasena:$scope.record.contrasena
                    },function (response) {
                        $scope.response = response
                        if (response.status){
                            $mdDialog.hide();
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .title('Mensaje')
                                    .textContent(response.message)
                                    .ok('Aceptar')
                            )
                        }
                    })
                }
            }
        })
})();